<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReplacementleaveUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
                $table->decimal('rl_balance', 8, 2)->after('cf_monthlimit')->default(0);
                $table->decimal('rl_used', 8, 2)->after('cf_monthlimit')->default(0);
                $table->decimal('rl', 8,2)->after('cf_monthlimit')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
                $table->dropColumn('rl_balance');
                $table->dropColumn('rl_used');
                $table->dropColumn('rl');
        });
    }
}
