<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter', function (Blueprint $table) {
            $table->bigIncrements('let_id');
            $table->string('let_number');
            $table->string('let_name')->nullable();
            $table->string('let_desc')->nullable();
            $table->integer('let_attachid')->nullable();
            $table->string('let_status')->nullable();
            $table->integer('let_preparedby')->nullable();
            $table->timestamp('let_prepareddate')->nullable();
            $table->integer('let_verifiedby')->nullable();
            $table->timestamp('let_verifieddate')->nullable();
            $table->string('let_permissiondel')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter');
    }
}
