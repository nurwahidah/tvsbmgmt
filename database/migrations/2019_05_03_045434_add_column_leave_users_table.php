<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLeaveUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){

                $table->decimal('mc_balance', 8, 2)->after('status_approve')->nullable();
                $table->decimal('mc_used', 8, 2)->after('status_approve')->nullable();
                $table->decimal('mc', 8, 2)->after('status_approve')->nullable();
                $table->decimal('leave_balance', 8, 2)->after('status_approve')->nullable();
                $table->decimal('leave_used', 8, 2)->after('status_approve')->nullable();
                $table->decimal('leave', 8, 2)->after('status_approve')->nullable();
                $table->integer('current_year')->after('status_approve')->nullable();
                $table->integer('contract_typeid')->after('status_approve')->nullable();
                $table->string('designation')->after('status_approve')->nullable();
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
                $table->dropColumn('designation');
                $table->dropColumn('contract_typeid');
                $table->dropColumn('current_year');
                $table->dropColumn('leave');
                $table->dropColumn('leave_used');
                $table->dropColumn('leave_balance');
                $table->dropColumn('mc');
                $table->dropColumn('mc_used');
                $table->dropColumn('mc_balance'); 
        });
    }
}
