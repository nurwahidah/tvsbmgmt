<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTnrptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tnrpt', function (Blueprint $table) {
            $table->bigIncrements('tn_id');
            $table->string('tn_name')->nullable();
            $table->string('tn_description')->nullable();
            $table->string('tn_referenceno')->nullable();
            $table->timestamp('tn_date')->nullable();
            $table->string('tn_title')->nullable();
            $table->string('tn_body',1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tnrpt');
    }
}
