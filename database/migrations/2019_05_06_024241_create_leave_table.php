<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave', function (Blueprint $table) {
            $table->bigIncrements('lve_id');
            $table->string('lve_category');
            $table->integer('lve_totalleave');
            $table->string('lve_description')->nullable();
            $table->string('lve_status')->default('N')->nullable();
            $table->integer('lve_preparedby')->nullable();
            $table->timestamp('lve_prepareddate')->nullable();
            $table->integer('lve_verifiedby')->nullable();
            $table->timestamp('lve_verifieddate')->nullable();
            $table->integer('lve_approvedby')->nullable();
            $table->timestamp('lve_approveddate')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave');
    }
}
