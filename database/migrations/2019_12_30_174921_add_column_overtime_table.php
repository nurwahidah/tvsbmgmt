<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOvertimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('overtime',function(Blueprint $table){
                $table->decimal('ot_totalhour',8,2)->after('ot_hour')->nullable();
                $table->decimal('ot_countday',8,2)->after('ot_readstatus')->nullable();
                $table->decimal('ot_balancehour',8,2)->after('ot_readstatus')->nullable();
                $table->string('ot_balanceusedstatus')->after('ot_readstatus')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('overtime', function(Blueprint $table){
            $table->dropColumn('ot_totalhour');
            $table->dropColumn('ot_countday');
            $table->dropColumn('ot_balancehour');
            $table->dropColumn('ot_balanceusedstatus');

        });
    }
}
