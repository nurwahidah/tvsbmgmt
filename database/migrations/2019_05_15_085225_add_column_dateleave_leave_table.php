<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateleaveLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave',function(Blueprint $table){
                $table->timestamp('lve_enddate')->after('lve_status')->nullable();
                $table->timestamp('lve_startdate')->after('lve_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave',function(Blueprint $table){
                $table->dropColumn('lve_enddate');
                $table->dropColumn('lve_startdate');
        });
    }
}
