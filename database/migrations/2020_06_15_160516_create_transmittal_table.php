<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmittalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transmittal', function (Blueprint $table) {
            $table->bigIncrements('tn_id');
            $table->string('tn_number');
            $table->string('tn_name')->nullable();
            $table->string('tn_attachid')->nullable();
            $table->string('tn_status')->nullable();
            $table->integer('tn_preparedby')->nullable();
            $table->timestamp('tn_prepareddate')->nullable();
            $table->integer('tn_verifiedby')->nullable();
            $table->timestamp('tn_verifieddate')->nullable();
            $table->string('tn_permissiondel')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transmittal');
    }
}
