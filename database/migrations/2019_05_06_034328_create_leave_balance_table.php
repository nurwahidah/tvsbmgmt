<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_balance', function (Blueprint $table) {
            $table->bigIncrements('lvbal_id');
            $table->string('lvbal_userid')->nullable();
            $table->integer('lvbal_year')->nullable();
            $table->decimal('lvbal_totalbalance', 8, 2)->nullable();
            $table->string('lvbal_type')->nullable();
            $table->string('lvbal_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_balance');
    }
}
