<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLeavecfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){

                $table->integer('cf_monthlimit')->after('mc_balance')->nullable();
                $table->decimal('cf_leaveused', 8, 2)->after('mc_balance')->nullable();
                $table->decimal('cf_leave', 8, 2)->after('mc_balance')->nullable();
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
                $table->dropColumn('cf_monthlimit');
                $table->dropColumn('cf_leaveused');
                $table->dropColumn('cf_leave');
        });
    }
}
