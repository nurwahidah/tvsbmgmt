<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_leave', function (Blueprint $table) {

            $table->bigIncrements('mlve_id');
            $table->string('mlve_description')->nullable();
            $table->string('mlve_cf')->nullable();
            $table->string('mlve_month')->nullable();
            $table->string('mlve_curryear')->nullable();
            $table->string('mlve_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_leave');
    }
}
