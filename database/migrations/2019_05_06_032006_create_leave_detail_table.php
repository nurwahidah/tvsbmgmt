<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_detail', function (Blueprint $table) {
            $table->bigIncrements('lvdl_id');
            $table->integer('lvdl_leaveid');
            $table->string('lvdl_description')->nullable();
            $table->timestamp('lvdl_dateapply')->nullable();
            $table->string('lvdl_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_detail');
    }
}
