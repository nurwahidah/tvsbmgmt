<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentotherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentother', function (Blueprint $table) {
            $table->bigIncrements('doc_id');
            $table->string('doc_number');
            $table->string('doc_name')->nullable();
            $table->string('doc_description')->nullable();
            $table->string('doc_package')->nullable();
            $table->string('doc_attachid')->nullable();
            $table->string('doc_status')->nullable();
            $table->integer('doc_preparedby')->nullable();
            $table->timestamp('doc_prepareddate')->nullable();
            $table->integer('doc_verifiedby')->nullable();
            $table->timestamp('doc_verifieddate')->nullable();
            $table->string('doc_permissiondel')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentother');
    }
}
