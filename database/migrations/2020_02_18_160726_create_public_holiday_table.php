<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_holiday', function (Blueprint $table) {
            $table->bigIncrements('ph_id');
            $table->string('ph_year')->nullable();
            $table->string('ph_city')->nullable();
            $table->string('ph_code')->nullable();
            $table->timestamp('ph_date')->nullable();
            $table->string('ph_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_holiday');
    }
}
