<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtime', function (Blueprint $table) {
            $table->bigIncrements('ot_id');
            $table->timestamp('ot_date');
            $table->string('ot_description')->nullable();
            $table->string('ot_type')->nullable();
            $table->timestamp('ot_starttime')->nullable();
            $table->timestamp('ot_endtime')->nullable();
            $table->decimal('ot_hour', 8, 2)->default(0);
            $table->string('ot_status')->default('N')->nullable();
            $table->string('ot_readstatus')->default('0');
            $table->integer('ot_preparedby')->nullable();
            $table->timestamp('ot_prepareddate')->nullable();
            $table->integer('ot_approvedby')->nullable();
            $table->timestamp('ot_approveddate')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtime');
    }
}
