<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAttachmentLeaveTablle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave',function(Blueprint $table){
                $table->string('lve_readstatus')->after('lve_type')->default(0);
                $table->string('lve_notice')->after('lve_type')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave',function(Blueprint $table){
                $table->dropColumn('lve_readstatus');
                $table->dropColumn('lve_notice');
        });
    }
}
