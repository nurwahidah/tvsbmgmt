<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'user'], function(){

	Route::get('registeruser','UserController@createUser')->name('user.registeruser');
    Route::get('{id}/enable', 'UserController@enableUser')->name('user.enable');
    Route::get('{id}/disable', 'UserController@disableUser')->name('user.disable');
    Route::get('{id}/approved', 'UserController@approveUser')->name('user.approved');
    Route::get('{id}/disapproved', 'UserController@disapprovedUser')->name('user.disapproved');
    Route::get('useractivate', 'UserController@getSetActivation');
    Route::get('showuser/{id}','UserController@showuser')->name('user.showuser');
    Route::get('createuser','UserController@createUser')->name('user.createuser');
    Route::get('edituser/{id}/{roleid}','UserController@editUser')->name('user.edituser');
    Route::get('updateCF','UserController@updateCF')->name('user.updatecf');
    Route::get('resetLeaveOpen','UserController@updateRLO')->name('user.updaterlo');
    Route::get('resetLeaveClose','UserController@updateRLC')->name('user.updaterlc');
    
    Route::post('process-action', 'UserController@processaction');
    Route::post('storeuser', 'UserController@storeUser')->name('user.storeuser');
    Route::post('updateuser', 'UserController@updateUser')->name('user.updateuser');
    Route::post('change-password','UserController@changePassword')->name('user.changepassword');
    Route::post('update-profile','UserController@updateProfile')->name('user.updateprofile');

    Route::get('showprofile','UserController@showProfile')->name('user.showprofile');
    Route::get('showprofile/getfile/{id}/{filename}','UserController@getFile')->name('user.showprofile.getfile');

    Route::resource('/','UserController');
});

Route::group(['prefix'=>'admin'], function(){

    //Permission
    Route::get('permission/getRolePermission/{id}','PermissionController@getRolePermission')->name('admin.permission.getRolePermission');
    Route::get('permission/getPerm/{id}','PermissionController@getPerm')->name('admin.permission.getPerm');
    Route::post('permission/addpermission','PermissionController@addPerm')->name('admin.permission.addpermission');
    Route::post('permission/updatepermission','PermissionController@updatePerm')->name('admin.permission.updatepermission');
    Route::resource('permission','PermissionController');

    //Roles
    Route::get('role/getRole/{id}','RoleController@getRole')->name('admin.permission.getRole');
    Route::post('role/addrole','RoleController@addRole')->name('admin.role.addrole');
    Route::post('role/updaterole','RoleController@updateRole')->name('admin.role.updaterole');
    Route::post('role/storepermission','RoleController@storepermission')->name('admin.roles.storepermission');
    Route::resource('role','RoleController');
});

Route::group(['prefix'=>'leave'], function(){

    Route::get('{id}/approved', 'LeaveController@approveLeave')->name('leave.approved');
    Route::get('{id}/disapproved', 'LeaveController@disapprovedLeave')->name('leave.disapproved');
    Route::get('remove/{id}', 'LeaveController@removeLeave')->name('leave.remove');
    Route::get('editleave/{id}','LeaveController@editLeave')->name('leave.editleave');
    Route::get('showleave/{id}','LeaveController@showLeave')->name('leave.showleave');
    Route::get('showleave/getfile/{id}/{filename}','LeaveController@getFile')->name('leave.showleave.getfile');
    Route::get('createleave','LeaveController@createLeave')->name('leave.createleave');
    
    Route::get('master','LeaveController@showMasterLeave')->name('leave.master.show');
    Route::get('master/getCF/{id}','LeaveController@getCF')->name('leave.master.getCF');
    Route::get('htmltopdfview/{id}','LeaveController@generatePDF')->name('leave.htmltopdfview');
    Route::get('printleave/{id}','LeaveController@getleavereport')->name('leave.printleave');

    Route::post('apply/storeleave','LeaveController@storeLeave')->name('leave.apply.storeleave');
    Route::post('updateleave', 'LeaveController@updateLeave')->name('leave.updateleave');
    Route::post('master/updateML','LeaveController@updateML')->name('leave.master.updateML');

    Route::resource('/','LeaveController');
});

Route::group(['prefix'=>'master'], function(){
    
    Route::get('public_holiday/edit/{id}','MasterController@editPublicHoliday')->name('master.pb.editpb');
    Route::get('public_holiday','MasterController@showPublicHoliday')->name('master.pb.show');
    Route::get('public_holiday/{id}', 'MasterController@removePublicHoliday')->name('master.pb.remove');
    
    Route::post('public_holiday/updatepublicholiday', 'MasterController@updatePublicHoliday')->name('master.pb.updatepublicholiday');
    Route::post('public_holiday/myproductsDeleteAll', 'MasterController@deleteAllPublicHoliday')->name('master.pb.deleteallPH');
    Route::resource('/config','MasterController');
});

Route::group(['prefix'=>'overtime'], function(){

    Route::get('{id}/approved', 'OvertimeController@approveOT')->name('overtime.approved');
    Route::get('{id}/disapproved', 'OvertimeController@disapprovedOT')->name('overtime.disapproved');
    Route::get('removeOT/{id}', 'OvertimeController@removeOT')->name('overtime.remove');
    Route::get('editOT/{id}','OvertimeController@editOT')->name('overtime.editOT');

    Route::get('showOT/{id}','OvertimeController@showOT')->name('overtime.showOT');
    Route::get('createOT','OvertimeController@createOT')->name('overtime.createOT');

    Route::post('create/storeot','OvertimeController@storeOT')->name('overtime.create.storeOT');
    Route::post('updateot', 'OvertimeController@updateOT')->name('overtime.updateOT');

    Route::resource('/','OvertimeController');
});

Route::group(['prefix'=>'report'], function(){
    
    Route::get('printreport/{id}','TnController@getPrintReport')->name('report.tn.printreport');
    Route::get('exportreport','TnController@getExportReport')->name('report.tn.exportreport');
    Route::get('convertpdftn/{id}','TnController@generatePDF')->name('report.tn.convertpdftn');
    Route::get('create','TnController@createTn')->name('report.tn.createTn');
    Route::post('generate/storeTn','TnController@storeTn')->name('report.generate.storeTn');
    Route::resource('tn','TnController');

});

Route::group(['prefix'=>'document'], function(){


    Route::get('removeAttachment/{id}', 'LetterController@removeLetterAttachment')->name('document.letter.removeattachment');
    Route::get('remove/{id}', 'LetterController@removeLetter')->name('document.letter.remove');
    Route::get('requestremove/{id}', 'LetterController@requestRemoveLetter')->name('document.letter.requestremoveletter');
    Route::get('verifiedremove/{id}', 'LetterController@verifiedremoveletter')->name('document.letter.verifiedremoveletter');
    Route::get('cancelremove/{id}', 'LetterController@cancelremoveletter')->name('document.letter.cancelremoveletter');
    Route::get('removeletter/{id}', 'LetterController@cancelremoveletter')->name('document.letter.removeletter');
    Route::get('editletter/{id}','LetterController@editLetter')->name('document.letter.editletter');
    Route::get('showletter/{id}','LetterController@showLetter')->name('document.letter.showletter');
    Route::get('create','LetterController@createL')->name('document.letter.create');
    Route::get('letter/getfile/{id}/{filename}','LetterController@getFileReply')->name('document.letter.getfilereply');
    Route::post('store','LetterController@storeLetter')->name('document.letter.store');
    Route::post('update', 'LetterController@updateLetter')->name('document.letter.update');
    Route::resource('letter','LetterController');
    //Letter 19062020


    Route::get('tn/removeAttachment/{id}', 'TransmittalController@removeTransmittalAttachment')->name('document.transmittal.removeattachment');
    Route::get('tn/remove/{id}', 'TransmittalController@removeTransmittal')->name('document.transmittal.remove');
    Route::get('tn/requestremove/{id}', 'TransmittalController@requestRemoveTransmittal')->name('document.transmittal.requestremovetransmittal');
    Route::get('tn/verifiedremove/{id}', 'TransmittalController@verifiedremoveTransmittal')->name('document.transmittal.verifiedremovetransmittal');
    Route::get('tn/cancelremove/{id}', 'TransmittalController@cancelremoveTransmittal')->name('document.transmittal.cancelremovetransmittal');
    Route::get('tn/removetransmittal/{id}', 'TransmittalController@cancelremoveTransmittal')->name('document.transmittal.removetransmittal');
    Route::get('tn/edittransmittal/{id}','TransmittalController@editTransmittal')->name('document.transmittal.edittransmittal');
    Route::get('tn/showtransmittal/{id}','TransmittalController@showTransmittal')->name('document.transmittal.showtransmittal');
    Route::get('tn/create','TransmittalController@createL')->name('document.transmittal.create');
    Route::get('tn/transmittal/getfile/{id}/{filename}','TransmittalController@getFileReply')->name('document.transmittal.getfilereply');
    Route::post('tn/store','TransmittalController@storeTransmittal')->name('document.transmittal.store');
    Route::post('tn/update', 'TransmittalController@updateTransmittal')->name('document.transmittal.update');
    Route::resource('transmittal','TransmittalController');
    //Transmittal


    Route::get('doc/remove/{id}','DocumentController@removeDoc')->name('document.other.remove');
    Route::get('doc/requestremove/{id}','DocumentController@requestRemoveDoc')->name('document.other.requestremovedoc');
    Route::get('doc/verifiedremove/{id}', 'DocumentController@verifiedremovedoc')->name('document.other.verifiedremovedoc');
    Route::get('doc/cancelremove/{id}','DocumentController@cancelremovedoc')->name('document.other.cancelremovedoc');

    Route::get('doc/removeAttachment/{id}', 'DocumentController@removeDocAttachment')->name('document.other.removeattachment');
    Route::get('doc/getfile/{id}/{filename}','DocumentController@getFileReply')->name('document.other.getfilereply');
    Route::get('editdoc/{id}','DocumentController@editDoc')->name('document.doc.editdoc');
    Route::get('showdoc/{id}','DocumentController@showDoc')->name('document.doc.showdoc');
    Route::get('doc/create','DocumentController@createDoc')->name('document.doc.create');
    Route::post('doc/update', 'DocumentController@updateDoc')->name('document.letter.update');
    Route::post('doc/store','DocumentController@storeDocument')->name('document.doc.store');
    Route::resource('others','DocumentController');
});


