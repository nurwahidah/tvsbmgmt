<?php 

namespace App\Library;

use Auth;
use Form;
use Session;
use Illuminate\Support\Facades\Crypt;
use DB;
use Exception;
use Redirect;
use URL;
use Config;
use Carbon\Carbon;
use App\PrefixModel;
use JasperPHP;

/**
* global function place here
*/
class Globe
{

    public static function sp_prefix($type, $prefix)
    {
        
        $result = PrefixModel::where('pr_type', $type)->first();

        if (!$result):
            $result = new PrefixModel;
            $result->pr_type = $type;
            $result->pr_prefix = $prefix;
            $result->pr_prefixcounter = 0;
            $result->save();
        endif;

        $result->pr_prefixcounter += 1;
        $reference = $result->pr_prefix.str_pad($result->pr_prefixcounter, 5, "0", STR_PAD_LEFT);
        $result->save();

        return $reference;
    }

    public static function sp_prefixDoc($type,$prefix)
    {
        $result = PrefixModel::where('pr_type',$type)->first();

        if (!$result):
            $result = new PrefixModel;
            $result->pr_type = $type;
            $result->pr_prefix = $prefix;
            $result->pr_prefixcounter = 0;
            $result->save();
        endif;

        $result->pr_prefixcounter += 1;
        $reference = str_pad($result->pr_prefixcounter, 3, "0", STR_PAD_LEFT);
        $result->save();

        return $reference;
    }

    public static function statusLabel($status)
    {
        switch ($status) 
        {
            case 'N':
                // N = New
                $active = '<span class="label label-primary">New!</span>';
                break;
            case 'A':
                // A = Approve
                $active = '<span class="label label-success">Approved</span>';
                break;
            case 'V':
                // V = Verify
                $active = '<span class="label label-info">Verify</span>';
                break;
            case 'D':
                // V = Disapproved
                $active = '<span class="label label-danger">Disapproved</span>';
                break;
            default :
                $active = '<span class="label label-primary">New!</span>';
                break;
        }
        return $active;
    }


    public static function printiReport($ireportfilename, $param=[], $printFormat='pdf', $download='download') {
        
        $database = \Config::get('database.connections.mysql');
        $output = public_path() . '/reports/'.time().'_'.$ireportfilename;
        
        $parameters = ['SUBREPORT_DIR'=>public_path().'/reports/'
                
        ];
        
        // check if there is additional parameter supply
        if(count($param) > 0) {
        
            $parameters = array_merge($parameters,$param);
        }
        
        
       JasperPHP::process(
                public_path() . '/reports/'.$ireportfilename.'.jasper',
                $output,
                array($printFormat),
                $parameters,
                $database,
                false,
                false
                )->execute();  

        // for debugging process, comment above lines and uncomment this lines
    /*    print_r(\JasperPHP::process(
                public_path() . '/reports/'.$ireportfilename.'.jasper',
                $output,
                array($printFormat),
                $parameters,
                $database,
                false,
                false
                )->output()); exit();  */
        
            if ($download == 'download') {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.time().'_'.$ireportfilename.'.'.$printFormat);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($output.'.'.$printFormat));
                flush();
                readfile($output.'.'.$printFormat);
                unlink($output.'.'.$printFormat); // deletes the temporary file
            }
            else
            {
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename='.time().'_'.$ireportfilename.'.'.$printFormat);
                readfile($output.'.'.$printFormat);
                unlink($output.'.'.$printFormat); // deletes the temporary file
                flush(); // suppress undefined pageTitle error
            }
        
    }




}