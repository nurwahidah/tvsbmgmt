<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave2';
    protected $primaryKey = 'lve_id';
    protected $fillable = array();
}
