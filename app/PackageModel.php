<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'package';
    protected $primaryKey = 'pck_id';
    protected $fillable = array();
}
