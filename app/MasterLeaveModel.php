<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterLeaveModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'master_leave';
    protected $primaryKey = 'mlve_id';
    protected $fillable = array();
}
