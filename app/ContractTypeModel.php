<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractTypeModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'contract_type';
    protected $primaryKey = 'cnty_id';
    protected $fillable = array();
}
