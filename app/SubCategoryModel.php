<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoryModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'subcategory';
    protected $primaryKey = 'sub_id';
    protected $fillable = array();
}
