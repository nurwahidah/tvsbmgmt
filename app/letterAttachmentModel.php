<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class letterAttachmentModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'letterattachment';
    protected $primaryKey = 'lratt_id';
    protected $fillable = array();
}
