<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customerModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'customer';
    protected $primaryKey = 'cust_id';
    protected $fillable = array();
}
