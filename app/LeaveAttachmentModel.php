<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveAttachmentModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave_attachment';
    protected $primaryKey = 'lvat_id';
    protected $fillable = array();
}
