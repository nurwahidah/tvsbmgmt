<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrefixModel extends Model
{
    protected $guarded = [];
    protected $table = 'prefix';
    protected $primaryKey = 'pr_prefixid';
    protected $fillable = array();

    public static function getRunningNo($id)
    {
    	$prefix = PrefixModel::find($id);
    	$runningno = $prefix->pr_prefix.str_pad($prefix->pr_prefixcounter,7,'0', STR_PAD_LEFT);
    	$prefix->pr_prefixcounter += 1;
    	$prefix->save();

    	return $runningno;
    }
}
