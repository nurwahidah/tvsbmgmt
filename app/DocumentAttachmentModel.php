<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentAttachmentModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'documentattachment';
    protected $primaryKey = 'docatt_id';
    protected $fillable = array();
}
