<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class letterModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'letter';
    protected $primaryKey = 'let_id';
    protected $fillable = array();
}
