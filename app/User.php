<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Config;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function insertNewUser($request) {

        $this->name = $request->input('username');
        $this->password = Hash::make($request->input('password'));
        $this->email = $request->input('email');
        $this->created_at = Carbon::now(Config::get('constants.common.systemtimezone'));
        $this->save();
            
    }

    public function updateUser($userid, $data=[]) {
        
        $user = self::find($userid);
        $user->update($data);

    }

    public function deleteUser($userid) {
        
        $user = self::find($userid);
        $user->delete();
    }
}
