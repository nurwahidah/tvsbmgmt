<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveDetailModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave_detail';
    protected $primaryKey = 'lvdl_id';
    protected $fillable = array();
}
