<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OvertimeModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'overtime';
    protected $primaryKey = 'ot_id';
    protected $fillable = array();
}
