<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PublicHolidayModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'public_holiday';
    protected $primaryKey = 'ph_id';
    protected $fillable = array();
}
