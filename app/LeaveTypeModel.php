<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveTypeModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave_type';
    protected $primaryKey = 'lvty_id';
    protected $fillable = array();
}
