<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveBalanceModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave_balance';
    protected $primaryKey = 'lvbal_id';
    protected $fillable = array();
}
