<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransmittalAttachmentModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'transmittalattachment';
    protected $primaryKey = 'trmt_id';
    protected $fillable = array();
}
