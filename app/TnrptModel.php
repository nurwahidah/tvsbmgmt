<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TnrptModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'tnrpt';
    protected $primaryKey = 'tn_id';
    protected $fillable = array();
}
