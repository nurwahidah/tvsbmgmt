<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveCategoryModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'leave_category';
    protected $primaryKey = 'lvct_id';
    protected $fillable = array();
}
