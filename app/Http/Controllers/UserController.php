<?php

namespace App\Http\Controllers;

use Auth;   
use Hash;
use DB;
use Input;
use Form;
use Config;
use App;
use Validator;
use Redirect;
use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Library\Globe;
use App\User;

use App\LeaveModel;
use App\LeaveTypeModel;
use App\LeaveDetailModel;
use App\LeaveBalanceModel;
use App\LeaveCategoryModel;
use App\MasterLeaveModel;

use Illuminate\Support\Facades\Crypt;
//permission/roles
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $roles = Role::get();

        $ml = MasterLeaveModel::find(1);
        $ml_ResetLeave = MasterLeaveModel::find(2);
        $curryear = date('Y');

        //dd($ml->mlve_curryear);
        //dd($curryear);
        //$query = User::query();
        //hasanyrole('superadmin|admin')
        $query = User::select('*', 'roles.name as nameroles','roles.id as roleid', 'users.name as username','users.id as userid');

        if(auth()->user()->hasRole('superadmin'))
        {
            $userList = $query->leftjoin('model_has_roles', 'users.id','=','model_has_roles.model_id')
                          ->leftjoin('roles','roles.id','=','model_has_roles.role_id')
                          ->where('deleted_at',null)
                          ->orderBy('users.status','desc')
                          ->orderBy('users.id','desc')
                          ->get();
                          // ->paginate(3);
        }
        elseif(auth()->user()->hasRole('admin')){

            $userList = $query->leftjoin('model_has_roles', 'users.id','=','model_has_roles.model_id')
                              ->leftjoin('roles','roles.id','=','model_has_roles.role_id')
                              ->where('deleted_at',null)
                              ->where('roles.name','!=','superadmin')
                              ->where('roles.name','!=','admin')
                              ->orderBy('users.status','desc')
                              ->orderBy('users.id','desc')
                              ->get();
                              // ->paginate(3);

        }
        elseif(auth()->user()->hasRole('staff')) {
            $userList = $query->leftjoin('model_has_roles', 'users.id','=','model_has_roles.model_id')
                          ->leftjoin('roles','roles.id','=','model_has_roles.role_id')
                          ->where('deleted_at',null)
                          ->where('roles.name','!=','superadmin')
                          ->where('roles.name','!=','admin')
                          ->where('roles.name','!=','staff')
                          ->orderBy('users.status','desc')
                          ->orderBy('users.id','desc')
                          ->get();
                          // ->paginate(3);
        }

        $count = 1;
        return view('layouts.user.index',[
            'userList'=>$userList,
            'roles'=>$roles,
            'count'=>$count,
            'ml'=>$ml,
            'ml_ResetLeave'=>$ml_ResetLeave,
            'curryear'=>$curryear,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approveUser($userid)
    {

        // if(!Auth::user()->can('user.approve')):
        //     abort(403);
        // endif;
        if( !Auth::user()->hasPermissionTo('user.approve') ):
            abort(403);
        endif;

        DB::beginTransaction();
        try{

            if(!empty($userid)):
                $user = new User;

                // $data = ['status'=>'A'];
                // $user->updateUser($userid,$data);

                $updateUser = User::find($userid);
                $updateUser->status = 'A';
                $updateUser->status_approve = '1';
                $updateUser->save();

                
            else:
                throw new Exception("You didn't select any user to unable");
            endif;

            DB::commit();
            return Redirect()->back()->with('success', 'User has been approved!');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }


    }

    public function disapprovedUser($userid)
    {
        if(!Auth::user()->hasPermissionTo('user.disapproved')):
            abort(403);
        endif;

        DB::beginTransaction();
        try{

            if(!empty($userid)):
                $user = new User;

                // $data = ['status'=>'A'];
                // $user->updateUser($userid,$data);

                $deleteUser = User::find($userid);
                $deleteUser->delete();

            else:
                throw new Exception("You didn't select any user to reject");
            endif;

            DB::commit();
            return Redirect()->back()->with('success', 'User has been remove!');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }


    }


    public function enableUser($userid)
    {

        // if(!Auth::user()->can('user.enable')):
        //     abort(403);
        // endif;
        
        if( !Auth::user()->hasPermissionTo('user.enable') ):
            abort(403);
        endif;

        DB::beginTransaction();
        try{

            if(!empty($userid)):
                $user = new User;

                // $data = ['status'=>'A'];
                // $user->updateUser($userid,$data);

                $updateUser = User::find($userid);
                $updateUser->status = 'A';
                $updateUser->save();

            else:
                throw new Exception("You didn't select any user to unable");
            endif;

            DB::commit();
            return Redirect()->back()->with('success', 'Users has been activated!');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }


    }
    public function disableUser($userid) 
    {

        // if( !Auth::user()->can('user.disable') ):
        //     abort(403);
        // endif;

        if( !Auth::user()->hasPermissionTo('user.disable') ):
            abort(403);
        endif;
        
        DB::beginTransaction();
        try {
            
            if(!empty($userid)):
                $user = new User;

                // $data = ['status'=>'I'];
                // $user->updateUser($userid, $data);

                $updateUser = User::find($userid);
                $updateUser->status = 'I';
                $updateUser->save();
                
            else:
                throw new Exception("You didn't select any user to disable");               
            endif;

            DB::commit();
            return Redirect()->back()->with('warning', 'Users has been disabled!');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }


    public function showuser($id){

        $userdetail = User::findOrFail(Crypt::decrypt($id));

        return view('layouts.user.showuser',[
            'userdetail'=>$userdetail,
        ]);

        
    }

    public function showProfile(){

        return view('layouts.user.showprofile');
    }

    public function getFile($id)
    {
        $filename = User::find($id);
        return response()->download(public_path($filename->filedescription), null, [], null);
    }

    public function updateProfile(Request $request)
    {

        DB::beginTransaction();
        try{

            $editprofile = User::find(auth::user()->id);
            $editprofile->name = $request->name;
            $editprofile->designation = $request->designation;
            // $editprofile->email = $request->email;
            $editprofile->phone = $request->phone;

            if($request->file('fileattachment') !== null)
            {
                $file = $request->file('fileattachment');
                $destination_path = public_path().'/profile/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $editprofile->file = $fileName;
                $editprofile->filedescription = '/profile/'.$fileName;
            }

            
            $editprofile->save();

            DB::commit();
            return Redirect()->back()->with('success',$request->name .' has been updated');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }

    }

    public function createUser()
    {

        if( !Auth::user()->hasPermissionTo('user.create') ):
            abort(403);
        endif;

        $user = new User;
        if(auth()->user()->hasRole('superadmin'))
        {
            $roles = Role::all();
        }
        elseif(auth()->user()->hasRole('admin'))
        {
            $roles = Role::where('name','!=','superadmin')->where('name','!=','admin')->get();
        }
        elseif(auth()->user()->hasRole('staff'))
        {
            $roles = Role::where('name','=','customer')->get();
        }
        
        $year = date('Y');

        return view('layouts.user.createuser', [
            'user' => $user,
            'roles' => $roles,
            'year'=> $year,
        ]);
    }

    public function editUser($id,$roleid)
    {
        // if(!Auth::user()->can('update')): // refer table (user has permission) / (model has permission)
        //     abort(403);
        // endif;
        if( !Auth::user()->hasPermissionTo('user.edit') ):
            abort(403);
        endif;

        $user = User::findOrFail(Crypt::decrypt($id));

        if(auth()->user()->hasRole('superadmin'))
        {
            $roles = Role::all();
        }
        elseif(auth()->user()->hasRole('admin'))
        {
            $roles = Role::where('name','!=','superadmin')->where('name','!=','admin')->get();
        }
        elseif(auth()->user()->hasRole('staff'))
        {
            $roles = Role::where('name','=','customer')->get();
        }
        
        $year = date('Y');
        $ml_ResetLeave = MasterLeaveModel::find(2);

        return view('layouts.user.createuser',[
            'user'=>$user,
            'roles'=>$roles,
            'roleid'=>$roleid,
            'year'=>$year,
            'ml_ResetLeave'=>$ml_ResetLeave,
        ]);

    }

    public function storeUser(Request $request)
    {
        DB::beginTransaction();
        try{
            $messages = [
                'required' => ':attribute field is required.',
                'name.max' => 'The name may not be greater than 255 characters.',
                'email.required' => 'Email is required.',
                'email.email' => 'Invalid email.',
                'email.unique' => 'Email has already been taken.',
                'password.required|string' => 'Password is required.',
                'password.min' => 'Password must be at least 8 characters.',
                'password.regex'   => ':attribute must contain at least (1 capital letter, 1 small letter, 1 number, and 1 special character). Special Character include   # ? ! @ $ % ^ & * - ',
                'password.confirmed' => 'Password confirmation does not match.',
                'designation.required'=>'Designation is required',
                'role_id.required' => 'Role is required',
                'leave.required'=> 'Leave is required',
                'mc.required'=>'MC is required',
                'cfleave.required'=>'Carry forward (AL) is required',
                'cfmonthlimit.required'=>'Carry Forward (month limit) is required',

            ];

            $rules = array(
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 
                            'string',
                            'min:8',
                            'regex:/[a-z]/',         // must contain at least one lowercase letter
                            'regex:/[A-Z]/',        // at least 1 capital letter 
                            'regex:/[0-9]/',       // must contain at least one digit
                            'regex:/[#?!@$%^&*-]/', // must contain a special character
                            'confirmed'
                        ],
                'designation'=>'required',
                'role_id'=> 'required',
                'leave'=>'required',
                'mc'=>'required',
                'cfleave'=>'required',
                'cfmonthlimit'=>'required',
            );

            $validator = Validator::make(Input::all(), $rules, $messages);

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else
        {

            if ($request->password!=$request->password_confirmation) {
                 return Redirect()->back()->with('error','Error Confirmation Password');
            }
            $adduser = new User;
            $adduser->name = $request->name;

            $adduser->current_year = date('Y');
            $adduser->designation = $request->designation;
            $adduser->leave = $request->leave;
            $adduser->leave_used = 0;
            $adduser->leave_balance = $request->leave;

            $adduser->mc = $request->mc;
            $adduser->mc_used = 0;
            $adduser->mc_balance = $request->mc;

            $adduser->cf_leave = $request->cfleave;
            $adduser->cf_leaveused = 0;
            $adduser->cf_monthlimit = $request->cfmonthlimit;

            $adduser->rl = $request->rl;
            $adduser->rl_used = 0;
            $adduser->rl_balance = $request->rl;

            $adduser->email = $request->email;
            $adduser->password = bcrypt($request->password);
            $adduser->phone = $request->phone;
            $adduser->assignRole($request->role_id);
            $adduser->save();

            DB::commit();
            return Redirect('user')->with('success','User Successfully Registered');
        }
    }
    catch(Exception $error){
        DB::rollback();
        return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
    }

}

    public function updateUser(Request $request)
    {
        // if( !Auth::user()->hasPermissionTo('user.edit') ):
        //     abort(403);
        // endif;

        DB::beginTransaction();
        try{
            $messages = [
                'required' => ':attribute field is required.',
                'name.max' => 'The name may not be greater than 255 characters.',
                'email.required' => 'Email is required.',
                'email.email' => 'Invalid email.',
                'designation.required' => 'Company is required',
                'role_id.required' => 'Role is required',
                'leave.required'=> 'Leave is required',
                'mc.required'=>'MC is required',
                'cfleave.required'=>'Carry forward (AL) is required',
                'cfmonthlimit.required'=>'Carry Forward (month limit) is required',

            ];

            $rules = array(
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'designation'=> 'required',
                'role_id'=> 'required',
                'leave'=>'required',
                'mc'=>'required',
                'cfleave'=>'required',
                'cfmonthlimit'=>'required',
            );

            $validator = Validator::make(Input::all(), $rules, $messages);

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else
        {
            $ml_ResetLeave = MasterLeaveModel::find(2);

            $edituser = User::find($request->id);
            $edituser->name = $request->name;
            $edituser->designation = $request->designation;
            $edituser->email = $request->email;
            $edituser->phone = $request->phone;
            $edituser->leave = $request->leave;
            $edituser->mc = $request->mc;
            $edituser->cf_leave = $request->cfleave;
            $edituser->cf_monthlimit = $request->cfmonthlimit;

            $edituser->rl = $request->rl;
            $edituser->rl_used = 0;
            $edituser->rl_balance = $request->rl;

            if($edituser->leave_used == null) {
                $edituser->leave_used = 0;
                $edituser->leave_balance = $request->leave;
            }
            if($edituser->mc_used == null) {
                $edituser->mc_used = 0;
                $edituser->mc_balance = $request->mc;
            }

            if($ml_ResetLeave->mlve_status == 1 ){
                $edituser->leave_used = $request->leaveused;
                $edituser->leave_balance = $request->leavebalance;
                $edituser->mc_used = $request->mcused;
                $edituser->mc_balance = $request->mcbalance;
            }

            $edituser->syncRoles($request->role_id);
            $edituser->save();

            DB::commit();
            return Redirect('user')->with('success',$request->name .' has been updated');
        }
    }
    catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
    }

    
}

    public function changePassword(Request $request)
    {

        DB::beginTransaction();
        try{
            // dd(bcrypt($request->currpass));
            // dd(Auth::user()->password);

            if (!(Hash::check($request->currpass, Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }

            if(strcmp($request->currpass, $request->password) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }

            if(!(strcmp($request->password, $request->password_confirmation) == 0)){
                return redirect()->back()->with("error","Your new password does not matches with the confirm password you provided. Please try again.");
            }
            $messages = [
                'required' => ':attribute field is required.',
                'password.required|string' => 'Password is required.',
                'password.min' => 'Password must be at least 8 characters.',
                'password.regex'   => ':attribute must contain at least (1 capital letter, 1 small letter, 1 number, and 1 special character). Special Character include   # ? ! @ $ % ^ & * - ',
                'password.confirmed' => 'Password confirmation does not match.'

            ];

            $rules = array(
                'password' => ['required', 
                            'string',
                            'min:8',
                            'regex:/[a-z]/',         // must contain at least one lowercase letter
                            'regex:/[A-Z]/',        // at least 1 capital letter 
                            'regex:/[0-9]/',       // must contain at least one digit
                            'regex:/[#?!@$%^&*-]/', // must contain a special character
                            'confirmed'
                        ],
            );

            $validator = Validator::make(Input::all(), $rules, $messages);

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else
        {

            $validatedData = $request->validate([
            'currpass' => 'required',
            'password' => 'required',
            ]);
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->password);
            $user->save();

            DB::commit();
            return redirect()->back()->with("success","Password changed successfully !");

        }
    }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        
    }
}

    // public function updateProfile(Request $request)
    // {

    //     DB::beginTransaction();
    //     try{

    //         $editprofile = User::find(auth::user()->id);
    //         $editprofile->name = $request->name;
    //         $editprofile->email = $request->email;
    //         $editprofile->phone = $request->phone;
    //         $editprofile->save();

    //         DB::commit();
    //         return Redirect()->back()->with('success',$request->name .' has been updated');
    //     }
    //     catch(Exception $error){
    //         DB::rollback();
    //         return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
    //     }

    // }

    public function updateCF()
    {
        DB::beginTransaction();
        try{

            $ml = MasterLeaveModel::find(1);
            $cf = $ml->mlve_cf;
            $month = $ml->mlve_month;

            $alluser = User::all();

            foreach($alluser as $au) 
            {
                $leave = $au->leave;
                $mc = $au->mc;

                if($au->leave_balance < $cf) 
                {
                    $cfleave = $au->leave_balance;
                }
                else
                {
                    $cfleave = $cf;
                }

                $updU = User::find($au->id);
                $updU->cf_leave = $cfleave;
                $updU->cf_leaveused = 0;
                $updU->cf_monthlimit = $month;

                $updU->current_year = date('Y');
                $updU->leave_used = 0;
                $updU->leave_balance = $leave;
                $updU->mc_used = 0;
                $updU->mc_balance = $mc;
                $updU->save();
            }

            $updml = MasterLeaveModel::find(1);
            $updml->mlve_curryear = date('Y');
            $updml->save();

            DB::commit();
            return Redirect()->back()->with('success','Leave Updated for '.date('Y'));
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function updateRLO()
    {
        DB::beginTransaction();
        try{

            $ml = MasterLeaveModel::find(2);
            $ml->mlve_status = 1;
            $ml->save();

            DB::commit();
            return Redirect()->back()->with('success','Leave can be reset now.');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function updateRLC()
    {
        DB::beginTransaction();
        try{

            $ml = MasterLeaveModel::find(2);
            $ml->mlve_status = 0;
            $ml->save();

            DB::commit();
            return Redirect()->back()->with('success','Leave Permission has been closed.');
        }
        catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }



}
