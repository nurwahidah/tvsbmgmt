<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Response;
use View;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\transmittalModel;
use App\TransmittalAttachmentModel;
use App\customerModel;

class TransmittalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        DB::beginTransaction();
        try{

            $listTransmittal = transmittalModel::get();
            $count = 1;

            DB::commit();
            return view('layouts.document.transmittal.index',[
                        'listTransmittal'=>$listTransmittal,
                        'count'=>$count,
                       ]);
        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createL()
    {
        DB::beginTransaction();
        try{
            $cust = customerModel::all();

            DB::commit();
            return view('layouts.document.transmittal.createtransmittal',[
                        'cust'=>$cust,
            ]);
        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }
    }

    public function storeTransmittal(Request $request)
    {
        DB::beginTransaction();
        try{

        $rules = array(
                        'customer'=>'required',
                        'month'=>'required',
                        'year'=>'required',
                        'name'=>'required',
                        'description'=>'required',
                     );

            $customMessages = ['required'=> ':attribute field is required'];
            $validator = Validator::make(Input::all(), $rules, $customMessages);

    if($validator->fails()){

        return Redirect::back()->withInput()->withErrors($validator);
    }
    else
    {

            if($request->customer=="Others")
            {
                $reffNo = Globe::sp_prefixDoc('Transmittal','TRN');
                $number = 'TN/TVSB/'.$request->cust_other.'/'.$reffNo.'/'.$request->month.'/'.$request->year;
            }
            else{

                $reffNo = Globe::sp_prefixDoc('Transmittal','TRN');
                $number = 'TN/TVSB/'.$request->customer.'/'.$reffNo.'/'.$request->month.'/'.$request->year;
            }

            $storetransmittal = new TransmittalModel;
            $storetransmittal->tn_number = $number;
            $storetransmittal->tn_name = $request->name;
            $storetransmittal->tn_desc = $request->description;
            $storetransmittal->tn_preparedby = auth::user()->id;
            $storetransmittal->tn_prepareddate = Carbon::now();
            $storetransmittal->tn_permissiondel = '0';

            if($request->file('fileattachment') == null) {
                $storetransmittal->tn_attachment = 0;  
            }else{
                $storetransmittal->tn_attachment = 1;
            }

            $storetransmittal->save();

        if($request->file('fileattachment')) 
        { 
            
            for ($i=0; $i < count($request->file('fileattachment')) ; $i++) 
            { 
                $file = $request->file('fileattachment')[$i];
                $destination_path = public_path().'/files/transmittal/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $attch = new TransmittalAttachmentModel;
                $attch->trmt_transmittalid = $storetransmittal->tn_id;
                $attch->trmt_file = $fileName;
                $attch->trmt_path = '/files/transmittal/'.$fileName;
                $attch->save();
            }
        }
            

            DB::commit();
            return Redirect('document/transmittal')->with('success','Transmittal successfuly created');
    }

        }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function showTransmittal($id)
    {
        DB::beginTransaction();
        try{

            $transmittal = TransmittalModel::find($id);
            $transmittalAttach = TransmittalAttachmentModel::where('trmt_transmittalid',$id)->get();
            $count = 1;

            DB::commit();

            return view('layouts.document.transmittal.showtransmittal',[
                    'transmittal'=>$transmittal,
                    'transmittalAttach'=>$transmittalAttach,
                    'count'=>$count,
                   ]);

        }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
        }
    }

    public function editTransmittal($id)
    {

        DB::beginTransaction();
        try{

            $transmittal = TransmittalModel::find($id);
            $transmittalAttach = TransmittalAttachmentModel::where('trmt_transmittalid',$id)->get();
            $count = 1;

            DB::commit();
            return view('layouts.document.transmittal.edittransmittal',[
                'transmittal'=>$transmittal,
                'transmittalAttach'=>$transmittalAttach,
                'count'=>$count,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }

        
    }

    public function requestRemoveTransmittal($id)
    {
        DB::beginTransaction();
        try{

            DB::commit();

              $getPerm = TransmittalModel::find($id);
              $getPerm->tn_permissiondel = 1;
              $getPerm->save();

            DB::commit();
            return Redirect('document/transmittal')->with('success','Request has been submit');

        }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
        
    }

    public function verifiedremovetransmittal($id)
    {
        DB::beginTransaction();
        try{

            $transmittal = TransmittalModel::find($id);
            $transmittal->tn_verifiedby = Auth::user()->id;
            $transmittal->tn_verifieddate = Carbon::now();
            $transmittal->save();

            DB::commit();
            return Redirect('document/transmittal')->with('success','Transmittal has been verified to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }
    public function cancelremovetransmittal($id)
    {
        DB::beginTransaction();
        try{

            $transmittal = TransmittalModel::find($id);
            $transmittal->tn_permissiondel = 0;
            $transmittal->tn_verifiedby = null;
            $transmittal->save();

            DB::commit();
            return Redirect('document/transmittal')->with('success','Transmittal has been decline to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }
    

    public function removeTransmittal($id)
    {
        DB::beginTransaction();
        try{

            $attachment = TransmittalAttachmentModel::where('trmt_transmittalid',$id)
                                               ->get();

            if(count($attachment)>0)
            {
                foreach ($attachment as $att) {
                    
                    $file = $att->trmt_path;
                    $filename = public_path().$file;
                    \File::delete($filename);
                }
                
            }

              $deleteL = transmittalModel::findOrFail($id);
              $deleteL->forcedelete();

              $deleteAttachment = TransmittalAttachmentModel::where('trmt_transmittalid',$id)->forcedelete();

            DB::commit();
            return Redirect('document/transmittal')->with('success','Transmittal has been deleted');

        }catch(Exception $error){

              DB::rollback();
              return abort(404);
        }
        
    }

    public function getFileReply($id)
    {
        DB::beginTransaction();
        try{
            $filename = TransmittalAttachmentModel::find($id);

            DB::commit();
            return response()->download(public_path($filename->trmt_path), null, [], null);
        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }


    }

    public function updateTransmittal(Request $request)
    {
        DB::beginTransaction();
        try{

            $totalsize = $request->totalsize;

            if ($totalsize>390) {
                return Redirect()->back()->withInput()->with(['error'=>'Total file size is too large and exceeds 390 MB']);
            }

            $updateT = TransmittalModel::find($request->transmittalid);
            $updateT->tn_name = $request->name;
            $updateT->tn_desc = $request->description;
            $updateT->save();

            $haveattachment = $updateT->tn_attachment;

        if($request->counter != null) 
        {
            $counterItem = count($request->counter);

             //dd($counterItem);
            for($i=0; $i<$counterItem; $i++){

              //dd($_POST['fileattachment'][$i]);
            if($request->file('fileattachment')[$i]!= null) 
            {
                $haveattachment = 1;
               
                $file = $request->file('fileattachment')[$i];
                // $file = $_POST['fileattachment'][$i];
                $destination_path = public_path().'/files/transmittal/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $addAttach = new TransmittalAttachmentModel;
                $addAttach->trmt_transmittalid = $request->transmittalid;
                $addAttach->trmt_file = $fileName;
                $addAttach->trmt_path = '/files/transmittal/'.$fileName;
                $addAttach->save();
            }
        }
    }
            $updateT->tn_attachment = $haveattachment;
            $updateT->save();

            DB::commit();
            return Redirect()->back()->with('success', 'Transmittal has been updated successfully!');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage()]);

        }
    }

    public function removeTransmittalAttachment($id)
    {
        DB::beginTransaction();
        try {

            $deleteLA = TransmittalAttachmentModel::findOrFail($id);
            $tnid = $deleteLA->trmt_transmittalid;
            $file = $deleteLA->trmt_path;
            $deleteLA->forcedelete();

            //check still have an attachment or not
            $checkattach = TransmittalAttachmentModel::where('trmt_transmittalid',$tnid)->get();

            if(count($checkattach) == 0)
            {
                $updateNoti = TransmittalModel::find($tnid);
                $updateNoti->tn_attachment = 0;
            }else {
                $updateNoti = TransmittalModel::find($tnid);
                $updateNoti->tn_attachment = 1;
            }
            $updateNoti->save();

            $filename = public_path().$file;
            \File::delete($filename);
            
            DB::commit();
            return Redirect()->back()->with('success', 'Attachment has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }


}
