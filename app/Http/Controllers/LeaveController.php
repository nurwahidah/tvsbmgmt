<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\User;
use App\LeaveModel;
use App\LeaveTypeModel;
use App\LeaveDetailModel;
use App\LeaveBalanceModel;
use App\LeaveCategoryModel;
use App\MasterLeaveModel;
use App\LeaveAttachmentModel;
use App\StateCodeModel;
use App\PublicHolidayModel;
use App\ConfigSystemModel;

use PDF;
use JasperPHP;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        if(auth()->user()->hasRole('staff'))
        {
            if(Input::has('leavedate')) 
            {
                $leavelist = LeaveModel::select('*','leave2.created_at as leavecreated')
                               ->join('users','users.id','=','leave2.lve_preparedby')
                               ->join('leave_category','leave_category.lvct_id','=','leave2.lve_category')
                               ->join('leave_detail','leave_detail.lvdl_leaveid','=','leave2.lve_id')
                               ->where('leave_detail.lvdl_dateapply','=',Input::get('leavedate'))
                               ->where('leave2.lve_status','=','A')
                               ->orderBy('leave2.created_at','desc')
                               ->get();
            }
            else
            {
            $leavelist = LeaveModel::select('*','leave2.created_at as leavecreated')
                               ->join('users','users.id','=','leave2.lve_preparedby')
                               ->join('leave_category','leave_category.lvct_id','=','leave2.lve_category')
                               ->where('lve_status','=','A')
                               ->orwhere('lve_preparedby',auth()->user()->id)
                               ->orderBy('leave2.created_at','desc')
                               ->get();
            }
        }
        else
        {
            if(Input::has('leavedate')) 
            {
                $leavelist = LeaveModel::select('*','leave2.created_at as leavecreated')
                               ->join('users','users.id','=','leave2.lve_preparedby')
                               ->join('leave_category','leave_category.lvct_id','=','leave2.lve_category')
                               ->join('leave_detail','leave_detail.lvdl_leaveid','=','leave2.lve_id')
                               ->where('leave_detail.lvdl_dateapply','=',Input::get('leavedate'))
                               ->orderBy('leave2.created_at','desc')
                               ->get();
            }
            else
            {
                $leavelist = LeaveModel::select('*','leave2.created_at as leavecreated')
                               ->join('users','users.id','=','leave2.lve_preparedby')
                               ->join('leave_category','leave_category.lvct_id','=','leave2.lve_category')
                               ->orderBy('leave2.created_at','desc')
                               ->get();
            }
        }
        
        $count = 1;


        return view('layouts.leave.index',[
                    'leavelist'=>$leavelist,
                    'count'=>$count,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createLeave()
    {
        $leave = new LeaveModel;
        $leaveCat = LeaveCategoryModel::all();
        $leaveType = LeaveTypeModel::all();
        return view('layouts.leave.createleave',[
                    'leaveCat'=>$leaveCat,
                    'leaveType'=>$leaveType,
                    'leave'=>$leave,
        ]);
    }

    public function storeLeave(Request $request)
    {
        DB::beginTransaction();
        try{
            
            $rules = array(
                        'category'=>'required',
                        'type'=>'required',
                        'datestart'=>'required',
                        'dateend'=>'required',
                        'description'=>'required',
                     );

            $customMessages = ['required'=> ':attribute field is required'];

            $validator = Validator::make(Input::all(), $rules, $customMessages);

            $sysconfig = ConfigSystemModel::find(1);
            $statecode = $sysconfig->cs_statecode;

            $publicHoliday = PublicHolidayModel::where('ph_code',$statecode)->get();
            $leavearray[] = null;

            foreach($publicHoliday as $ph)
            {
                $leavearray[]= date('Y-m-d',strtotime($ph->ph_date));    
            }

        //dd($leavearray);

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $now = Carbon::now();
            $start = Carbon::parse($request->datestart);
            $end = Carbon::parse($request->dateend);
            $length = $start->diffInDays($end) + 1;
            //dd($length);
            // Carbon::now()->subDays(1)

            $leavedate2 = Carbon::parse($request->datestart);
            $nextdate2 = Carbon::parse($request->datestart);
            $countexcluded = 0;
            for ($i=1; $i<=$length ; $i++) 
            {
                
                //dd('wait');
                if(date('l', strtotime($nextdate2)) == 'Saturday' || date('l', strtotime($nextdate2)) == 'Sunday'
                    || in_array(date('Y-m-d', strtotime($nextdate2)), $leavearray)
                   )
                {
                    $countexcluded++;
                }
                $nextdate2 = date('Y-m-d', strtotime($leavedate2.' + '.$i.' days'));
            }
            $storeLeave = new LeaveModel;
            $storeLeave->lve_category = $request->category;
            $storeLeave->lve_type = $request->type;

            if($request->type == 1) //fullday
            {
                $storeLeave->lve_totalleave = $length - $countexcluded;
                $totalL = $length - $countexcluded;
            }
            else{ //halfday
                $storeLeave->lve_totalleave = ($length - $countexcluded)/2;
                $totalL = ($length - $countexcluded)/2;
            }
            
            $storeLeave->lve_description = $request->description;
            $storeLeave->lve_startdate = $request->datestart;
            $storeLeave->lve_enddate = $request->dateend;
            $storeLeave->lve_preparedby = auth::user()->id;
            $storeLeave->lve_prepareddate = Carbon::now();
            $storeLeave->save();


        if($request->file('fileattachment') !== null)
        {
                
            
            for ($i=0; $i < count($request->file('fileattachment')) ; $i++) 
            { 

                $file = $request->file('fileattachment')[$i];
                $destination_path = public_path().'/files/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $attch = new LeaveAttachmentModel;
                $attch->lvat_leaveid = $storeLeave->lve_id;
                $attch->lvat_file = $fileName;
                $attch->lvat_path = '/files/'.$fileName;
                $attch->save();
            }
        }

            // ***********
            // dd(Carbon::now()->subDays(1));
            // dd($request->dateend);
            //dd(date('l', strtotime($request->datestart))); //display day of date ex: Tuesday

            $leavedate = Carbon::parse($request->datestart);
            $nextdate = Carbon::parse($request->datestart);

            for ($i=1; $i<=$length ; $i++) 
            {
                
                if(date('l', strtotime($nextdate)) == 'Saturday' || date('l', strtotime($nextdate)) == 'Sunday'
                    || in_array(date('Y-m-d', strtotime($nextdate)), $leavearray)
                  )
                {
                }
                else{

                    $storeLeaveDetail = new LeaveDetailModel;
                    $storeLeaveDetail->lvdl_leaveid = $storeLeave->lve_id;
                    $storeLeaveDetail->lvdl_type = $request->type;
                    $storeLeaveDetail->lvdl_dateapply = $nextdate;
                    $storeLeaveDetail->save();
                    
                }
                $nextdate = date('Y-m-d', strtotime($leavedate.' + '.$i.' days'));
            }


    //***************************************** check before apply leave (same process in approveleave() )
                $updateLeave = LeaveModel::findOrFail($storeLeave->lve_id);
                $totalleave = $updateLeave->lve_totalleave;
                $preparedby = $updateLeave->lve_preparedby;
                $category = $updateLeave->lve_category;

                // $updateLeave->lve_status = 'A';
                // $updateLeave->lve_approvedby = auth::user()->id;
                // $updateLeave->lve_approveddate = Carbon::now();
                // $updateLeave->save();


                $updUserLeave = User::findOrFail($preparedby);

                $leaveused = $updUserLeave->leave_used + $totalleave;
                $leaveori = $updUserLeave->leave;
                $leaveusedori = $updUserLeave->leave_used;

                $mcused = $updUserLeave->mc_used + $totalleave;
                $mcori = $updUserLeave->mc;
                $mcusedori = $updUserLeave->mc_used;

                $rpleaveused = $updUserLeave->rl_used + $totalleave;
                $rpleaveori = $updUserLeave->rl;
                $rpleaveusedori = $updUserLeave->rl_used;


            //check apply leave date condition
            $passdate7 = date('Y-m-d', strtotime($now.' + 7 days'));
            $passdate14 = date('Y-m-d', strtotime($now.' + 14 days'));

            if($category == 1 || $category == 4) // AL || UL
            {
                if($totalL > 3) 
                {
                    if($start > $passdate14 && $end > $passdate14)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 14 days before.');
                    }
                }
                else
                {
                    if($start > $passdate7 && $end > $passdate7)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 7 days before.');
                    }
                }
                
            }
            elseif($category == 2 || $category == 5) // EL || MC
            {
                if($request->file('fileattachment') == null)
                {
                    return Redirect()->back()->withInput()->with('error', 'Attachment is required');
                }
            }
            else // RL
            {
                if ($totalL > 3) 
                {
                    if($start > $passdate14 && $end > $passdate14)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 14 days before.');
                    }
                }
                elseif($totalL >= 2 && $totalL <= 3) 
                {
                    if($start > $passdate7 && $end > $passdate7)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 7 days before.');
                    }
                }
                else
                {
                    //do nothing
                }
            }
    //close check apply leave date condition


        if($updUserLeave->leave !== null) 
        {
            if($category == 1 || $category == 2) // AL || EL
            {

                    if($leaveori == $leaveusedori) {
                        return Redirect()->back()->withInput()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                    }
                    else
                    {
                        if($updUserLeave->leave >= $leaveused) {

                            // $updUserLeave->leave_used = $leaveused;
                            // $updUserLeave->leave_balance = $leaveori - $leaveused;
                            // $updUserLeave->save();
                        }
                        else
                        {
                            $totalexceed = $leaveused - $leaveori;
                            return Redirect()->back()->withInput()->with('error', $totalexceed.' total leave has been exceeded');
                        }
                    }
            }
            elseif($category == 5) // MC
            {
                    

                    if($mcori == $mcusedori) {

                        // MC balance is 0, then deduct from anual leave (AL)
                        if($leaveori == $leaveusedori) {
                            return Redirect()->back()->withInput()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                        }
                        else
                        {
                            if($updUserLeave->leave >= $leaveused) {

                                // $updUserLeave->leave_used = $leaveused;
                                // $updUserLeave->leave_balance = $leaveori - $leaveused;
                                // $updUserLeave->save();
                            }
                            else
                            {
                                $totalexceed = $leaveused - $leaveori;
                                return Redirect()->back()->withInput()->with('error', $totalexceed.' total anual leave has been exceeded');
                            }
                        }
                    }
                    else
                    {
                        if($updUserLeave->mc >= $mcused) {

                            // $updUserLeave->mc_used = $mcused;
                            // $updUserLeave->mc_balance = $mcori - $mcused;
                            // $updUserLeave->save();
                        }
                        else
                        {
                            //deduct from anual leave
                            $totalexceed = $mcused - $mcori;
                            $leavemc = $leaveusedori + $totalexceed;

                            if($leaveori == $leaveusedori) {
                                return Redirect()->back()->withInput()->with('error', 'Leave Balance is 0');
                            }
                            else
                            {
                                if($updUserLeave->leave >= $leavemc) {

                                    // $updUserLeave->leave_used = $leavemc;
                                    // $updUserLeave->leave_balance = $leaveori - $leavemc;
                                    // $updUserLeave->mc_used = $mcori;
                                    // $updUserLeave->mc_balance = 0;
                                    // $updUserLeave->save();
                                }
                                else
                                {
                                    $totalexceed = $leavemc - $leaveori;
                                    return Redirect()->back()->withInput()->with('error', $totalexceed.' total leave has been exceeded. Please apply unpaid leave for exceeded leave.');
                                }
                            }
                        }
                    }
            }
            elseif($category == 3) // RL
            {
                if($rpleaveori == $rpleaveusedori) 
                {
                    return Redirect()->back()->with('error', 'Replacement Leave Balance is 0.');
                }
                else
                {
                    if($updUserLeave->rl >= $rpleaveused) {

                        // $updUserLeave->rl_used = $rpleaveused;
                        // $updUserLeave->rl_balance = $rpleaveori - $rpleaveused;
                        // $updUserLeave->save();
                    }
                    else
                    {
                        $totalexceed = $rpleaveused - $rpleaveori;
                        return Redirect()->back()->with('error', $totalexceed.' total replacement leave has been exceeded');
                    }
                }
            }
            else // UL
            {
                // do nothing
            }        
                    
        }

            $data = array(
                    'subject'=> 'Leave Request Notification',
                    'emailFrom'=> Auth::user()->email,
                    );

            Mail::to('wahidah@tiaravision.com')->send(new SendMail($data));

            DB::commit();
            return Redirect('leave')->with('success','Your Application successful submitted!');

        }
    }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }

    }

    public function showLeave($id){

        DB::beginTransaction();
        try{

            $ll = LeaveModel::findOrFail($id);
            if(Auth::user()->id == $ll->lve_preparedby && $ll->lve_readstatus == '0' && $ll->lve_status !== 'N')
            {
                $ll->lve_readstatus = '1';
                $ll->save();
            }

            $leaveCat = LeaveCategoryModel::all();
            $leaveType = LeaveTypeModel::all();
            $leavedetail = LeaveModel::join('leave_category','lvct_id','=','lve_category')
                                     ->join('leave_type','lvty_id','=','lve_type')
                                     ->where('lve_id',$id)
                                     ->first();

            $prepared = LeaveModel::join('users','users.id','=','leave2.lve_preparedby')
                                  ->where('lve_id',$id)
                                  ->first();



            $approved = LeaveModel::join('users','users.id','=','leave2.lve_approvedby')
                                  ->where('lve_id',$id)
                                  ->first();

            $attachment = LeaveAttachmentModel::join('leave2','lvat_leaveid','=','lve_id')
                                              ->where('lve_id',$id)
                                              ->get();



            DB::commit();

            return view('layouts.leave.showleave',[
                'leavedetail'=>$leavedetail,
                'leaveCat'=>$leaveCat,
                'leaveType'=>$leaveType,
                'attachment'=>$attachment,
                'prepared'=>$prepared,
                'approved'=>$approved,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }

        
    }

    public function editLeave($id){

        DB::beginTransaction();
        try{
            $leave = LeaveModel::findOrFail($id);
            $leaveCat = LeaveCategoryModel::all();
            $leaveType = LeaveTypeModel::all();
            $leavedetail = LeaveModel::join('leave_category','lvct_id','=','lve_category')
                                     ->join('leave_type','lvty_id','=','lve_type')
                                     ->where('lve_id',$id)
                                     ->first();

            $attachment = LeaveAttachmentModel::join('leave2','lvat_leaveid','=','lve_id')
                                              ->where('lve_id',$id)
                                              ->get();



            DB::commit();

            return view('layouts.leave.createleave',[
                'leavedetail'=>$leavedetail,
                'leaveCat'=>$leaveCat,
                'leaveType'=>$leaveType,
                'leave'=>$leave,
                'attachment'=>$attachment,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }

        
    }

    public function updateLeave(Request $request)
    {

        DB::beginTransaction();
        try{

            $rules = array(
                        'category'=>'required',
                        'type'=>'required',
                        'datestart'=>'required',
                        'dateend'=>'required',
                        'description'=>'required',
                     );
            $customMessages = ['required'=> ':attribute field is required'];

            $validator = Validator::make(Input::all(), $rules, $customMessages);

            $sysconfig = ConfigSystemModel::find(1);
            $statecode = $sysconfig->cs_statecode;

            $publicHoliday = PublicHolidayModel::where('ph_code',$statecode)->get();
            $leavearray[] = null;

            foreach($publicHoliday as $ph)
            {
                $leavearray[]= date('Y-m-d',strtotime($ph->ph_date));    
            }


        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else
        {
            $now = Carbon::now();
            $start = Carbon::parse($request->datestart);
            $end = Carbon::parse($request->dateend);
            $length = $start->diffInDays($end) + 1;
            //dd($length);
            // Carbon::now()->subDays(1)

            $leavedate2 = Carbon::parse($request->datestart);
            $nextdate2 = Carbon::parse($request->datestart);
            $countexcluded = 0;
            for ($i=1; $i<=$length ; $i++) 
            {
                if(date('l', strtotime($nextdate2)) == 'Saturday' || date('l', strtotime($nextdate2)) == 'Sunday'
                    || in_array(date('Y-m-d', strtotime($nextdate2)), $leavearray)
                  )
                {
                    $countexcluded++;
                }
                $nextdate2 = date('Y-m-d', strtotime($leavedate2.' + '.$i.' days'));
            }
            $storeLeave = LeaveModel::findOrFail($request->leaveid);
            $storeLeave->lve_category = $request->category;
            $storeLeave->lve_type = $request->type;

            if($request->type == 1) //fullday
            {
                $storeLeave->lve_totalleave = $length - $countexcluded;
                $totalL = $length - $countexcluded;
            }
            else{ //halfday
                $storeLeave->lve_totalleave = ($length - $countexcluded)/2;
                $totalL = ($length - $countexcluded)/2;
            }
            
            $storeLeave->lve_description = $request->description;
            $storeLeave->lve_startdate = $request->datestart;
            $storeLeave->lve_enddate = $request->dateend;
            $storeLeave->save();

        LeaveDetailModel::where('lvdl_leaveid',$request->leaveid)
                        ->forcedelete();

        if($request->file('fileattachment') !== null)
        {
                
            
            for ($i=0; $i < count($request->file('fileattachment')) ; $i++) { 

                $file = $request->file('fileattachment')[$i];
                $destination_path = public_path().'/files/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $attch = new LeaveAttachmentModel;
                $attch->lvat_leaveid = $storeLeave->lve_id;
                $attch->lvat_file = $fileName;
                $attch->lvat_path = '/files/'.$fileName;
                $attch->save();
            }
        }

            $leavedate = Carbon::parse($request->datestart);
            $nextdate = Carbon::parse($request->datestart);

            for ($i=1; $i<=$length ; $i++) 
            {
                if(date('l', strtotime($nextdate)) == 'Saturday' || date('l', strtotime($nextdate)) == 'Sunday'
                    || in_array(date('Y-m-d', strtotime($nextdate)), $leavearray)
                   )
                {
                }
                else{

                    $storeLeaveDetail = new LeaveDetailModel;
                    $storeLeaveDetail->lvdl_leaveid = $storeLeave->lve_id;
                    $storeLeaveDetail->lvdl_type = $request->type;
                    $storeLeaveDetail->lvdl_dateapply = $nextdate;
                    $storeLeaveDetail->save();
                    
                }
                $nextdate = date('Y-m-d', strtotime($leavedate.' + '.$i.' days'));
            }


    //***************************************** check before apply leave (same process in approveleave() )
                $updateLeave = LeaveModel::findOrFail($storeLeave->lve_id);
                $totalleave = $updateLeave->lve_totalleave;
                $preparedby = $updateLeave->lve_preparedby;
                $category = $updateLeave->lve_category;

                // $updateLeave->lve_status = 'A';
                // $updateLeave->lve_approvedby = auth::user()->id;
                // $updateLeave->lve_approveddate = Carbon::now();
                // $updateLeave->save();


                $updUserLeave = User::findOrFail($preparedby);

                $leaveused = $updUserLeave->leave_used + $totalleave;
                $leaveori = $updUserLeave->leave;
                $leaveusedori = $updUserLeave->leave_used;

                $mcused = $updUserLeave->mc_used + $totalleave;
                $mcori = $updUserLeave->mc;
                $mcusedori = $updUserLeave->mc_used;

                $rpleaveused = $updUserLeave->rl_used + $totalleave;
                $rpleaveori = $updUserLeave->rl;
                $rpleaveusedori = $updUserLeave->rl_used;


    //check apply leave date condition
            $passdate7 = date('Y-m-d', strtotime($now.' + 7 days'));
            $passdate14 = date('Y-m-d', strtotime($now.' + 14 days'));

            if($category == 1 || $category == 4) // AL || UL
            {
                if($totalL > 3) 
                {
                    if($start > $passdate14 && $end > $passdate14)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 14 days before.');
                    }
                }
                else
                {
                    if($start > $passdate7 && $end > $passdate7)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 7 days before.');
                    }
                }
                
            }
            elseif($category == 2 || $category == 5) // EL || MC
            {
                if($request->file('fileattachment') == null)
                {
                    return Redirect()->back()->withInput()->with('error', 'Attachment is required');
                }
            }
            else // RL
            {
                if ($totalL > 3) 
                {
                    if($start > $passdate14 && $end > $passdate14)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 14 days before.');
                    }
                }
                elseif($totalL >= 2 && $totalL <= 3) 
                {
                    if($start > $passdate7 && $end > $passdate7)
                    {
                        //do nothing, proceed
                    }
                    else
                    {
                        return Redirect()->back()->withInput()->with('error', 'Total Leave apply is '.$totalL.' day(s). Leave must be applied 7 days before.');
                    }
                }
                else
                {
                    //do nothing
                }
            }
    //close check apply leave date condition


        if($updUserLeave->leave !== null) 
        {
            if($category == 1 || $category == 2) // AL || EL
            {

                    if($leaveori == $leaveusedori) {
                        return Redirect()->back()->withInput()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                    }
                    else
                    {
                        if($updUserLeave->leave >= $leaveused) {

                        }
                        else
                        {
                            $totalexceed = $leaveused - $leaveori;
                            return Redirect()->back()->withInput()->with('error', $totalexceed.' total leave has been exceeded');
                        }
                    }
            }
            elseif($category == 5) // MC
            {
                    

                    if($mcori == $mcusedori) {

                        // MC balance is 0, then deduct from anual leave (AL)
                        if($leaveori == $leaveusedori) {
                            return Redirect()->back()->withInput()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                        }
                        else
                        {
                            if($updUserLeave->leave >= $leaveused) {

                            }
                            else
                            {
                                $totalexceed = $leaveused - $leaveori;
                                return Redirect()->back()->withInput()->with('error', $totalexceed.' total anual leave has been exceeded');
                            }
                        }
                    }
                    else
                    {
                        if($updUserLeave->mc >= $mcused) {

                        }
                        else
                        {
                            //deduct from anual leave
                            $totalexceed = $mcused - $mcori;
                            $leavemc = $leaveusedori + $totalexceed;

                            if($leaveori == $leaveusedori) {
                                return Redirect()->back()->withInput()->with('error', 'Leave Balance is 0');
                            }
                            else
                            {
                                if($updUserLeave->leave >= $leavemc) {

                                }
                                else
                                {
                                    $totalexceed = $leavemc - $leaveori;
                                    return Redirect()->back()->withInput()->with('error', $totalexceed.' total leave has been exceeded. Please apply unpaid leave for exceeded leave.');
                                }
                            }
                        }
                    }
            }
            elseif($category == 3) // RL
            {
                if($rpleaveori == $rpleaveusedori) 
                {
                    return Redirect()->back()->with('error', 'Replacement Leave Balance is 0.');
                }
                else
                {
                    if($updUserLeave->rl >= $rpleaveused) {
                    }
                    else
                    {
                        $totalexceed = $rpleaveused - $rpleaveori;
                        return Redirect()->back()->with('error', $totalexceed.' total replacement leave has been exceeded');
                    }
                }
            }
            else // UL
            {
                // do nothing
            }    
                    
        }

            DB::commit();
            return Redirect('leave')->with('success','Leave has been updated!!');

        }
    }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }

    }

    public function approveLeave($leaveid)
    {
        DB::beginTransaction();
        try{

                $updateLeave = LeaveModel::findOrFail($leaveid);
                $totalleave = $updateLeave->lve_totalleave;
                $preparedby = $updateLeave->lve_preparedby;
                $category = $updateLeave->lve_category;

                $updateLeave->lve_status = 'A';
                $updateLeave->lve_approvedby = auth::user()->id;
                $updateLeave->lve_approveddate = Carbon::now();
                $updateLeave->save();

        //cf process
                $updUserLeave = User::findOrFail($preparedby);
                $cfleave = $updUserLeave->cf_leave;
                $cfleaveused = $updUserLeave->cf_leaveused;
                $cfmonthlimit = $updUserLeave->cf_monthlimit;

            if($category == 1 || $category == 2) // AL || EL
            {
                $enter = 'no';
                if(date('n') > $cfmonthlimit && $cfleaveused !== 0) //burn cf leave as current month more than month limit that has been set
                {
                    $updCF = User::find($preparedby);
                    $updCF->cf_leave = 0;
                    $updCF->save();

                    $cfleave = 0;
                }

                if($totalleave > $cfleave && $cfleave !== 0)
                {
                    //$totalleave = $totalleave - $cfleave;
                    $totalleave = $totalleave - $cfleave;
                    $enter = 'yes';

                    $upCF = User::find($preparedby);
                    $upCF->cf_leave = 0;
                    $upCF->cf_leaveused = $cfleave;
                    $upCF->save();

                }
                if($totalleave <= $cfleave && $cfleave !== 0 && $enter=='no')
                {

                    $upCF = User::find($preparedby);
                    $upCF->cf_leave = $cfleave - $totalleave;
                    $upCF->cf_leaveused = $cfleaveused + $totalleave;
                    $upCF->save();
                    $totalleave = 0;
                }

            }

        //close cf process

                $leaveused = $updUserLeave->leave_used + $totalleave;
                $leaveori = $updUserLeave->leave;
                $leaveusedori = $updUserLeave->leave_used;

                
                $mcused = $updUserLeave->mc_used + $totalleave;
                $mcori = $updUserLeave->mc;
                $mcusedori = $updUserLeave->mc_used;

                $rpleaveused = $updUserLeave->rl_used + $totalleave;
                $rpleaveori = $updUserLeave->rl;
                $rpleaveusedori = $updUserLeave->rl_used;


        if($updUserLeave->leave !== null) 
        {
            if($category == 1 || $category == 2) // AL || EL
            {
                    

                    if($leaveori == $leaveusedori) {
                        return Redirect()->back()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                    }
                    else
                    {
                        if($updUserLeave->leave >= $leaveused) {

                            $updUserLeave->leave_used = $leaveused;
                            $updUserLeave->leave_balance = $leaveori - $leaveused;
                            $updUserLeave->save();
                        }
                        else
                        {
                            $totalexceed = $leaveused - $leaveori;
                            return Redirect()->back()->with('error', $totalexceed.' total leave has been exceeded');
                        }
                    }
            }
            elseif($category == 5) // MC
            {
                    

                    if($mcori == $mcusedori) {

                        // MC balance is 0, then deduct from anual leave (AL)
                        //return Redirect()->back()->with('error', 'MC Balance is 0');

                        if($leaveori == $leaveusedori) {
                            return Redirect()->back()->with('error', 'Anual Leave Balance is 0. Please apply for unpaid leave');
                        }
                        else
                        {
                            if($updUserLeave->leave >= $leaveused) {

                                $updUserLeave->leave_used = $leaveused;
                                $updUserLeave->leave_balance = $leaveori - $leaveused;
                                $updUserLeave->save();
                            }
                            else
                            {
                                $totalexceed = $leaveused - $leaveori;
                                return Redirect()->back()->with('error', $totalexceed.' total anual leave has been exceeded');
                            }
                        }
                    }
                    else
                    {
                        if($updUserLeave->mc >= $mcused) {

                            $updUserLeave->mc_used = $mcused;
                            $updUserLeave->mc_balance = $mcori - $mcused;
                            $updUserLeave->save();
                        }
                        else
                        {
                            //deduct from anual leave
                            $totalexceed = $mcused - $mcori;
                            $leavemc = $leaveusedori + $totalexceed;
                            // return Redirect()->back()->with('error', $totalexceed.' total leave has been exceeded');

                            if($leaveori == $leaveusedori) {
                                return Redirect()->back()->with('error', 'Leave Balance is 0');
                            }
                            else
                            {
                                if($updUserLeave->leave >= $leavemc) {

                                    $updUserLeave->leave_used = $leavemc;
                                    $updUserLeave->leave_balance = $leaveori - $leavemc;
                                    $updUserLeave->mc_used = $mcori;
                                    $updUserLeave->mc_balance = 0;
                                    $updUserLeave->save();
                                }
                                else
                                {
                                    $totalexceed = $leavemc - $leaveori;
                                    return Redirect()->back()->with('error', $totalexceed.' total leave has been exceeded. Please apply unpaid leave for exceeded leave.');
                                }
                            }
                        }
                    }
            }
            elseif($category == 3) // RL
            {
                if($rpleaveori == $rpleaveusedori) 
                {
                    return Redirect()->back()->with('error', 'Replacement Leave Balance is 0.');
                }
                else
                {
                    if($updUserLeave->rl >= $rpleaveused) {

                        $updUserLeave->rl_used = $rpleaveused;
                        $updUserLeave->rl_balance = $rpleaveori - $rpleaveused;
                        $updUserLeave->save();
                    }
                    else
                    {
                        $totalexceed = $rpleaveused - $rpleaveori;
                        return Redirect()->back()->with('error', $totalexceed.' total leave has been exceeded');
                    }
                }
            }
            else // UL
            {
                // do nothing
            }    
                    
        }
        else
        {
            return Redirect()->back()->with('error', 'Total Leave '.$updUserLeave->name. 'is null');
        }


            DB::commit();
            return Redirect()->back()->with('success', 'Leave has been approved');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }


    }

    public function disapprovedLeave($leaveid)
    {
        DB::beginTransaction();
        try{

                $updateLeave = LeaveModel::findOrFail($leaveid);
                $updateLeave->lve_status = 'D';
                $updateLeave->lve_approvedby = auth::user()->id;
                $updateLeave->lve_approveddate = Carbon::now();
                $updateLeave->save();

            DB::commit();
            return Redirect()->back()->with('success', 'Done!');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }


    }

    public function removeLeave($leaveid) 
    {
        DB::beginTransaction();
        try {

            $deleteLeave = LeaveModel::findOrFail($leaveid);
            $deleteLeave->forcedelete();

            LeaveDetailModel::where('lvdl_leaveid',$leaveid)
                            ->forcedelete();
            
            
            DB::commit();
            return Redirect()->back()->with('success', 'Leave has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public function showMasterLeave()
    {
        DB::beginTransaction();
        try{

            $mastleave = MasterLeaveModel::where('mlve_id','=','1')->get();

            DB::commit();

            return view('layouts.leave.master.index',[
                'mastleave'=>$mastleave,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }
    }

    public static function getCF($id)
    {
        
        $ml = MasterLeaveModel::findOrFail($id);


        $formgroup1 = '<div class=" item form-group">
                        <div class="x_content">
                            <input type="hidden" name="mlveid" value="'.$id.'">
                            <label class="control-label" title="">'.__('Desription').'</label>
                            <input type="text" class="form-control" name="description" value="'.$ml->mlve_description.'">
                            <label class="control-label">'.__('CF').'</label>
                            <input type="text" class="form-control" name="cf" value="'.$ml->mlve_cf.'">
                            <label class="control-label">'.__('Month Limit').'</label>
                            <input type="text" class="form-control" name="month" value="'.$ml->mlve_month.'">
                        </div>
                      </div>';

        $html2 = $formgroup1;

        $data = ['html'=>$html2];
        
        return json_encode($data);

    }

    public function updateML(Request $request)
    {
        DB::beginTransaction();
        try{

            $updML = MasterLeaveModel::find($request->mlveid);
            $updML->mlve_description = $request->description;
            $updML->mlve_cf = $request->cf;
            $updML->mlve_month = $request->month;
            $updML->updated_at = now();
            $updML->save();

            DB::commit();
            return Redirect()->back()->with('success','Master Leave has been updated');

        }catch(Exception $error){

            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public function getFile($id)
    {
        $filename = LeaveAttachmentModel::find($id);
        return response()->download(public_path($filename->lvat_path), null, [], null);
    }

    public function generatePDF($id)
    {
        // $leaveinfo = LeaveModel::join('users','users.id','=','leave2.lve_preparedby');
        //dd($leaveinfo);
        $leaveall = LeaveModel::select('*','leave2.created_at as leavecreated')
                               ->join('users','users.id','=','leave2.lve_preparedby')
                               ->join('leave_category','leave_category.lvct_id','=','leave2.lve_category')
                               ->orderBy('leave2.created_at','desc')
                               ->get();

        $leavedetail = LeaveModel::join('leave_category','lvct_id','=','lve_category')
                                     ->join('leave_type','lvty_id','=','lve_type')
                                     ->where('lve_id',$id)
                                     ->first();

        $prepared = LeaveModel::join('users','users.id','=','leave2.lve_preparedby')
                                  ->where('lve_id',$id)
                                  ->first();

        $approved = LeaveModel::join('users','users.id','=','leave2.lve_approvedby')
                                  ->where('lve_id',$id)
                                  ->first();


        $data = ['title' => 'Leave Application Form',
                 'employeename'=>$prepared->name,
                 'designation'=>$prepared->designation,
                 'leaveall'=>$leaveall,

                 'typeleave'=>$leavedetail->lvty_name,
                 'typedetail'=>$leavedetail->lvty_description,
                 'time'=>$leavedetail->lvty_description,
                 'totalleave'=>$leavedetail->lve_totalleave,
                 'startdate' => $leavedetail->lve_startdate,
                 'enddate'=> $leavedetail->lve_enddate,
                 'desc'=> $leavedetail->lve_description,
                 'prepareddate'=>$leavedetail->lve_prepareddate,
                 'category'=>$leavedetail->lvct_description,

                 'approvedby'=>$approved->name,
                 'approveddate'=>$approved->lve_approveddate

                ];
        $pdf = PDF::loadView('layouts.leave.testviewpdf', $data);

        return $pdf->download('leaveform.pdf');
        //return $pdf->download('example.pdf', [], 'inline');

    }

    public function getleavereport($id)
    {

        Globe::printiReport('rpt_leave',['leaveid'=>$id]);
        return Redirect::action(Request::url());
    }


}
