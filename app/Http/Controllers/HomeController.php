<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\LeaveModel;
use App\OvertimeModel;
use App\letterModel;
use App\documentOtherModel;
use App\transmittalModel;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //1.model_has_roles
        // $user = auth()->user();
        // $user->assignRole('superadmin');

        //ROLE HAS PERMISSION
        // $role = Role::find(1);
        // $role->syncPermissions(['menu.admin','menu.admin.users','menu.admin.roles','menu.admin.permission','menu.leave','user.create','user.edit','user.enable','user.disable','user.approve','user.disapproved','role.create','role.edit','role.permission','permission.create','permission.edit']);

        $countrequest = LeaveModel::select(DB::raw('COUNT(leave2.lve_id) as leaverequest'))
                                ->where('lve_status','=','N')
                                ->first();
        $countOTrequest = OvertimeModel::select(DB::raw('COUNT(overtime.ot_id) as otrequest'))
                                ->where('ot_status','=','N')
                                ->first();

        $newleave = LeaveModel::join('users','users.id','=','lve_preparedby')->where('lve_status','=','N')->get();

        $newOT = OvertimeModel::join('users','users.id','=','ot_preparedby')->where('ot_status','=','N')->get();

        $countleavenoti = LeaveModel::select(DB::raw('COUNT(leave2.lve_id) as leavenoti'))
                                ->where('lve_readstatus','=','0')
                                ->whereIn('lve_status',array('A','D'))
                                ->where('lve_preparedby',auth()->user()->id)
                                ->first();

        $countOTnoti = OvertimeModel::select(DB::raw('COUNT(overtime.ot_id) as OTnoti'))
                                ->where('ot_readstatus','=','0')
                                ->whereIn('ot_status',array('A','D'))
                                ->where('ot_preparedby',auth()->user()->id)
                                ->first();

        $newleavenoti = LeaveModel::join('users','users.id','=','lve_preparedby')
                                  ->where('lve_readstatus','=','0')
                                  ->whereIn('lve_status',array('A','D'))
                                  ->where('lve_preparedby',auth()->user()->id)
                                  ->get();

        $newOTnoti = OvertimeModel::join('users','users.id','=','ot_preparedby')
                                  ->where('ot_readstatus','=','0')
                                  ->whereIn('ot_status',array('A','D'))
                                  ->where('ot_preparedby',auth()->user()->id)
                                  ->get();

        $countLetternoti = letterModel::select(DB::raw('COUNT(letter.let_id) as letterrequest'))
                                ->where('let_permissiondel','=','1')
                                ->first();

        $countTransmittalnoti = transmittalModel::select(DB::raw('COUNT(transmittal.tn_id) as transmittalrequest'))
                                ->where('tn_permissiondel','=','1')
                                ->first();

        $countDocnoti = documentOtherModel::select(DB::raw('COUNT(documentother.doc_id) as documentrequest'))
                                ->where('doc_permissiondel','=','1')
                                ->first();

        $delLetternoti = letterModel::join('users','users.id','=','let_preparedby')
                                    ->where('let_permissiondel','=','1')
                                    ->get();

        $delTransmittalnoti = transmittalModel::join('users','users.id','=','tn_preparedby')
                                              ->where('tn_permissiondel','=','1')->get();

        $delDocnoti = documentOtherModel::join('users','users.id','=','doc_preparedby')
                                        ->where('doc_permissiondel','=','1')->get();

        return view('home',[
                    'newleave'=>$newleave,
                    'newOT'=>$newOT,
                    'countrequest'=>$countrequest,
                    'countOTrequest'=>$countOTrequest,
                    'countleavenoti'=>$countleavenoti,
                    'countOTnoti'=>$countOTnoti,
                    'newleavenoti'=>$newleavenoti,
                    'newOTnoti'=>$newOTnoti,
                    'countLetternoti'=>$countLetternoti,
                    'countTransmittalnoti'=>$countTransmittalnoti,
                    'countDocnoti'=>$countDocnoti,
                    'delLetternoti'=>$delLetternoti,
                    'delTransmittalnoti'=>$delTransmittalnoti,
                    'delDocnoti'=>$delDocnoti,
        ]);


    }
}
