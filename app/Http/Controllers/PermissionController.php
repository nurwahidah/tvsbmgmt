<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use DB;
use Config;
use App;
use Validator;
use Redirect;
use Exception;
use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Crypt;
//permission/roles
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $perm = Permission::orderBy('id','DESC')->get();
        $count = 1;
        
        return view('layouts.admin.permission.index',[
            'perm'=>$perm,
            'count'=>$count,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getRolePermission($id){
        
        $permission = Permission::orderBy('description')->get();

        $role = Role::findOrFail($id);

        $rolehaspermissions = DB::table('role_has_permissions')->where('role_id',$id)->pluck('permission_id')->toArray();

        $html = '<div class="checkbox"><label><input type="checkbox" onclick="toggleCheck(this)" name="checkall" class="checkall">All</label></div>';
        $checked = '';
        $width = 40;
        $i = 0;
        foreach($permission as $perm){

            $i++;
            if(in_array($perm->id,$rolehaspermissions)){
                $checked = 'checked';
            }else{
                $checked = '';
            }

            if ($i % 50 == 0){
                $width += 15;
                $html .= '</div><div class="col-md-3 col-sm-3 col-xs-12">';
            }

            $html .= '<div class="checkbox"><label><input type="checkbox" name="permission[]" value="'.$perm->name.'" '.$checked.'>'.$perm->description.'</label></div>';

        }

        if($i>49){
            $collabel  = '2';
            $collabel2 = '3';
        }else{
            $collabel = '3';
            $collabel2 = '7';
        }

        $formgroup1 = '<div class=" item form-group"><input type="hidden" name="roleidmodal2" value="'.$id.'"><input type="hidden" name="rolenamemodal2" value="'.strtoupper($role->name).'"><label class="control-label col-md-'.$collabel.' col-sm-'.$collabel.' col-xs-12">'.__('Role Name').'</label><div class="col-md-'.$collabel2.' col-sm-'.$collabel2.' col-xs-12"><label class="control-label">'.strtoupper($role->name).'</label></div></div>';

        $formgroup2 = '<div class="item form-group"><div class="form-group"><label class="control-label col-md-'.$collabel.' col-sm-'.$collabel.' col-xs-12">'.__('Permission').'</label><div class="col-md-'.$collabel2.' col-sm-'.$collabel2.' col-xs-12">';

        $html .= '</div></div>';

        $html2 = $formgroup1.$formgroup2.$html;

        $data = ['html'=>$html2, 'width'=>$width];
        
        return json_encode($data);

    }

    public function addPerm(Request $request)
    {
        // dd($request->rolename);
        DB::beginTransaction();
        try{

            $perm = Permission::create(['name' => $request->permname,
                                        'description'=>$request->permdesc]);

            DB::commit();
            return Redirect()->back()->with('success','Success Create New Permission');

        }catch(Exception $error){

            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public static function getPerm($id){
        
        $permission = Permission::findOrFail($id);


        $formgroup1 = '<div class=" item form-group">
                        <div class="x_content">
                            <input type="hidden" name="permid" value="'.$id.'">
                            <label class="control-label" title="use for programmer (source code)">'.__('Permission Name').'</label>
                            <input type="text" class="form-control" name="permname" value="'.$permission->name.'">
                            <label class="control-label">'.__('Description').'</label>
                            <input type="text" class="form-control" name="permdesc" value="'.$permission->description.'">
                        </div>
                      </div>';

        $html2 = $formgroup1;

        $data = ['html'=>$html2];
        
        return json_encode($data);

    }

    public function updatePerm(Request $request)
    {
        DB::beginTransaction();
        try{

            $updPerm = Permission::find($request->permid);
            $updPerm->name = $request->permname;
            $updPerm->description = $request->permdesc;
            $updPerm->updated_at = now();
            $updPerm->save();

            DB::commit();
            return Redirect()->back()->with('success','Permission has been updated');

        }catch(Exception $error){

            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }
}
