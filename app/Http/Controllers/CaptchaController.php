<?php

namespace App\Http\Controllers;

use Anam\Captcha\Captcha;
use Illuminate\Http\Request;

class CaptchaController extends Controller
{
    public function store(Request $request, Captcha $captcha)
    {
        $response = $captcha->check($request);

        if (! $response->isVerified()) {
            dd($response->errors());
        }
        
        dd($response->hostname());
    }
}
