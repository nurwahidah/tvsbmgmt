<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Response;
use View;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\documentOtherModel;
use App\DocumentAttachmentModel;
use App\customerModel;
use App\PackageModel;
use App\SubCategoryModel;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        DB::beginTransaction();
        try
        {

        $listother = documentOtherModel::join('customer','customer.cust_id','=','documentother.doc_customerid')
                                       ->join('subcategory','subcategory.sub_id','=','documentother.doc_subid')
                                       ->get();
        $count = 1;

        DB::commit();

        return view('layouts.document.other.index',[
                    'listother'=>$listother,
                    'count'=>$count,
                   ]);

        }catch(Exception $error){
            
            DB::rollback();
            return abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createDoc()
    {
        DB::beginTransaction();
        try
        {

            $cust = customerModel::all()->sortBy('cust_name');
            $subcat = SubCategoryModel::all()->sortBy('sub_name');
            $package = PackageModel::all()->sortBy('pck_name');

            DB::commit();

            return view('layouts.document.other.createdoc',[
                        'cust'=>$cust,
                        'subcat'=>$subcat,
                        'package'=>$package,
            ]);

        }catch(Exception $error){
            
            DB::rollback();
            return abort(404);
        }
    }

    public function storeDocument(Request $request)
    {
        DB::beginTransaction();
        try{

            $rules = array(
                        'subcategory'=>'required',
                        'customer'=>'required',
                        'package'=>'required',
                        'docnumber'=>'required',
                        'filename'=>'required',
                        'description'=>'required'
                     );

            $customMessages = ['required'=> ':attribute field is required'];
            $validator = Validator::make(Input::all(), $rules, $customMessages);

    if($validator->fails()){

        return Redirect::back()->withInput()->withErrors($validator);
    }
    else{

            $storedoc = new documentOtherModel;
            $storedoc->doc_number = $request->docnumber;
            $storedoc->doc_name = $request->filename;
            $storedoc->doc_description = $request->description;
            $storedoc->doc_package = $request->package;
            $storedoc->doc_otherpack = $request->doc_other;
            $storedoc->doc_subid = $request->subcategory;
            $storedoc->doc_subother = $request->cat_other;
            $storedoc->doc_customerid = $request->customer;
            $storedoc->doc_customerother = $request->cust_other;
            $storedoc->doc_preparedby = auth::user()->id;
            $storedoc->doc_modifiedby = auth::user()->id;
            $storedoc->doc_prepareddate = Carbon::now();
            $storedoc->doc_permissiondel = '0';

            if($request->file('fileattachment') == null) {
                $storedoc->doc_attachment = 0;  
            }else{
                $storedoc->doc_attachment = 1;
            }

            $storedoc->save();

          if($request->file('fileattachment')) 
          { 
            
            for($i=0; $i < count($request->file('fileattachment')) ; $i++) 
            { 
                $file = $request->file('fileattachment')[$i];
                $destination_path = public_path().'/files/document/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $attch = new DocumentAttachmentModel;
                $attch->docatt_documentid = $storedoc->doc_id;
                $attch->docatt_file = $fileName;
                $attch->docatt_path = '/files/document/'.$fileName;
                $attch->save();
            }
          }

            DB::commit();
            return Redirect('document/others')->with('success','Document successfuly created');
    }

        }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function showDoc($id)
    {
        DB::beginTransaction();
        try{

            $document = documentOtherModel::join('subcategory','documentother.doc_subid','=','subcategory.sub_id')
                                          ->join('customer','customer.cust_id','=','documentother.doc_customerid')
                                          ->where('doc_id',$id)
                                          ->first();

            $preparedBy = documentOtherModel::select('*','documentother.created_at as prepareddate')
                                            ->leftjoin('users','documentother.doc_preparedby','users.id')
                                            ->where('documentother.doc_id',$id)
                                            ->first();

            $modifiedBy = documentOtherModel::select('*','documentother.updated_at as modifieddate')
                                            ->leftjoin('users','documentother.doc_modifiedby','users.id')
                                            ->where('documentother.doc_id',$id)
                                            ->first();

            $docAttach = DocumentAttachmentModel::where('docatt_documentid',$id)->get();
            $count = 1;
            $cust = customerModel::all();
            $subcat = SubCategoryModel::all();
            $package = PackageModel::all();

            DB::commit();

            return view('layouts.document.other.showdoc',[
                    'document'=>$document,
                    'docAttach'=>$docAttach,
                    'count'=>$count,
                    'cust'=>$cust,
                    'subcat'=>$subcat,
                    'package'=>$package,
                    'preparedBy'=>$preparedBy,
                    'modifiedBy'=>$modifiedBy,
            ]);

        }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
        }
    }

    public function editDoc($id)
    {

        DB::beginTransaction();
        try{

            $document = documentOtherModel::join('subcategory','documentother.doc_subid','=','subcategory.sub_id')
                                          ->join('customer','customer.cust_id','=','documentother.doc_customerid')
                                          ->where('doc_id',$id)
                                          ->first();

            $preparedBy = documentOtherModel::select('*','documentother.created_at as prepareddate')
                                            ->leftjoin('users','documentother.doc_preparedby','users.id')
                                            ->where('documentother.doc_id',$id)
                                            ->first();

            $modifiedBy = documentOtherModel::select('*','documentother.updated_at as modifieddate')
                                            ->leftjoin('users','documentother.doc_modifiedby','users.id')
                                            ->where('documentother.doc_id',$id)
                                            ->first();

            $docAttach = DocumentAttachmentModel::where('docatt_documentid',$id)->get();
            $count = 1;
            $cust = customerModel::all();
            $subcat = SubCategoryModel::all();
            $package = PackageModel::all();

            DB::commit();

            return view('layouts.document.other.editdoc',[
                    'document'=>$document,
                    'docAttach'=>$docAttach,
                    'count'=>$count,
                    'cust'=>$cust,
                    'subcat'=>$subcat,
                    'package'=>$package,
                    'preparedBy'=>$preparedBy,
                    'modifiedBy'=>$modifiedBy,
            ]);

        }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
        }

        
    }

    public function getFileReply($id)
    {
        DB::beginTransaction();
        try{

            $filename = DocumentAttachmentModel::find($id);

            DB::commit();

            return response()->download(public_path($filename->docatt_path), null, [], null);

        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
        }

    }

    public function removeDocAttachment($id)
    {
        DB::beginTransaction();
        try {
          // dd($id);

            $deleteDA = DocumentAttachmentModel::findOrFail($id);
            $docid = $deleteDA->docatt_documentid;
            $file = $deleteDA->docatt_path;
            $deleteDA->forcedelete();

            //check still have an attachment or not
            $checkattach = DocumentAttachmentModel::where('docatt_documentid',$docid)->get();
            //dd(count($checkattach));
            if(count($checkattach) == 0)
            {
                $updateNoti = documentOtherModel::find($docid);
                $updateNoti->doc_attachment = 0;
            }else {
                $updateNoti = documentOtherModel::find($docid);
                $updateNoti->doc_attachment = 1;
            }
            $updateNoti->save();

            $filename = public_path().$file;
            \File::delete($filename);
            
            
            DB::commit();
            return Redirect()->back()->with('success', 'Attachment has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

    public function updateDoc(Request $request)
    {
        DB::beginTransaction();
        try{

            $totalsize = $request->totalsize;

            if ($totalsize>390) {
                return Redirect()->back()->withInput()->with(['error'=>'Total file size is too large and exceeds 390 MB']);
            }

            $updateDoc = documentOtherModel::find($request->docid);
            $updateDoc->doc_number = $request->doc_number;
            $updateDoc->doc_name = $request->filename;
            $updateDoc->doc_description = $request->description;
            $updateDoc->doc_package = $request->package;

            if($request->doc_other != null){
                $docother = $request->doc_other;
            }
            else{
                $docother = $request->doc_other2;
            }

            if($request->cat_other != null){
                $catother = $request->cat_other;
            }
            else{
                $catother = $request->cat_other2;
            }

            if($request->cust_other != null){
                $custother = $request->cust_other;
            }
            else{
                $custother = $request->cust_other2;
            }

            $updateDoc->doc_otherpack = $docother;
            $updateDoc->doc_subid = $request->subcategory;
            $updateDoc->doc_subother = $catother;
            $updateDoc->doc_customerid = $request->customer;
            $updateDoc->doc_customerother = $custother;
            $updateDoc->doc_modifiedby = auth()->user()->id;
            $updateDoc->save();

            $haveattachment = $updateDoc->doc_attachment;

          if($request->counter != null) 
          { 
                $counterItem = count($request->counter);

              for($i=0; $i<$counterItem; $i++){

                if ($request->file('fileattachment')[$i] !=null) 
                {
                    $haveattachment = 1;

                //dd($_POST['fileattachment'][$i]);
                  $file = $request->file('fileattachment')[$i];
                  // $file = $_POST['fileattachment'][$i];
                  $destination_path = public_path().'/files/document/';
                  $extension = $file->getClientOriginalExtension();
                  $files = $file->getClientOriginalName();
                  $fileName = $files.'_'.time().'.'.$extension;
                  $file->move($destination_path,$fileName);

                  $addAttach = new DocumentAttachmentModel;
                  $addAttach->docatt_documentid = $request->docid;
                  $addAttach->docatt_file = $fileName;
                  $addAttach->docatt_path = '/files/document/'.$fileName;
                  $addAttach->save();
                }
            }
          }

          $updateDoc->doc_attachment = $haveattachment;
          $updateDoc->save();

            DB::commit();
            return Redirect()->back()->with('success', 'Document has been updated successfully!');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage()]);

        }
    }

    public function removeDoc($id)
    {
        DB::beginTransaction();
        try{

            $attachment = DocumentAttachmentModel::where('docatt_documentid',$id)
                                                 ->get();

            if(count($attachment)>0)
            {
                foreach($attachment as $att) {
                    
                    $file = $att->docatt_path;
                    $filename = public_path().$file;
                    \File::delete($filename);
                }
            }

              $deleteD = documentOtherModel::findOrFail($id);
              $deleteD->forcedelete();

              $deleteAttachment = DocumentAttachmentModel::where('docatt_documentid',$id)
                                                         ->forcedelete();

            DB::commit();
            return Redirect('document/others')->with('success','Document has been deleted');

        }catch(Exception $error){

              DB::rollback();
              return abort(404);
        }
        
    }

    public function requestRemoveDoc($id)
    {
        DB::beginTransaction();
        try{


              $getPerm = documentOtherModel::find($id);
              $getPerm->doc_permissiondel = 1;
              $getPerm->save();

            DB::commit();
            return Redirect('document/others')->with('success','Request has been submited');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
        
    }

    public function verifiedremovedoc($id)
    {
        DB::beginTransaction();
        try{

            $transmittal = documentOtherModel::find($id);
            $transmittal->doc_verifiedby = Auth::user()->id;
            $transmittal->doc_verifieddate = Carbon::now();
            $transmittal->save();

            DB::commit();
            return Redirect('document/others')->with('success','Document has been verified to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function cancelremovedoc($id)
    {
        DB::beginTransaction();
        try{

            $transmittal = documentOtherModel::find($id);
            $transmittal->doc_permissiondel = 0;
            $transmittal->doc_verifiedby = null;
            $transmittal->save();

            DB::commit();
            return Redirect('document/others')->with('success','Document has been decline to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

}
