<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Carbon\Carbon;
use App\Library\Globe;

use App\User;
use App\StateCodeModel;
use App\PublicHolidayModel;
use App\ConfigSystemModel;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $sysconfig = ConfigSystemModel::where('cs_id','=','1')->first();
        $configid = 1;
        $state = StateCodeModel::all();

        return view('layouts.master.index',[
            'sysconfig'=>$sysconfig,
            'configid'=>$configid,
            'state'=>$state,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            
            $update = ConfigSystemModel::find($id);
            $update->cs_name = $request->name;
            $update->cs_description = $request->description;
            $update->cs_statecode = $request->statecode;
            $update->save();


            DB::commit();
            return Redirect('master/config')->with('success','System Configuration has been updated!');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage()]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPublicHoliday()
    {
        DB::beginTransaction();
        try{

            $liststate = StateCodeModel::all();
            $count = 1;

            DB::commit();
            return view('layouts.master.public_holiday.index',[
                'liststate'=>$liststate,
                'count'=>$count,
            ]);

        }catch(Exception $error){

            DB::rollback();
            return abort(404);
        }

    }

    public function editPublicHoliday($id)
    {
        DB::beginTransaction();
        try{

            $state = StateCodeModel::where('st_code',$id)->first();

            $phcity = $state->st_name;
            $phcode = $state->st_code;
            $counter = 1;

            $publicholiday = PublicHolidayModel::where('ph_code',$id)
                                               ->orderBy('ph_year','DESC')
                                               ->orderBy('ph_date')
                                               ->get();

            DB::commit();
            return view('layouts.master.public_holiday.edit',[
                'publicholiday'=>$publicholiday,
                'phcity'=>$phcity,
                'phcode'=>$phcode,
                'counter'=>$counter,
            ]);

        }catch(Exception $error){
            DB::rollback();
            return abort(404);
        }

    }

    public function updatePublicHoliday(Request $request)
    {
        DB::beginTransaction();
        try{

            if($request->counter == null) {
                return Redirect()->back()->with('error', 'Please add (+) before submit!');
            }
            $counterItem = count($request->counter);

            for($i=0; $i<$counterItem; $i++){
                $addPH = new PublicHolidayModel;
                $addPH->ph_year = date('Y');
                $addPH->ph_city = $request->ph_city;
                $addPH->ph_code = $request->ph_code;
                $addPH->ph_date = $_POST['ph_date'][$i];
                $addPH->ph_description = $_POST['ph_description'][$i];
                $addPH->save();
            }
            DB::commit();
            return Redirect()->back()->with('success', 'Public Holiday has been added successfully!');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage()]);

        }
    }

    public function removePublicHoliday($phid)
    {
        DB::beginTransaction();
        try {

            $deletePH = PublicHolidayModel::findOrFail($phid);
            $deletePH->forcedelete();
            
            
            DB::commit();
            return Redirect()->back()->with('success', 'Record has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

    public function deleteAllPublicHoliday(Request $request)
    {

        DB::beginTransaction();
        try {

             $data = $request->all();

            foreach ($data['holiday'] as $i => $id) {

                $deletePB =PublicHolidayModel::find($id);
                $deletePB->forcedelete();
            }

            DB::commit();
            return Redirect()->back()->with('success', 'Record has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

}
