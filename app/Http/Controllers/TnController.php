<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Response;
use View;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use PDF;
use mPDF;
use App\TnrptModel;

class TnController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        $listTn = TnrptModel::get();
        $count = 1;
    	return view('layouts.report.tn.index',[
        		    'listTn'=>$listTn,
                    'count'=>$count,
                   ]);
    }

    public function createTn()
    {
    	return view('layouts.report.tn.create-tn',[

    			   ]);
    }

    public function storeTn(Request $request)
    {
        DB::beginTransaction();
        try{

            //dd(Carbon::parse($request->tndate));
            //dd( $request->tndate);
            // dd( date('m',strtotime($request->tndate)) ); //month
            // dd( date('y',strtotime($request->tndate)) ); //year


            $tnNo = Globe::sp_prefix('Transmittal_note', 'TN');
            $storeTn = new TnrptModel;
            $storeTn->tn_name = $request->tnname;
            $storeTn->tn_description = $request->tnreferenceno;
            $storeTn->tn_referenceno = 'TN/'.$request->tnreferenceno.'/'.date('y',strtotime($request->tndate)).date('m',strtotime($request->tndate)).'-TVSB';
            $storeTn->tn_date = Carbon::parse($request->tndate);
            $storeTn->tn_title = $request->tntitle;
            $storeTn->tn_body = $request->tnbody;
            $storeTn->save();

            DB::commit();
            return Redirect('report/tn')->with('success','Successfuly generate to PDF format');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage() ]);

        }

    }

    public function generatePDF($id)
    {

        $tndetail = TnrptModel::find($id);

        $data = ['title'=>'Transmittal Note',
                 'tnname'=>$tndetail->tn_name,
                 'tnreferenceno'=>$tndetail->tn_referenceno,
                 'tndate'=>$tndetail->tn_date,
                 'tntitle'=>$tndetail->tn_title,
                 'tnbody'=>$tndetail->tn_body
                ];

        $pdf = PDF::loadView('layouts.report.tn.viewtnrptpdf', $data);

        return $pdf->download('Transmittal_Note.pdf');
    }

    public function getPrintReport($id)
    {
        Globe::printiReport('report1',['id'=>$id]);
        return Redirect::action(Request::url());
    }

    public function getExportReport()
    {
        //dd('a');
        $type = Input::get('type');
        $tndetail = TnrptModel::find(Input::get('tnid'));
        $contenthtml = array('tndetail'=>$tndetail);
        $filename = 'Reporting';
        if($type=='XLS'){

        return Response::view(
                'layouts.report.tn.viewtnreport',$contenthtml)
        ->header('Content-type','application/excel')
        ->header('Content-Disposition','attachment; filename='.$filename.'.xls');
        
        }else if($type=='PDF'){
            $html = View::make('layouts.report.tn.viewtnreport', $contenthtml)->render();
            return response()->file($pathToFile);

            return response()->file($pathToFile, $headers);

            $mpdf = new mPDF(
                    '',    // mode - default ''
                    'A4-L',    // format - A4, for example, default ''
                    0,     // font size - default 0
                    'Arial',// default font family
                    10,    // margin_left
                    10,    // margin right
                    16,     // margin top
                    16,    // margin bottom
                    9,     // margin header
                    9,     // margin footer
                    'L'
                    );

            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetHeader('{PAGENO}');
            $mpdf->keep_table_proportions = true;
            //$mpdf->SetFooter('This is a computer generated document and no signature is required.||{PAGENO}');
            // $mpdf->shrink_tables_to_fit = 0;
            $mpdf->WriteHTML($html);
            $mpdf->Output($filename.'.pdf','I');
        }

    }






}
