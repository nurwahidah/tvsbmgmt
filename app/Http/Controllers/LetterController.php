<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Response;
use View;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\letterModel;
use App\letterAttachmentModel;
use App\customerModel;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        DB::beginTransaction();
        try{
            $listLetter = letterModel::get();
            $count = 1;

            DB::commit();
            return view('layouts.document.letter.index',[
                        'listLetter'=>$listLetter,
                        'count'=>$count,
                       ]);

        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createL()
    {
        DB::beginTransaction();
        try{
            $cust = customerModel::all();

        DB::commit();
            return view('layouts.document.letter.createletter',[
                        'cust'=>$cust,
            ]);
        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }
    }

    public function storeLetter(Request $request)
    {
        DB::beginTransaction();
        try{

            $rules = array(
                        'customer'=>'required',
                        'month'=>'required',
                        'year'=>'required',
                        'name'=>'required',
                        'description'=>'required',
                     );

            $customMessages = ['required'=> ':attribute field is required'];
            $validator = Validator::make(Input::all(), $rules, $customMessages);

    if($validator->fails()){

        return Redirect::back()->withInput()->withErrors($validator);
    }
    else
    {
            if($request->customer=="Others")
            {
                $reffNo = Globe::sp_prefixDoc('Letter','LTR');
                $number = 'TVSB/'.$request->cust_other.'/'.$reffNo.'/'.$request->month.'/'.$request->year;
            }
            else{

                $reffNo = Globe::sp_prefixDoc('Letter','LTR');
                $number = 'TVSB/'.$request->customer.'/'.$reffNo.'/'.$request->month.'/'.$request->year;
            }
            
            $storeletter = new letterModel;
            $storeletter->let_number = $number;
            $storeletter->let_name = $request->name;
            $storeletter->let_desc = $request->description;
            $storeletter->let_preparedby = auth::user()->id;
            $storeletter->let_prepareddate = Carbon::now();
            $storeletter->let_permissiondel = '0';

            if($request->file('fileattachment') == null) {
              $storeletter->let_attachment = 0;  
            }else{
                $storeletter->let_attachment = 1;
            }

            $storeletter->save();


        if($request->file('fileattachment')) 
        {
            
            for ($i=0; $i < count($request->file('fileattachment')) ; $i++) 
            { 
                $file = $request->file('fileattachment')[$i];
                $destination_path = public_path().'/files/letter/';
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $files.'_'.time().'.'.$extension;
                $file->move($destination_path,$fileName);

                $attch = new letterAttachmentModel;
                $attch->lratt_letterid = $storeletter->let_id;
                $attch->lratt_file = $fileName;
                $attch->lratt_path = '/files/letter/'.$fileName;
                $attch->save();
            }
        }
    
            

            DB::commit();
            return Redirect('document/letter')->with('success','Letter successfuly created');
    }

        }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }

    public function showLetter($id)
    {
        DB::beginTransaction();
        try{

            $letter = letterModel::find($id);
            $letterAttach = letterAttachmentModel::where('lratt_letterid',$id)->get();
            $count = 1;

            DB::commit();

            return view('layouts.document.letter.showletter',[
                    'letter'=>$letter,
                    'letterAttach'=>$letterAttach,
                    'count'=>$count,
                   ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
            }
    }

    public function editLetter($id){

        DB::beginTransaction();
        try{

            $letter = letterModel::find($id);
            $letterAttach = letterAttachmentModel::where('lratt_letterid',$id)->get();
            $count = 1;

            DB::commit();
            return view('layouts.document.letter.editletter',[
                'letter'=>$letter,
                'letterAttach'=>$letterAttach,
                'count'=>$count,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }

        
    }

    public function requestRemoveLetter($id)
    {
        DB::beginTransaction();
        try{

            DB::commit();

              $getPerm = letterModel::find($id);
              $getPerm->let_permissiondel = 1;
              $getPerm->save();

            DB::commit();
            return Redirect('document/letter')->with('success','Request has been submit');

        }//close try
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
        
    }

    public function verifiedremoveletter($id)
    {
        DB::beginTransaction();
        try{

            $letter = letterModel::find($id);
            $letter->let_verifiedby = Auth::user()->id;
            $letter->let_verifieddate = Carbon::now();
            $letter->save();

            DB::commit();
            return Redirect('document/letter')->with('success','Letter has been verified to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }
    public function cancelremoveletter($id)
    {
        DB::beginTransaction();
        try{

            $letter = letterModel::find($id);
            $letter->let_permissiondel = 0;
            $letter->let_verifiedby = null;
            $letter->save();

            DB::commit();
            return Redirect('document/letter')->with('success','Letter has been decline to delete');

        }
        catch(Exception $error){
            
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=> $error->getMessage() ]);
        }
    }
    

    public function removeLetter($id)
    {

        DB::beginTransaction();
        try{

            DB::commit();

            $attachment = letterAttachmentModel::where('lratt_letterid',$id)
                                               ->get();

            if(count($attachment)>0)
            {
                foreach ($attachment as $att) {
                    
                    $file = $att->lratt_path;
                    $filename = public_path().$file;
                    \File::delete($filename);
                }
                
            }

            $deleteL = letterModel::findOrFail($id);
            $deleteL->forcedelete();

            $deleteAttachment = letterAttachmentModel::where('lratt_letterid',$id)
                                                     ->forcedelete();

            DB::commit();
            return Redirect('document/letter')->with('success','Letter has been deleted');

          }catch(Exception $error){

              DB::rollback();
              return abort(404);
          }
        
    }

    public function getFileReply($id)
    {
        DB::beginTransaction();
        try{

            $filename = letterAttachmentModel::find($id);

            DB::commit();
            return response()->download(public_path($filename->lratt_path), null, [], null);
            
        }catch(Exception $error){
                            
                DB::rollback();
                return abort(404);
            }
    }

    public function updateLetter(Request $request)
    {
        DB::beginTransaction();
        try{

            $totalsize = $request->totalsize;

            if ($totalsize>390) {
                return Redirect()->back()->withInput()->with(['error'=>'Total file size is too large and exceeds 390 MB']);
            }

            $updateL = letterModel::find($request->letterid);
            $updateL->let_name = $request->filename;
            $updateL->let_desc = $request->description;
            $updateL->save();

            $haveattachment = $updateL->let_attachment;


        if($request->counter != null) 
        {
            $counterItem = count($request->counter);

             //dd($counterItem);
            for($i=0; $i<$counterItem; $i++){

              //dd($_POST['fileattachment'][$i]);
                if ($request->file('fileattachment')[$i] !=null) 
                {
                    $haveattachment = 1;

                    $file = $request->file('fileattachment')[$i];
                    // $file = $_POST['fileattachment'][$i];
                    $destination_path = public_path().'/files/letter/';
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName = $files.'_'.time().'.'.$extension;
                    $file->move($destination_path,$fileName);

                    $addAttach = new letterAttachmentModel;
                    $addAttach->lratt_letterid = $request->letterid;
                    $addAttach->lratt_file = $fileName;
                    $addAttach->lratt_path = '/files/letter/'.$fileName;
                    $addAttach->save();
                }
            }
        }
            $updateL->let_attachment = $haveattachment;
            $updateL->save();

            DB::commit();
            return Redirect()->back()->with('success', 'Letter has been updated successfully!');

        }catch(Exception $error){
            DB::rollback();
            return Redirect()->back()->withInput()->with(['error'=>$error->getMessage()]);

        }
    }

    public function removeLetterAttachment($id)
    {
        DB::beginTransaction();
        try {

            $deleteLA = letterAttachmentModel::findOrFail($id);
            $letterid = $deleteLA->lratt_letterid;
            $file = $deleteLA->lratt_path;
            $deleteLA->forcedelete();

            //check still have an attachment or not
            $checkattach = letterAttachmentModel::where('lratt_letterid',$letterid)->get();

            if(count($checkattach) == 0)
            {
                $updateNoti = letterModel::find($letterid);
                $updateNoti->let_attachment = 0;
            }else {
                $updateNoti = letterModel::find($letterid);
                $updateNoti->let_attachment = 1;
            }
            $updateNoti->save();
            
            $filename = public_path().$file;
            \File::delete($filename);
            
            DB::commit();
            return Redirect()->back()->with('success', 'Attachment has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }



}
