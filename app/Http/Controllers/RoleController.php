<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use DB;
use Config;
use App;
use Validator;
use Redirect;
use Exception;
use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Crypt;
//permission/roles
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'permission:admin.roles']);
        $this->middleware('auth');
    }
    public function index()
    {
        $roles = Role::get();
        $count = 1;

        return view('layouts.admin.role.index',[
            'roles'=>$roles,
            'count'=>$count,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addRole(Request $request)
    {
        // dd($request->rolename);
        DB::beginTransaction();
        try{

            $role = Role::create(['name' => $request->rolename]);

            DB::commit();
            return Redirect()->back()->with('success','Success Create New Role');

        }catch(Exception $error){

            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public static function getRole($id){
        
        $permission = Permission::get();

        $role = Role::findOrFail($id);

        $rolehaspermissions = DB::table('role_has_permissions')->where('role_id',$id)->pluck('permission_id')->toArray();


        $formgroup1 = '<div class=" item form-group">
                        <div class="x_content">
                            <input type="hidden" name="roleidmodal2" value="'.$id.'">
                            <input type="hidden" name="rolenamemodal2" value="'.strtoupper($role->name).'">
                            <label class="control-label">'.__('Role Name').'</label>
                            <input type="text" class="form-control" name="rolename" value="'.$role->name.'">
                        </div>
                      </div>';

        $html2 = $formgroup1;

        $data = ['html'=>$html2];
        
        return json_encode($data);

    }


    public function updateRole(Request $request)
    {
        DB::beginTransaction();
        try{

            $updRole = Role::find($request->roleidmodal2);
            $updRole->name = $request->rolename;
            $updRole->updated_at = now();
            $updRole->save();

            DB::commit();
            return Redirect()->back()->with('success','Role has been updated');

        }catch(Exception $error){

            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public function storepermission(Request $request)
    {

        DB::beginTransaction();

        try {

            $id = $request->input('roleidmodal2');
            $desc = $request->input('rolenamemodal2');
            $list = $request->input('permission');

            $role = Role::find($id);
            $role->syncPermissions($list);

            // DB::table('role_has_permissions')->where('role_id',$id)->delete();

            // if(!empty($list)){
                
            //     for($i=0;$i<count($list);$i++){

            //         DB::table('role_has_permissions')->insert(['permission_id'=>$list[$i], 'role_id'=>$id]); 

            //     }

            // }

            DB::commit();
            return Redirect('admin/role')->with('success', 'Permission for Role '.$desc.' has been updated!');
            
        } catch (Exception $error) {
            DB::rollback();
            return Redirect('admin/role')->withInput()->with( [ 'error' => $error->getMessage(), 'modal'=>true ]);
            
        }
    }
}
