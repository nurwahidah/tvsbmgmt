<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use Form;
use Validator;
use Config;
use Redirect;
use Exception;
use Carbon\Carbon;
use App\Library\Globe;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;

use App\User;
use App\OvertimeModel;
use PDF;

class OvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $count = 1;

        if(auth()->user()->hasRole('staff'))
        {
            if(Input::has('otdate')) 
            {

                $otlist = OvertimeModel::join('users','users.id','=','overtime.ot_preparedby')
                                       ->where('overtime.ot_status','=','A')
                                       ->where(DB::raw('date(overtime.ot_date)'),'=',Input::get('otdate'))
                                       ->orderBy('overtime.created_at','desc')
                                       ->get();
            }
            else
            {

                $otlist = OvertimeModel::join('users','users.id','=','overtime.ot_preparedby')
                                       ->where('overtime.ot_status','=','A')
                                       ->orwhere('overtime.ot_preparedby',auth()->user()->id)
                                       ->orderBy('overtime.created_at','desc')
                                       ->get();
            }
        }
        else
        {
            if(Input::has('otdate')) 
            {

                $otlist = OvertimeModel::join('users','users.id','=','overtime.ot_preparedby')
                                       ->where(DB::raw('date(overtime.ot_date)'),'=',Input::get('otdate'))
                                       ->orderBy('overtime.created_at','desc')
                                       ->get();
            }
            else
            {

                $otlist = OvertimeModel::join('users','users.id','=','overtime.ot_preparedby')
                                       ->orderBy('overtime.created_at','desc')
                                       ->get();
            }
        }


        return view('layouts.overtime.index',[
                    'otlist'=>$otlist,
                    'count'=>$count,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createOT()
    {
        $ot = new OvertimeModel;
        return view('layouts.overtime.createovertime',[
                    'ot'=>$ot,
        ]);

    }

    public function storeOT(Request $request)
    {
        DB::beginTransaction();
        try{
            $rules = array(
                        'otdate'=>'required',
                        'otstarttime'=>'required',
                        'otendtime'=>'required',
                        'description'=>'required',
                     );
            $customMessages = ['required'=> ':attribute field is required'];

            $validator = Validator::make(Input::all(), $rules, $customMessages);

            if($validator->fails())
            {
                return Redirect::back()->withInput()->withErrors($validator);
            }
            else
            {
                $starttime = Carbon::parse($request->otstarttime);
                $endtime = Carbon::parse($request->otendtime);

                $startdate = $starttime->format('h:i');
                $enddate = $endtime->format('h:i');

                $startdate2 = Carbon::parse($startdate);
                $enddate2 = Carbon::parse($enddate);

                if($starttime->format('A') =="AM" && $endtime->format('A')=="AM")
                {

                    if ($startdate == $enddate) {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }
                    elseif($startdate > $enddate)
                    {
                        if ($startdate>=12) {
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        
                    }
                    elseif($enddate > $startdate)
                    {
                        if ($enddate>=12) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                        
                    }
                    else{
                        $hours = ($starttime->diffInMinutes($endtime))/60;
                    }
                    
                }
                elseif ($starttime->format('A') =="PM" && $endtime->format('A')=="PM") 
                {
                    if ($startdate == $enddate) {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }
                    elseif($startdate > $enddate)
                    {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }

                    elseif ($enddate > $startdate) {
                        if ($enddate>=12) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                    }
                    else{
                        
                        $hours = ($starttime->diffInMinutes($endtime))/60;
                    }

                }

                else 
                {
                    if ($starttime->format('A') == "AM" && $endtime->format('A')=="PM") {
                        $hours = ($starttime->diffInMinutes($endtime))/60; 
                    }
                    else
                    {
                        if ($startdate == $enddate) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        elseif($startdate > $enddate)
                        {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }

                        elseif ($enddate > $startdate) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            if ($enddate>=12) {
                                $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                            }
                            else{
                                $hours = ($starttime->diffInMinutes($endtime))/60;
                            }
                        }

                    }
                    
                    
                }

        

                $addOT = new OvertimeModel;
                $addOT->ot_date = $request->otdate;
                $addOT->ot_description = $request->description;
                $addOT->ot_starttime = $starttime;
                $addOT->ot_endtime = $endtime;
                $addOT->ot_preparedby = Auth::user()->id;
                $addOT->ot_prepareddate = Carbon::now();


                //diffInMinutes  diffInHours
                // $hours = ($starttime->diffInMinutes($endtime))/60;
                // dd($hours);
                $addOT->ot_hour = $hours;
                $addOT->ot_balanceusedstatus = '0';

                if($hours >= 8) // full day
                {
                    $addOT->ot_type = 'Full day';
                }
                elseif($hours >= 4 && $hours < 8) //half day
                {
                    $addOT->ot_type = 'Half day';
                }
                elseif($hours < 4)
                {
                    $addOT->ot_type = 'Time off';
                }
                    
                $addOT->save();


                
            }

            $data = array(
                    'subject'=> 'Overtime Request Notification',
                    'emailFrom'=> Auth::user()->email,
                    );

            Mail::to('wahidah@tiaravision.com')->send(new SendEmail($data));

            DB::commit();
            return Redirect('overtime')->with('success', 'Overtime successfully created!');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

    public function updateOT(Request $request)
    {
        DB::beginTransaction();
        try{
            $rules = array(
                        'otdate'=>'required',
                        'otstarttime'=>'required',
                        'otendtime'=>'required',
                        'description'=>'required',
                     );
            $customMessages = ['required'=> ':attribute field is required'];

            $validator = Validator::make(Input::all(), $rules, $customMessages);

            if($validator->fails())
            {
                return Redirect::back()->withInput()->withErrors($validator);
            }
            else
            {

                $starttime = Carbon::parse($request->otstarttime);
                $endtime = Carbon::parse($request->otendtime);

                $startdate = $starttime->format('h:i');
                $enddate = $endtime->format('h:i');

                $startdate2 = Carbon::parse($startdate);
                $enddate2 = Carbon::parse($enddate);

                if($starttime->format('A') =="AM" && $endtime->format('A')=="AM")
                {

                    if ($startdate == $enddate) {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }
                    elseif($startdate > $enddate)
                    {
                        if ($startdate>=12) {
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        
                    }
                    elseif($enddate > $startdate)
                    {
                        if ($enddate>=12) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                        
                    }
                    else{
                        $hours = ($starttime->diffInMinutes($endtime))/60;
                    }
                    
                }
                elseif ($starttime->format('A') =="PM" && $endtime->format('A')=="PM") 
                {
                    if ($startdate == $enddate) {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }
                    elseif($startdate > $enddate)
                    {
                        $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                    }

                    elseif ($enddate > $startdate) {
                        if ($enddate>=12) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            $hours = ($starttime->diffInMinutes($endtime))/60;
                        }
                    }
                    else{
                        
                        $hours = ($starttime->diffInMinutes($endtime))/60;
                    }

                }

                else 
                {
                    if ($starttime->format('A') == "AM" && $endtime->format('A')=="PM") {
                        $hours = ($starttime->diffInMinutes($endtime))/60; 
                    }
                    else
                    {
                        if ($startdate == $enddate) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        elseif($startdate > $enddate)
                        {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }

                        elseif ($enddate > $startdate) {
                            $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                        }
                        else{
                            if ($enddate>=12) {
                                $hours = 24 - ($starttime->diffInMinutes($endtime))/60;
                            }
                            else{
                                $hours = ($starttime->diffInMinutes($endtime))/60;
                            }
                        }

                    }
                    
                    
                }

                $updOT = OvertimeModel::find($request->otid);
                $updOT->ot_date = $request->otdate;
                $updOT->ot_description = $request->description;
                $updOT->ot_starttime = $starttime;
                $updOT->ot_endtime = $endtime;
                $updOT->ot_hour = $hours;

                if($hours >= 8) // full day
                {
                    $updOT->ot_type = 'Full day';
                }
                elseif($hours >= 4 && $hours <= 8) //half day
                {
                    $updOT->ot_type = 'Half day';
                }
                elseif($hours < 4)
                {
                    $updOT->ot_type = 'Time off';
                }

                $updOT->save();
                    

            }


            DB::commit();
            return Redirect('overtime')->with('success', 'Overtime successfully updated!');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

    public function showOT($id)
    {
        DB::beginTransaction();
        try{

            $ot = OvertimeModel::findOrFail($id);
            if(Auth::user()->id == $ot->ot_preparedby && $ot->ot_readstatus == '0' && $ot->ot_status !== 'N')
            {
                $ot->ot_readstatus = '1';
                $ot->save();
            }



            $overtime = OvertimeModel::join('users','users.id','=','overtime.ot_preparedby')
                                     ->where('ot_id',$id)->first();


            $approved = OvertimeModel::join('users','users.id','=','overtime.ot_approvedby')
                                     ->where('ot_id',$id)
                                     ->first();

            DB::commit();

            return view('layouts.overtime.showOT',[
                'overtime'=>$overtime,
                'approved'=>$approved,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
            }

    }

    public function editOT($id)
    {
        DB::beginTransaction();
        try{

            $ot = OvertimeModel::findOrFail($id);

            DB::commit();

            return view('layouts.overtime.createovertime',[
                'ot'=>$ot,
            ]);

            }catch(Exception $error){
                
                DB::rollback();
                return abort(404);
          }
    }

    public function removeOT($otid)
    {
        DB::beginTransaction();
        try {

            $deleteOT = OvertimeModel::findOrFail($otid);
            $deleteOT->forcedelete();
            
            
            DB::commit();
            return Redirect()->back()->with('success', 'Overtime has been deleted');

        } catch (Exception $error) {
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }

    }

    public function disapprovedOT($otid)
    {
        DB::beginTransaction();
        try{

                $updateOT = OvertimeModel::findOrFail($otid);
                $updateOT->ot_status = 'D';
                $updateOT->ot_approvedby = auth::user()->id;
                $updateOT->ot_approveddate = Carbon::now();
                $updateOT->save();

            DB::commit();
            return Redirect()->back()->with('success', 'Overtime has been disapproved!');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }

    public function approveOT($otid)
    {
        DB::beginTransaction();
        try{

                $updateOT = OvertimeModel::findOrFail($otid);
                $updateOT->ot_status = 'A';
                $preparedby = $updateOT->ot_preparedby;
                $type = $updateOT->ot_type;
                $hours = $updateOT->ot_hour;
                $updateOT->ot_approvedby = auth::user()->id;
                $updateOT->ot_approveddate = Carbon::now();
                // $updateOT->save();

                //***********************************************
                //find latest OT for the user
                $othour = OvertimeModel::where('ot_preparedby',$preparedby)
                                        ->where('ot_balanceusedstatus','=','0')
                                        ->where('ot_status','=','A')
                                        ->first();
                                       

                if($othour !== null)
                {
                    $totalhour = $othour->ot_balancehour + $hours;
                    $viewOT = OvertimeModel::where('ot_preparedby',$preparedby)
                                           ->where('ot_status','=','A')
                                           ->where('ot_balanceusedstatus','=','0')->first();

                    $updOT = OvertimeModel::find($viewOT->ot_id);
                    $updOT->ot_balanceusedstatus = '1';
                    $updOT->save();

                }
                else
                {
                    $totalhour = $hours;
                }

                $updateOT->ot_totalhour = $totalhour;

                $countfull = 0;
                $counthalf = 0;
                $totaltimeoff = 0;

                while($totalhour >= 8) {
                    $balance = $totalhour - 8;
                    $totalhour = $balance;
                    $countfull++;
                }
                while($totalhour >= 4) {
                    $balance = $totalhour - 4;
                    $totalhour = $balance;
                    $counthalf++;
                }
                if($totalhour < 4 && $totalhour > 0) 
                {
                    $totaltimeoff = $totalhour;
                }
                $countday = $countfull + ($counthalf*1/2);
                $balancehr = $totaltimeoff;

                if($balancehr==0) {
                    $updateOT->ot_balanceusedstatus = 1;
                    $updateOT->ot_balancehour = 0;
                }
                else{
                    $updateOT->ot_balanceusedstatus = 0;
                    $updateOT->ot_balancehour = $balancehr;
                }

                $updateOT->ot_countday = $countday;
                $updateOT->save();

                //***********************************************


                $updateuser = User::findOrFail($preparedby);
                $currreplacementleave = $updateuser->rl;
                $currreplacementused = $updateuser->rl_used;
                $newrl = $countday;
                $updateuser->rl = $currreplacementleave + $newrl;
                $updateuser->rl_balance = ($currreplacementleave + $newrl) - $currreplacementused;

                // if ($type == "Half day") 
                // {
                //     $updateuser->rl = $currreplacementleave + 0.5;
                //     $updateuser->rl_balance = ($currreplacementleave + 0.5) - $currreplacementused;
                // }
                // elseif($type == "Full day")
                // {
                //     $updateuser->rl = $currreplacementleave + 1;
                //     $updateuser->rl_balance = ($currreplacementleave + 1) -  $currreplacementused;
                // }

                $updateuser->save();
                

            DB::commit();
            return Redirect()->back()->with('success', 'Overtime has been approved!');

        }
        catch(Exception $error){
            DB::rollback();
            return Redirect::back()->withInput()->with('error', $error->getMessage());
        }
    }
}
