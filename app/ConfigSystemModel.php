<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigSystemModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'configsystem';
    protected $primaryKey = 'cs_id';
    protected $fillable = array();
}
