<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class documentOtherModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'documentother';
    protected $primaryKey = 'doc_id';
    protected $fillable = array();
}
