<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StateCodeModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'state_code';
    protected $primaryKey = 'st_id';
    protected $fillable = array();
}
