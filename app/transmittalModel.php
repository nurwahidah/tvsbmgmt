<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class transmittalModel extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'transmittal';
    protected $primaryKey = 'tn_id';
    protected $fillable = array();
}
