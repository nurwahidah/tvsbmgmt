@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Leave</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->leave }}</strong></h2>
              </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Used</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->leave_used }}</strong></h2>
                </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Balanced</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->leave_balance }}</strong></h2>
                </center>
            </div>
        </div>
        <legend></legend>
        <div class="row ">
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Carry Forward</span>
                </h4>
                <br>
                <h2><strong>{{ number_format((Auth::user()->cf_leave + Auth::user()->cf_leaveused),2) }}</strong></h2>
                
              </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Used</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->cf_leaveused }}</strong></h2>
                </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Balanced</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->cf_leave }}{{-- Auth::user()->cf_monthlimit --}}</strong></h2>
                </center>
            </div>
        </div>
        <legend></legend>
        <div class="row ">
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Medical Certificate</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->mc }}</strong></h2>
              </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Used</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->mc_used }}</strong></h2>
                </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Balanced</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->mc_balance }}</strong></h2>
                </center>
            </div>
        </div>
        <legend></legend>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Replacement Leave</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->rl }}</strong></h2>
              </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Used</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->rl_used }}</strong></h2>
                </center>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <center>
                <h4>
                  <span class="text-uppercase">Balanced</span>
                </h4>
                <br>
                <h2><strong>{{ Auth::user()->rl_balance }}</strong></h2>
                </center>
            </div>
        </div>
        <br>
        
      </div>
    </div>
  </div>
</div>

@hasanyrole('superadmin|admin')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          
          <div class="col-lg-12">
            <h5><strong> Leave Request </strong></h5>
              <div class="card card-body">
                @if($countrequest->leaverequest == 0)
                No new request !
                @else
                    @foreach($newleave as $nl)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('leave') }}" style="color:white;">A new request from {{ $nl->name }}, requested on {{ $nl->lve_prepareddate }} </a></strong>
                          </div>
                    @endforeach
                  @endif
              </div>
          </div>
          
          <br>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          
          <div class="col-lg-12">
            <h5><strong> Overtime Request </strong></h5>
              <div class="card card-body">
                @if($countOTrequest->otrequest == 0)
                No new request !
                @else
                    @foreach($newOT as $nl)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('overtime') }}" style="color:white;">A new request from {{ $nl->name }}, requested on {{ $nl->ot_prepareddate }} </a></strong>
                          </div>
                    @endforeach
                  @endif
              </div>
          </div>
          
          <br>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          <div class="col-lg-12">
            <h5><strong>Delete Letter Request</strong></h5>
              <div class="card card-body">
                @if($countLetternoti->letterrequest == 0)
                No new request !
                @else
                    @foreach($delLetternoti as $ln)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('document/letter') }}" style="color:white;">A new request from {{ $ln->name }}, created on {{ $ln->let_prepareddate }} </a></strong>
                          </div>
                    @endforeach
                  @endif
              </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          <div class="col-lg-12">
            <h5><strong>Delete Transmittal Request</strong></h5>
              <div class="card card-body">
                @if($countTransmittalnoti->transmittalrequest == 0)
                No new request !
                @else
                    @foreach($delTransmittalnoti as $tn)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('document/transmittal') }}" style="color:white;">A new request from {{ $tn->name }}, created on {{ $tn->tn_prepareddate }}</a></strong>
                          </div>
                    @endforeach
                  @endif
              </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          <div class="col-lg-12">
            <h5><strong>Delete Document Other Request</strong></h5>
              <div class="card card-body">
                @if($countDocnoti->documentrequest == 0)
                  No new request !
                @else
                    @foreach($delDocnoti as $dn)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('document/others') }}" style="color:white;">A new request from {{ $dn->name }}, created on {{ $dn->doc_prepareddate }}</a></strong>
                          </div>
                    @endforeach
                  @endif
              </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>
@endrole

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
          <div class="col-lg-12">
            <h5><strong> Unread Message </strong></h5>
              <div class="card card-body">
                @if($countleavenoti->leavenoti == 0 && $countOTnoti->OTnoti == 0)
                No new notification !
                @else
                    @foreach($newleavenoti as $nln)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('leave') }}" style="color:white;">A new leave notification on {{ $nln->lve_approveddate }}</a></strong>
                          </div>
                    @endforeach

                    @foreach($newOTnoti as $nln)
                          <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong><a href="{{ url('overtime') }}" style="color:white;">A new overtime notification on {{ $nln->ot_approveddate }}</a></strong>
                          </div>
                    @endforeach

                @endif
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
