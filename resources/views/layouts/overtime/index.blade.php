@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <span class="section">
          <a href="{{ route('overtime.createOT') }}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Overtime</a>
          <!-- <a href="{{ route('leave.htmltopdfview',['download'=>'pdf']) }}">Download PDF</a> -->
        </span>
          {{ Form::open(['action'=>'OvertimeController@index', 'method'=>'get']) }}
          <!-- <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
              <select class="form-control" name="ticketstatus" id="ticketstatus" onchange="this.form.submit()">
                      <option value="">-- Filter Status --</option>
                      <option value="all">All</option>
                      <option value="1" >Open</option>
                      <option value="0">Close</option>
              </select>
            </div>
          </div> -->
        <div class="row">
          <div class="col-md-1 col-sm-1 col-xs-12 pull-right">
              
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
              <button type="submit" class="btn btn-primary">Filter</button>
              <a href="{{ url('/overtime')}}" class="btn btn-success">Cancel</a>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
            <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerstartLeave'>
                <input type='text' class="form-control" name="otdate" value="" placeholder="Overtime Date" value="{{ request()->input('otdate', old('otdate')) }}" />
                <span class="input-group-addon">
                   <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
          </div>
        </div>
        {{ Form::close() }}



          {{ csrf_field() }}
            <!-- table-->
            <div class="table-responsive">
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Created Date</th>
                          <th>Overtime Date</th>
                          <th>Name</th>
                          <th>Designation</th>
                          <th>Start time</th>
                          <th>End time</th>
                          <th>Total Hours</th>
                          <th>Category</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                      @if(!$otlist->isEmpty())
                        @foreach($otlist as $otlt)

                        @if(Auth::user()->id == $otlt->ot_preparedby && $otlt->ot_readstatus == '0' && $otlt->ot_status !== 'N')
                        <tr style="background-color:#ece3fc;">
                        @else
                        <tr>
                        @endif
                          <td>{{ $count++ }}</td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{{ $otlt->ot_prepareddate }}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! date( 'Y-m-d', strtotime($otlt->ot_date)) !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! $otlt->name !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! $otlt->designation !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! date('h:iA',strtotime($otlt->ot_starttime)) !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! date('h:iA',strtotime($otlt->ot_endtime)) !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! $otlt->ot_hour !!}</a></td>
                          <td><a href="{{ route('overtime.showOT', $otlt->ot_id) }}">{!! $otlt->ot_type !!}</a></td>
                          <td>{!! Globe::statusLabel($otlt->ot_status) !!}</td>
                          <td>
                            <a data-toggle="tooltip" title="{{ __('Overtime Detail') }}" class="btn btn-icon btn-info btn-sm" href="{{ route('overtime.showOT', $otlt->ot_id) }}"><i class="fa fa-info-circle"></i></a>

                            @if($otlt->ot_status == 'N')
                              @if(Auth::user()->hasPermissionTo('ot.edit') && Auth::user()->id == $otlt->id)
                                <a data-toggle="tooltip" title="{{ __('Edit Overtime') }}" class="btn btn-icon btn-warning btn-sm" href="{{ route('overtime.editOT', $otlt->ot_id) }}"><i class="fa fa-pencil-square-o"></i></a>
                              @endif
                              @if(Auth::user()->hasPermissionTo('ot.approve') && Auth::user()->id != $otlt->id || Auth::user()->hasPermissionTo('ot.approve') && Auth()->user()->hasRole('superadmin'))
                                <a data-toggle="tooltip" title="{{ __('Approve') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('overtime.approved', $otlt->ot_id) }}" onclick="return confirm('Approve {{ $otlt->name }} request ?')"><i class="fa fa-check"></i></a>
                              @endif
                              @if(Auth::user()->hasPermissionTo('ot.disapproved') && Auth::user()->id != $otlt->id || Auth::user()->hasPermissionTo('leave.disapproved') && Auth()->user()->hasRole('superadmin'))
                                <a data-toggle="tooltip" title="{{ __('Disapproved') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('overtime.disapproved', $otlt->ot_id) }}" onclick="return confirm('Disapproved {{ $otlt->name }} request ?')"><i class="fa fa-minus-circle"></i>
                                </a>
                              @endif
                            @endif

                            @if($otlt->ot_status == 'D')
                              @if(Auth::user()->hasPermissionTo('ot.delete') && Auth::user()->id != $otlt->id || Auth::user()->hasPermissionTo('ot.delete') && Auth()->user()->hasRole('superadmin')) <!-- for role has permission -->
                                  <a data-toggle="tooltip" title="{{ __('Delete Overtime') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('overtime.remove', $otlt->ot_id) }}" onclick="return confirm('Delete Overtime?')"><i class="fa fa-trash-o"></i></a>
                              @endif
                            @endif

                          </td>
                        </tr>
                        @endforeach
                      @else
                        <tr><td colspan="11">No record found</td></tr>
                      @endif
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

@endsection


