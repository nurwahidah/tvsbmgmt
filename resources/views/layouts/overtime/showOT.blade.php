@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('overtime') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}
          {{ Form::open(['action'=>'OvertimeController@storeOT', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) }}
            <span class="section">Overtime Form</span>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">

            <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Name <span class="required" style="color:red">*</span>
                </label>

                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12'>
                      <input type='text' class="form-control" name="otname" value="{{ $overtime->name }}" readonly="readonly" />
                  </div>
                </div>
            </div> -->

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Date <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerOT'>
                      <input type='text' class="form-control" name="otdate" value="{{ $overtime->ot_date }}" readonly="readonly" />
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Start Time <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id=''>
                      <input type='text' class="form-control" name="otstarttime" value="{{ date('h:i A', strtotime($overtime->ot_starttime)) }}" readonly="readonly" />
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">End Time <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id=''>
                      <input type='text' class="form-control" name="otendtime" value="{{ date('h:i A', strtotime($overtime->ot_endtime) ) }}" readonly="readonly" />
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Designation <span class="required" style="color:red">*</span>
                </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12'>
                      <input type='text' class="form-control" name="otname" value="{{ $overtime->designation }}" readonly="readonly" />
                  </div>
                </div>
            </div> -->
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:150px;" readonly="readonly">{{ $overtime->ot_description }}</textarea>
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Prepared By
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="text" class="form-control" name="preparedby" value="{{ $overtime->name }}" readonly="readonly">
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">
            @if($approved != null)
                @if($approved->ot_status == 'D')
                  Disapproved By
                @else
                  Approved By
                @endif
            @else
              Approved by / Disapproved by
            @endif
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($approved != null)
                <input type="text" class="form-control" name="approvedby" value="{{ $approved->name }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="approvedby" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Prepared Date
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="text" class="form-control" name="preparedby" value="{{ $overtime->ot_prepareddate }}" readonly="readonly">
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">
            @if($approved != null)  
              @if($approved->ot_status == 'D')
                Disapproved Date
              @else
                Approved Date
              @endif
            @else
              Approved date / Disapproved date
            @endif
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($approved != null)
                <input type="text" class="form-control" name="approvedby" value="{{ $approved->ot_approveddate }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="approvedby" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
        </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection