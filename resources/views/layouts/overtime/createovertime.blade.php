@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('overtime') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}

          @if($ot->ot_id != '')
            {{ Form::open(['action'=>'OvertimeController@updateOT', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'updatedataform','files'=>true]) }}
          @else
            {{ Form::open(['action'=>'OvertimeController@storeOT', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) }}
          @endif

            <span class="section">Overtime Form</span>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Date <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  @if($ot->ot_id != '')
                    <input type="hidden" name="otid" value="{{ $ot->ot_id }}">
                  @endif
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerOT'>
                    @if($ot->ot_id != '')
                      <input type='text' class="form-control" name="otdate" value="{{ $ot->ot_date }}" />
                    @else
                      <input type='text' class="form-control" name="otdate" value="{{ request()->input('otdate', old('otdate')) }}" />
                    @endif
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Start Time <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerStartTime'>
                    @if($ot->ot_id != '')
                      <input type='text' class="form-control" name="otstarttime" value="{{ date('h:i A', strtotime($ot->ot_starttime)) }}" />
                    @else
                      <input type='text' class="form-control" name="otstarttime" value="{{ request()->input('otstarttime', old('otstarttime')) }}" />
                    @endif
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">End Time <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerEndTime'>
                    @if($ot->ot_id != '')
                      <input type='text' class="form-control" name="otendtime" value="{{ date('h:i A', strtotime($ot->ot_endtime)) }}" />
                    @else
                      <input type='text' class="form-control" name="otendtime" value="{{ request()->input('otendtime', old('otendtime')) }}" />
                    @endif
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                @if($ot->ot_id != '')
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:150px;">{{ $ot->ot_description }}</textarea>
                @else
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:150px;">{{ Input::old('description') }}</textarea>
                @endif
              </div>
            </div>
          </div>

        </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection