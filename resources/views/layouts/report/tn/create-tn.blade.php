@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('leave') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}

          {!! Form::open(['action'=>'TnController@storeTn', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          <span class="section">Transmittal Note</span>
        <div class="row">
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Date <span class="required" style="color:red">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerstartLeave'>
                      <input type='text' class="form-control" name="tndate" value="" />
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Header <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="text" name="tnname" class="form-control col-md-12 col-xs-12" placeholder="Ex: MEDIA CITY DEVELOPMENT SDN BHD">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Reference<span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="text" name="tnreferenceno" class="form-control col-md-12 col-xs-12" placeholder="Ex: MCDSB">
                </div>
            </div>

            

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Title <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="text" name="tntitle" class="form-control col-md-12 col-xs-12" placeholder="Ex: TITTLE : Media City Tender Document for E2 (Radio) Packages">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Description <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea name="tnbody" class="form-control col-md-7 col-xs-12" style="height:250px;"></textarea>
                </div>

            </div>
        </div> <!-- //close row -->

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Generate</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection