@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <span class="section">
          <a href="{{ route('report.tn.createTn') }}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Create TN</a>
        </span>
          {{ csrf_field() }}
            <!-- table-->
            <div class="table-responsive">
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Reference No</th>
                          <th>Header</th>
                          <th>Date</th>
                          <th>Title</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($listTn as $ltn)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $ltn->tn_referenceno }}</td>
                          <td>{{ $ltn->tn_name }}</td>
                          <td>{!! date( 'Y-m-d', strtotime($ltn->tn_date)) !!}</td>
                          <td>{{ $ltn->tn_title}}</td>
                          <td>
                            <a href="#" onclick="exportAlirTunai('xls')">Export Kepada XLS</a>
                            <a href="#" onclick="exportAlirTunai('pdf')">Export Kepada PDF</a>
                            <a data-toggle="tooltip" title="{{ __('Prints') }}" href="{{ route('report.tn.convertpdftn',$ltn->tn_id) }}" class="btn btn-icon btn-primary btn-sm"><i class="fa fa-file-pdf-o"></i></a>

                            <a data-toggle="tooltip" title="{{ __('Prints Report') }}" href="{{ route('report.tn.printreport',$ltn->tn_id) }}" class="btn btn-icon btn-danger btn-sm"><i class="fa fa-file-pdf-o"></i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

<script type="text/javascript">

function exportAlirTunai(type){
  if(type=='xls'){
    var url ='{{URL::action("TnController@getExportReport") }}?type=XLS&tnid={{ $ltn->tn_id }}';
  }else if(type=='pdf'){
    var url ='{{URL::action("TnController@getExportReport") }}?type=PDF&tnid={{ $ltn->tn_id }}';
  }
  window.open(url);
}

</script>



@endsection


