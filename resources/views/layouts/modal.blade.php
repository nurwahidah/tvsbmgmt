<!-- modal change password -->
<div class="modal fade" id="modal-password">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Change Password</h4>
      </div>
      <br>
      {!! Form::open(['action'=>'UserController@changePassword', 'method'=>'post', 'class'=>'form-horizontal form-label-left']) !!}
      <div class="card-body">
          <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Current Password <span class="required" style="color:red">*</span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input class="form-control" type="password" value="" name="currpass" id="currpass" placeholder="Current Password" required="required" />
            </div>          
          </div>
          <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">New Password <span class="required" style="color:red">*</span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input class="form-control" type="password" value="" name="newpass" placeholder="New Password" required="required"/>
            </div>          
          </div>
          <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Confirm New Password <span class="required" style="color:red">*</span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input class="form-control" type="password" value="" name="confirmpass" placeholder="Confirm Password" required="required"/>
            </div>          
          </div>

    </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Save</button>
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<!-- /modal change password -->



<!-- modal profile -->
<div class="modal fade" id="modal-profile">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">{{ Auth()->user()->name }} Profile</h4>
      </div>
      <br>
      {!! Form::open(['action'=>'UserController@updateProfile', 'method'=>'post', 'class'=>'form-horizontal form-label-left']) !!}
      <div class="card-body">
        <div class="item form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit<small>Profile</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th><input class="form-control" type="text" name="name" value="{{ auth::user()->name }}" required="required"/></th>
                        </tr>
                        <tr>
                          <th>E-mail</th>
                          <th><input class="form-control" type="text" name="email" value="{{ auth::user()->email }}" required="required"/></th>
                        </tr>
                        <tr>
                          <th>Mobile No</th>
                          <th><input class="form-control" type="text" name="phone" value="{{ auth::user()->phone }}"/></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
        </div>

    </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<!-- /modal profile -->


<!-- modal add role -->
<div class="modal fade" id="modal-add-role">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Add Role</h4>
      </div>
      <br>
      {!! Form::open(['action'=>'RoleController@addRole', 'method'=>'post', 'class'=>'form-horizontal form-label-left']) !!}
      <div class="card-body">
        <div class="item form-group">
          <div class="x_panel">
              <div class="x_title">
                <h2>Create New Role</h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <!-- start form for validation -->
                  <label for="fullname">Role Name</label>
                  <input type="text" id="rolename" class="form-control" name="rolename" required />
                      <br/>
                <!-- end form for validations -->
              </div>
            </div>
        </div>

    </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i> Add</button>
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<!-- /modal add role -->


<!-- modal add permission -->
<div class="modal fade" id="modal-add-permission">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Add Role</h4>
      </div>
      <br>
      {!! Form::open(['action'=>'PermissionController@addPerm', 'method'=>'post', 'class'=>'form-horizontal form-label-left']) !!}
      <div class="card-body">
        <div class="item form-group">
          <div class="x_panel">
              <div class="x_title">
                <h2>Create New Permission</h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <!-- start form for validation -->
                  <label for="fullname">Permission Name</label>
                  <input type="text" id="permname" class="form-control" name="permname" required />
                      <br/>
                <!-- end form for validations -->
              </div>
              <div class="x_content">
                <!-- start form for validation -->
                  <label for="fullname">Description</label>
                  <input type="text" id="permdesc" class="form-control" name="permdesc" required />
                      <br/>
                <!-- end form for validations -->
              </div>
            </div>
        </div>

    </div>
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i> Add</button>
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<!-- /modal add permission -->