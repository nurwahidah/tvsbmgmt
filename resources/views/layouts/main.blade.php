<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @php( $config = \App\ConfigSystemModel::find('1') )
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('assets/img/logos/logo_tvsb.png') }}" type="image/ico"/>

    <title>{{ $config->cs_name }}</title>

    <!-- <link href="{{ asset('assets2/css/core.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets2/css/app.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets2/css/style.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i|Dosis:300,500" rel="stylesheet"> -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" /> -->
    <!-- Bootstrap -->
    <link href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('assets/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset('assets/vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('assets/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('assets/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('assets/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset('assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset('assets/vendors/starrr/dist/starrr.css') }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('assets/build/css/custom.min.css') }}" rel="stylesheet">

  </head>

  <body class="nav-md" >
    <div class="container body" style="background-color:#0b2215;">
      <div class="main_container" style="background-color:#0b2215;">
        <div class="col-md-3 left_col" style="background-color:#0b2215;">
          <div class="left_col scroll-view" style="background-color:#0b2215;">
            <div class="navbar nav_title" style="border: 0;background-color:#0b2215">
              <a href="{{ url('/home') }}" class="site_title"><center><span style="font-size: 20px;">{{ $config->cs_name }}</span></center></a>
            </div>
                @if(Auth::user()->file == null)
                <img src="{{ asset('assets/img/logos/icon_user.png') }}" alt="" class="img-circle profile_img"/>
                @else
                <img src="{{ route('user.showprofile.getfile', [Auth::user()->id,Auth::user()->file]) }}" alt="" class="img-circle profile_img"/>
                @endif

            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile clearfix" style="background-color:#0b2215;">

              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2><br>
                <h2>{{ Auth::user()->email }}</h2>
              </div>
            </div>
            <legend></legend>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="background-color:#333d64;">
              <div class="menu_section" style="background-color:#0b2215">
                <h3>Menu</h3>
                <ul class="nav side-menu" style="background-color:#0b2215">
                  @if(Auth::user()->hasPermissionTo('menu.admin'))
                  <li style="background-color:#0b2215"><a><i class="fa fa-user"></i> Administration <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!-- <li><a href="#">Dashboard</a></li> -->
                      @if(Auth::user()->hasPermissionTo('menu.admin.users'))
                      <li><a href="{{url('/user')}}">Users</a></li>
                      @endif
                      @if(Auth::user()->hasPermissionTo('menu.admin.roles'))
                      <li><a href="{{ url('/admin/role') }}">Roles</a></li>
                      @endif
                      @if(Auth::user()->hasPermissionTo('menu.admin.permission'))
                      <li><a href="{{ url('/admin/permission')}}">Permission Manager</a></li>
                      @endif 
                      <!-- <li><a href="#">Menu Manager</a></li>
                      <li><a href="#">System Log</a></li>
                      <li><a href="#">System Configuration</a></li>
                      <li><a href="#">Audit Trails</a></li> -->
                    </ul>
                  </li>
                @endif
                @if(Auth::user()->hasPermissionTo('menu.master'))
                  <li><a><i class="fa fa-dashboard"></i> Master<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(Auth::user()->hasPermissionTo('menu.master.leave'))
                      <li><a href="{{url('/leave/master')}}">Master Leave</a></li>
                      @endif
                      @if(Auth::user()->hasPermissionTo('menu.master.config'))
                      <li><a href="{{url('/master/config')}}">System Config</a></li>
                      @endif
                      @if(Auth::user()->hasPermissionTo('menu.master.pb'))
                      <li><a href="{{url('/master/public_holiday')}}">Public Holiday</a></li>
                      @endif
                    </ul>
                  </li>
                @endif
                  <li><a href="{{ url('/home')}}"><i class="fa fa-home"></i> Home <!-- <span class="fa fa-chevron-down"></span> --></a>
                  </li>
                  @if(Auth::user()->hasPermissionTo('menu.leave'))
                  <li><a href="{{ url('/leave')}}"><i class="fa fa-calendar"></i> Leave </a></li>
                  @endif

                  @if(Auth::user()->hasPermissionTo('menu.ot'))
                  <li><a href="{{ url('/overtime')}}"><i class="fa fa-briefcase"></i> Overtime </a></li>
                  @endif

                  <li style="background-color:#0b2215"><a><i class="fa fa-print"></i> Document <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('document/letter') }}">Letter</a></li>
                      <li><a href="{{ url('document/transmittal') }}">Transmittal Note</a></li>
                      <li><a href="{{ url('document/others') }}">Others</a></li>
                    </ul>
                  </li>

              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav >
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                        <!-- <a href="javascript:;" data-toggle="modal" data-target="#modal-profile"><i class="fa fa-user fa-fw pull-right"></i>Profile</a> -->
                        <a href="{{url('user/showprofile')}}"><i class="fa fa-user"></i> Profile</a>
                        </li>
                        <!-- <li><a href="javascript:;" data-toggle="modal" data-target="#modal-password" data-backdrop="static"><i class="fa fa-key fa-fw pull-right"></i> Change Password</a></li> -->

                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>

                <!-- <li class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li> -->
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        @include('layouts.modal')
        <!-- page content -->
        <div class="right_col" role="main">
          @include('layouts.flash-message')
          @yield('content')
        </div>
        <!-- /page content -->


        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Tiara Vision Sdn Bhd
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script type="text/javascript">
  var session_id = "{!! (Session::getId())?Session::getId():'' !!}";
  var user_id = "{!! (Auth::user())?Auth::user()->id:'' !!}";

  // Initialize Firebase
  var config = {
    apiKey: "firebase.api_key",
    authDomain: "firebase.auth_domain",
    databaseURL: "firebase.database_url",
    storageBucket: "firebase.storage_bucket",
  };
  firebase.initializeApp(config);

  var database = firebase.database();

  if({!! Auth::user() !!}) {
    firebase.database().ref('/users/' + user_id + '/session_id').set(session_id);
  }

  firebase.database().ref('/users/' + user_id).on('value', function(snapshot2) {
    var v = snapshot2.val();

    if(v.session_id != session_id) {
        toastr.warning('Your account login from another device!!', 'Warning Alert', {timeOut: 3000});
        setTimeout(function() {
           window.location = '/login';
        }, 4000);
    } 
  });
</script>

    <!-- Scripts -->
    <script src="{{ asset('assets2/js/core.min.js') }}"></script>
    <script src="{{ asset('assets2/js/app.min.js') }}"></script>
    <script src="{{ asset('assets2/js/script.min.js') }}"></script>

    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('js/jQuery/jquery-2.2.3.min.js') }}"></script>
    
    <!-- jQuery -->
    <script src="{{ asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('assets/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('assets/vendors/nprogress/nprogress.js') }}"></script>
    <!-- validator -->
    <!-- <script src="{{ asset('assets/vendors/validator/validator.js') }}"></script> -->
    <!-- Dropzone.js -->
    <script src="{{ asset('assets/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{ asset('assets/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- gauge.js -->
    <script src="{{ asset('assets/vendors/gauge.js/dist/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('assets/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ asset('assets/vendors/skycons/skycons.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('assets/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('assets/vendors/DateJS/build/date.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('assets/vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ asset('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('assets/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('assets/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('assets/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('assets/vendors/starrr/dist/starrr.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('assets/build/js/custom.min.js') }}"></script>


<script>
    $('#myDatepickerLetterMonth').datetimepicker({
      format: 'MM'
    });
    $('#myDatepickerLetterYear').datetimepicker({
      format: 'YYYY'
    });

    $('#myDatepickerstart').datetimepicker({
      format: 'YYYY-MM-DD hh:mm:ss'
    });
    $('#myDatepickerend').datetimepicker({
      format: 'YYYY-MM-DD hh:mm:ss'
    });

    $('#myDatepickerstartLeave').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerendLeave').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerOT').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerStartTime').datetimepicker({
        format: 'hh:mm A'
    });
    $('#myDatepickerEndTime').datetimepicker({
        format: 'hh:mm A'
    });


    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>

<script>
      app.ready(function(){
        /*
        |--------------------------------------------------------------------------
        | Individual column searching
        |--------------------------------------------------------------------------
        */
        // Setup - add a text input to each footer cell
        $('#example-1 tfoot th').each( function () {
          var title = $(this).text();
          // $(this).html( '<input class="form-control" type="text" placeholder="Search '+title+'">' );
        });

        // DataTable
        var table = $('#example-1').DataTable({
          'scrollX': true,
        });

        // Apply the search
        table.columns().every( function () {
          var that = this;

          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that.search( this.value ).draw();
            }
          });
        });
      });

</script>


<script type="text/javascript">

$(document).ready(function(){

        $('.listbtn').on('click', function() {
        var id = $(this).data('id');

        $('#displaydiv').empty();

        $.ajax({

                  url: '{{ URL::to("admin/permission/getRolePermission") }}/'+id,
                  type: 'GET',
                  dataType: 'json',
                  success:function(data) {
                    // alert(data);
                    $('#displaydiv').append(data.html);
                    $('#modaldialog').attr('style','width:'+data.width+'%;');

                  },

                  error: function(jqxhr, status, exception) {
                      //debug error
                       // alert('{{ URL::to("admin/permission/getRolePermission") }}/'+id);
                   }

              });

              $('#modal-2').modal('show');

      });

      $('.editbtn').on('click', function() {
        
        var id = $(this).data('id');

        $('#displaydiv1').empty();

        $.ajax({

                  url: '{{ URL::to("admin/role/getRole") }}/'+id,
                  type: 'GET',
                  dataType: 'json',
                  success:function(data) {
                    // alert(data);
                    $('#displaydiv1').append(data.html);
                    // $('#modaldialog1').attr('style','width:'+data.width+'%;');

                  },

                  error: function(jqxhr, status, exception) {
                      //debug error
                      //alert('{{ URL::to("admin/role/getRole") }}/'+id);
                   }

              });

              $('#modal-1').modal('show');

      });


      $('.editbtnPerm').on('click', function() {
        var id = $(this).data('id');
        
        $('#displaydivPerm').empty();

        $.ajax({

                  url: '{{ URL::to("admin/permission/getPerm") }}/'+id,
                  type: 'GET',
                  dataType: 'json',
                  success:function(data) {
                     //alert(data);
                    $('#displaydivPerm').append(data.html);
                  },

                  error: function(jqxhr, status, exception) {
                      //debug error
                      //alert('{{ URL::to("admin/permission/getPerm") }}/'+id);
                   }

              });

              $('#modal-edit-perm').modal('show');

      });
      

      $('.editbtnmasterLeave').on('click', function() {
        var id = $(this).data('id');
        
        $('#displaydivML').empty();

        $.ajax({

                  url: '{{ URL::to("leave/master/getCF") }}/'+id,
                  type: 'GET',
                  dataType: 'json',
                  success:function(data) {
                     //alert(data);
                    $('#displaydivML').append(data.html);
                  },

                  error: function(jqxhr, status, exception) {
                      //debug error
                      //alert('{{ URL::to("admin/permission/getPerm") }}/'+id);
                   }

              });

              $('#modal-edit-ml').modal('show');

      });


      $("#add_leave").click(function(){

            var lastNumber  = $("#tablePublicHoliday tr:last").val();
            var nextNumber = parseInt(lastNumber) + 1;

            var $fieldA= '<input type="hidden" name="counter[]" value="' + nextNumber + '"/>';
            var $field1 = '<div class="input-group date col-md-12 col-sm-12 col-xs-12" ><input type="text" class="form-control myDatepickerLeave" name="ph_date[]"></input></div>'

            var $field2= '<input type="text" name="ph_description[]" type="text" class="form-control col-md-7 col-xs-12"/>';

            var $field3= "<a id='id_remove' data-original-title='Remove Date' data-toggle='tooltip' data-placement='top' class='btn btn-sm btn-remove btn-danger' ng-click='remove(item)'><i class='fa fa-trash-o'></i></a>";

            var $row="<tr><td></td><td>"+$fieldA+$field1+"</td><td>"+$field2+"</td><td style='text-align: center;'>"+$field3+"</td></tr>";
            
            $("#tablePublicHoliday:last").append($row);
            
            $(".btn-remove").click(function() {
                    $(this).parent().parent().remove();
            });

            $('.myDatepickerLeave').datetimepicker({
              format: 'YYYY-MM-DD'
            });
            
      });



      $("#add_letterattach").click( function(){

            var lastNumber  = $("#tableLetterAttachment tr:last").val();
            var rowCount = $('#tableLetterAttachment tr').length; 

            //alert('row count = '+rowCount);

            var nextNumber = parseInt(lastNumber) + 1;

            var $fieldA= '<input type="hidden" name="counter[]" value="' + nextNumber + '"/>';
            var $field1 = '<input type="file" onchange="ValidateMultipleSize(this)" class="form-control col-md-12 col-xs-12" name="fileattachment[]" id="idfileattachment'+rowCount+'" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">'

            var $field2= '<input type="text" name="ph_description[]" type="text" class="form-control col-md-7 col-xs-12"/>';

            var $field3= "<a id='id_remove' data-original-title='Remove Date' data-toggle='tooltip' data-placement='top' class='btn btn-sm btn-remove btn-danger' ng-click='remove(item)'><i class='fa fa-trash-o'></i></a>";

            var $row="<tr><td>"+$fieldA+$field1+"</td><td style='text-align: center;'>"+$field3+"</td></tr>";
            
            $("#tableLetterAttachment:last").append($row);
            
            $(".btn-remove").click(function() {
                    $(this).parent().parent().remove();
            });

            $('#idcountrow').val(rowCount);

            $('.myDatepickerLeave').datetimepicker({
              format: 'YYYY-MM-DD'
            });
            
      });


      $("#add_transmittalattach").click( function(){

            var lastNumber  = $("#tableTransmittalAttachment tr:last").val();
            var nextNumber = parseInt(lastNumber) + 1;

            var rowCount = $('#tableTransmittalAttachment tr').length; 

            var $fieldA= '<input type="hidden" name="counter[]" value="' + nextNumber + '"/>';
            var $field1 = '<input type="file" onchange="ValidateMultipleSize(this)" class="form-control col-md-12 col-xs-12" name="fileattachment[]" id="idfileattachment'+rowCount+'" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">'

            var $field2= '<input type="text" name="ph_description[]" type="text" class="form-control col-md-7 col-xs-12" />';

            var $field3= "<a id='id_remove' data-original-title='Remove Date' data-toggle='tooltip' data-placement='top' class='btn btn-sm btn-remove btn-danger' ng-click='remove(item)'><i class='fa fa-trash-o'></i></a>";

            var $row="<tr><td>"+$fieldA+$field1+"</td><td style='text-align: center;'>"+$field3+"</td></tr>";
            
            $("#tableTransmittalAttachment:last").append($row);
            
            $(".btn-remove").click(function() {
                $(this).parent().parent().remove();
            });

            $('#idcountrow').val(rowCount);

            $('.myDatepickerLeave').datetimepicker({
              format: 'YYYY-MM-DD'
            });
            
      });

      $("#add_docattach").click( function(){

            var lastNumber  = $("#tableDocAttachment tr:last").val();
            var nextNumber = parseInt(lastNumber) + 1;

            var rowCount = $('#tableDocAttachment tr').length;

            var $fieldA= '<input type="hidden" name="counter[]" value="' + nextNumber + '"/>';
            var $field1 = '<input type="file" onchange="ValidateMultipleSize(this)" class="form-control col-md-12 col-xs-12" name="fileattachment[]" id="idfileattachment'+rowCount+'" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">'

            var $field2= '<input type="text" name="ph_description[]" type="text" class="form-control col-md-7 col-xs-12" />';

            var $field3= "<a id='id_remove' data-original-title='Remove Date' data-toggle='tooltip' data-placement='top' class='btn btn-sm btn-remove btn-danger' ng-click='remove(item)'><i class='fa fa-trash-o'></i></a>";

            var $row="<tr><td>"+$fieldA+$field1+"</td><td style='text-align: center;'>"+$field3+"</td></tr>";
            
            $("#tableDocAttachment:last").append($row);
            
            $(".btn-remove").click(function() {
                    $(this).parent().parent().remove();
            });

            $('#idcountrow').val(rowCount);

            $('.myDatepickerLeave').datetimepicker({
              format: 'YYYY-MM-DD'
            });
            
      });

      $("#id_cat").change(function(){

        var selectedCat = $(this).children("option:selected").val();
        // alert(selectedCountry);

        $("select option:selected").each(function(){

            if(selectedCat=="12"){
              
              $('div.divcategory').show();

            }else{
              $('div.divcategory').hide();
            }

        });

      });


      $("#id_pack").change(function(){
        var selectedPackage = $(this).children("option:selected").val();
        // alert(selectedPackage);
        $("select option:selected").each(function(){

            if(selectedPackage=="Others"){
              
              $('div.divpackage').show();

            }else{
              $('div.divpackage').hide();
            }

        });

      });

      $("#custid").change(function(){
        var selectedCustomer = $(this).children("option:selected").val();
        //alert(selectedCustomer);

        $("select option:selected").each(function(){
            if(selectedCustomer=="6"){
              $('div.divcust').show();
            }else{
              $('div.divcust').hide();
            }

        });

      });


      $("#id_cust").change(function(){
        var selectedCust = $(this).children("option:selected").val();
        $("select option:selected").each(function(){

            if(selectedCust=="Others"){
              
              $('div.divcust').show();

            }else{
              $('div.divcust').hide();
            }

        });

      });


      $('#select-all').click(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;                        
            });
        }
        else {
          $(':checkbox').each(function() {
                this.checked = false;
            });
        }
      });


      $("#updateletter").click(function(){

        //alert('before update');

        numfiles = $('#idcountrow').val();

        var totalFilesize=0;

        //alert('num file'+numfiles); 

        for($i=0;$i<=numfiles;$i++) {

          //alert('loop'+$i);
          //alert($('#idfileattachment'+$i)[0].files[0].size);
          totalFilesize = totalFilesize + ($('#idfileattachment'+$i)[0].files[0].size / 1024 / 1024);
          
        }

        if (totalFilesize > 390) {

          alert('Total file size is too large and exceeds 390 MB');

          for($i=0;$i<=numfiles;$i++) {

              $('#idfileattachment'+$i).val(''); //for clearing with Jquery
          
          }

          $('#idtotalsize').val(totalFilesize);
          
        }
        //alert(totalFilesize);

      });


      $("#updatetransmittal").click(function(){

        numfiles = $('#idcountrow').val();

        var totalFilesize=0;

        for($i=0;$i<=numfiles;$i++) {

          totalFilesize = totalFilesize + ($('#idfileattachment'+$i)[0].files[0].size / 1024 / 1024);
          
        }

        if (totalFilesize > 390) {

          alert('Total file size is too large and exceeds 390 MB');

          for($i=0;$i<=numfiles;$i++) {

              $('#idfileattachment'+$i).val(''); //for clearing with Jquery
          
          }

          $('#idtotalsize').val(totalFilesize);
          
        }

      });

      
      $("#updatedocument").click(function(){

        numfiles = $('#idcountrow').val();

        var totalFilesize=0;

        for($i=0;$i<=numfiles;$i++) {

          totalFilesize = totalFilesize + ($('#idfileattachment'+$i)[0].files[0].size / 1024 / 1024);
        }

        if (totalFilesize > 390) {

          alert('Total file size is too large and exceeds 390 MB');

          for($i=0;$i<=numfiles;$i++) {

              $('#idfileattachment'+$i).val(''); //for clearing with Jquery
          
          }

          $('#idtotalsize').val(totalFilesize);
          
        }

      });


});

function toggleCheck(source) {
    checkboxes = document.getElementsByTagName('input');

    for(var i=0, l=checkboxes.length; i<l; i++) {
      checkboxes[i].checked = source.checked;
    }
}

function ValidateMultipleSize(file) {

      
      var numFiles = $("input:file")[0].files.length;
      //alert(numFiles);
      var $i, FileSize, totalFilesize=0 ;

      for($i=0;$i<numFiles;$i++) {
         
          totalFilesize = totalFilesize + (file.files[$i].size / 1024 / 1024);

      }

      if (totalFilesize > 390) {
          alert('File size is too large and exceeds 390 MB');
         $(file).val(''); //for clearing with Jquery
      }

      // alert(totalFilesize);

      // var FileSize = file.files[0].size / 1024 / 1024; // in MB
      // if (FileSize > 350) {
      //     alert('File size exceeds 350 MB');
      //    $(file).val(''); //for clearing with Jquery
      // } else {

      // }
}

</script>
  
  </body>
</html>