@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/others') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'DocumentController@updateDoc', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          {{ csrf_field() }}
        <span class="section">Document Form</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Sub-Category <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="subcategory" id="id_cat" required="required">
                        <option value="">-- Choose Category --</option>
                        @foreach($subcat as $sc)
                            <option value="{{ $sc->sub_id }}" @if($sc->sub_id == $document->doc_subid) selected='selected' @endif>{{ $sc->sub_name }}</option>
                        @endforeach
                    </select>
                    @if($document->doc_subid == "12") <!-- indicate others -->
                    <input id="" class="form-control col-md-7 col-xs-12" name="cat_other" type="text" value="{{ $document->doc_subother }}">
                    @endif
                    <div class="divcategory" style="display:none;">
                      <input id="id_subcat" class="form-control col-md-7 col-xs-12" name="cat_other2" type="text" value="" placeholder="Enter Category Name">
                    </div>
                </div>
                <input type="hidden" value="{{ $document->doc_id }}" name="docid">
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Customer<span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="customer" id="custid" required="required">
                        <option value="">-- Choose Customer --</option>
                        @foreach($cust as $cust)
                            <option value="{{ $cust->cust_id }}" @if($cust->cust_id== $document->doc_customerid) selected='selected' @endif>{{ $cust->cust_name }}</option>
                        @endforeach
                  </select>
                    @if($document->doc_customerid == "6") <!-- indicate others -->
                    <input id="" class="form-control col-md-7 col-xs-12" name="cust_other" type="text" value="{{ $document->doc_customerother }}">
                    @endif
                    <div class="divcust" style="display:none;">
                      <input id="id_custother" class="form-control col-md-7 col-xs-12" name="cust_other2" type="text" value="" placeholder="Enter Customer Name">
                    </div>
                </div>
                <input type="hidden" value="{{$document->doc_id}}" name="docid">
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Package<span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="package" id="id_pack" required="required">
                        <option value="">-- Choose Package --</option>
                        @foreach($package as $pack)
                            <option value="{{ $pack->pck_name }}" @if($pack->pck_name == $document->doc_package) selected='selected' : @endif>{{ $pack->pck_name }}</option>
                        @endforeach
                    </select>
                    @if($document->doc_package == "Others")
                    <input id="" class="form-control col-md-7 col-xs-12" name="doc_other" type="text" value="{{ $document->doc_otherpack }}">
                    @endif
                    <div class="divpackage" style="display:none;">
                    <input id="id_docpack" class="form-control col-md-7 col-xs-12" name="doc_other2" type="text" value="" placeholder="Enter Package Name">
                    </div>
                    
                </div>
              </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Document Number<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="doc_number" required="required" type="text" value="{{ $document->doc_number }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="filename" required="required" type="text" value="{{ $document->doc_name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
                    <thead>
                      <tr>
                        <th>File Name</th>
                        <th>
                          <center>
                            <a id="add_docattach" data-original-title="add new row" data-toggle="tooltip" data-placement="top" class="btn btn-sm btn-addrow btn-danger"> 
                              <i class="fa fa-plus"></i>
                            </a>
                          </center>
                        </th>
                      </tr>
                    </thead>
                    <tbody id="tableDocAttachment"></tbody>
                    <input type="hidden" name="docid" value="{{ $document->doc_id }}">
                    <tbody>
                      @foreach($docAttach as $la)
                      <tr>
                        <td><a data-toggle="tooltip" title="{{ $la->docatt_file }}" href="{{ route('document.other.getfilereply', [$la->docatt_id,$la->docatt_file]) }}">
                        {{ str_limit($la->docatt_file, $limit = 15, $end = '...') }}
                        </a>
                        </td>
                        <td><center><a data-toggle="tooltip" title="{{ __('Delete Attachment') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('document.other.removeattachment', $la->docatt_id) }}" onclick="return confirm('Delete Attachment?')"><i class="fa fa-trash-o"></i></a></center></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align:justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" required="required">{{ $document->doc_description }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Upload Date</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{!! date('Y-m-d', strtotime($preparedBy->prepareddate)) !!}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Modified Date</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{!! date('Y-m-d', strtotime($modifiedBy->modifieddate)) !!}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Upload by</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $preparedBy->name }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Modified by</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $modifiedBy->name }}" readonly="readonly">
              </div>
            </div>

            <input type="hidden" name="countrow" id="idcountrow">
            <input type="hidden" name="totalsize" id="idtotalsize">

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button id="updatedocument" type="submit" class="btn btn-primary">Update</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          </div>
        </div> <!-- //close row -->
        {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection