@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/others') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'DocumentController@storeDocument', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

        {{ csrf_field() }}
        <span class="section">Document Form</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Sub-Category <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="subcategory" id="id_cat">
                        <option value="">-- Choose Category --</option>
                        @foreach($subcat as $sc)
                            <option value="{{ $sc->sub_id }}" {{ old('subcategory') == $sc->sub_id ? 'selected' : '' }}>{{ $sc->sub_name }}
                            </option>
                        @endforeach
                  </select>
                  <div class="divcategory" style="display:none;">
                    <input id="id_subcat" class="form-control col-md-7 col-xs-12" name="cat_other" type="text" value="" placeholder="Enter Categroy Name">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Customer <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="customer" id="custid">
                        <option value="">-- Choose Customer --</option>
                        @foreach($cust as $cs)
                            <option value="{{ $cs->cust_id }}" {{ old('customer') == $cs->cust_id ? 'selected' : '' }}>{{ $cs->cust_name }}
                            </option>
                        @endforeach
                  </select>
                  <div class="divcust" style="display:none;">
                    <input id="id_custother" class="form-control col-md-7 col-xs-12" name="cust_other" type="text" value="" placeholder="Enter Customer Name">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Package <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="package" id="id_pack">
                        <option value="">-- Choose Package --</option>
                        @foreach($package as $pk)
                            <option value="{{ $pk->pck_name }}" {{ old('package') == $pk->pck_name ? 'selected' : '' }} >{{ $pk->pck_name }}
                            </option>
                        @endforeach
                  </select>
                  <div class="divpackage" style="display:none;">
                    <input id="id_docpack" class="form-control col-md-7 col-xs-12" name="doc_other" type="text" value="" placeholder="Enter Package Name">
                  </div>
                </div>
              </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Document Number <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="docnumber" class="form-control col-md-7 col-xs-12" name="docnumber" type="text" value="{{ request()->input('name', old('docnumber')) }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="filename" class="form-control col-md-7 col-xs-12" name="filename" type="text" value="{{ request()->input('name', old('filename')) }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="file" onchange="ValidateMultipleSize(this)" class="form-control col-md-12 col-xs-12" name="fileattachment[]" multiple="multiple" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:250px;">{{ Input::old('description') }}</textarea>
              </div>
            </div>
          </div>
        </div> <!-- //close row -->

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection