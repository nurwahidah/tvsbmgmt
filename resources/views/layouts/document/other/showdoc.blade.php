@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/others') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'DocumentController@storeDocument', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          {{ csrf_field() }}
        <span class="section">Document Form</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Sub-Category <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat" readonly="readonly">
                        <option value="">-- Choose Category --</option>
                        @foreach($subcat as $sc)
                            <option value="{{ $sc->sub_id }}" @if($sc->sub_id == $document->doc_subid) selected='selected' @endif>{{ $sc->sub_name }}</option>
                        @endforeach
                    </select>
                    @if($document->doc_subid == "12") <!-- indicate others -->
                    <input id="" class="form-control col-md-7 col-xs-12" name="cat_other" type="text" value="{{ $document->doc_subother }}" readonly="readonly">
                    @endif
                </div>
                <input type="hidden" value="{{$document->doc_id}}" name="docid">
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Customer<span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat" readonly="readonly">
                        <option value="">-- Choose Customer --</option>
                        @foreach($cust as $cust)
                            <option value="{{ $cust->cust_id }}" @if($cust->cust_id== $document->doc_customerid) selected='selected' @endif>{{ $cust->cust_name }}</option>
                        @endforeach
                    </select>
                    @if($document->doc_customerid == "6") <!-- indicate others -->
                    <input id="" class="form-control col-md-7 col-xs-12" name="cust_other" type="text" value="{{ $document->doc_customerother }}" readonly="readonly">
                    @endif
                </div>
                <input type="hidden" value="{{$document->doc_id}}" name="docid">
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Package<span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat" readonly="readonly">
                        <option value="">-- Choose Package --</option>
                        @foreach($package as $pack)
                            <option value="{{ $pack->pck_name }}" @if($pack->pck_name == $document->doc_package) selected='selected' : @endif>{{ $pack->pck_name }}</option>
                        @endforeach
                    </select>
                    @if($document->doc_package == "Others")
                    <input id="" class="form-control col-md-7 col-xs-12" name="doc_other" type="text" value="{{ $document->doc_otherpack }}" readonly="readonly">
                    @endif
                </div>
              </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Document Number<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $document->doc_number }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $document->doc_name }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  @foreach($docAttach as $la)
                    <a data-toggle="tooltip" title="{{$la->docatt_file}}" href="{{ route('document.other.getfilereply', [$la->docatt_id,$la->docatt_file]) }}">
                    {{ str_limit($la->docatt_file, $limit = 15, $end = '...') }}
                    </a><br>
                  @endforeach
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align:justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" readonly="readonly">{{ $document->doc_description }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Upload Date<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{!! date('Y-m-d', strtotime($preparedBy->prepareddate)) !!}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Modified Date<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{!! date('Y-m-d', strtotime($modifiedBy->modifieddate)) !!}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Upload by<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $preparedBy->name }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Modified by<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $modifiedBy->name }}" readonly="readonly">
              </div>
            </div>
          </div>
        </div> <!-- //close row -->
        {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection