@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/transmittal') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'TransmittalController@storeTransmittal', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          {{ csrf_field() }}
        <span class="section">Transmittal Note</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Category <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="customer" id="id_cust">
                        <option value="">-- Choose Customer --</option>
                        @foreach($cust as $cs)
                            <option value="{{ $cs->cust_name }}" {{ old('customer') == $cs->cust_name ? 'selected' : '' }}>{{ $cs->cust_name }}
                            </option>
                        @endforeach
                  </select>
                  <div class="divcust" style="display:none;">
                    <input id="id_custother" class="form-control col-md-7 col-xs-12" name="cust_other" type="text" value="" placeholder="Enter Customer Name">
                  </div>
                </div>
              </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Month <span class="required" style="color:red">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerLetterMonth'>
                      <input type='text' class="form-control" name="month" value="{{ request()->input('month', old('month')) }}" />
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Year <span class="required" style="color:red">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerLetterYear'>
                      <input type='text' class="form-control" name="year" value="{{ request()->input('year', old('year')) }}"/>
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" type="text" value="{{ request()->input('name', old('name')) }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="file" onchange="ValidateMultipleSize(this)" class="form-control col-md-12 col-xs-12" name="fileattachment[]" multiple="multiple" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:250px;">{{ Input::old('description') }}</textarea>
              </div>
            </div>
          </div>
        </div> <!-- //close row -->

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection