@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/letter') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'LetterController@storeLetter', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          {{ csrf_field() }}
            <span class="section">Letter Form</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Letter Number<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $letter->let_number }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $letter->let_name }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <!-- <input type="file" class="form-control col-md-12 col-xs-12" name="fileattachment[]" multiple="multiple" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;"> -->
                  @foreach($letterAttach as $la)
                    <a data-toggle="tooltip" title="{{$la->lratt_file}}" href="{{ route('document.letter.getfilereply', [$la->lratt_id,$la->lratt_file]) }}">
                    {{ str_limit($la->lratt_file, $limit = 15, $end = '...') }}
                    </a><br>
                  @endforeach
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align:justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:150px;" readonly="readonly">{{ $letter->let_desc }}</textarea>
              </div>
            </div>
          </div>
        </div> <!-- //close row -->
        {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection