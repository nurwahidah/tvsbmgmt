@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <span class="section">
          <a href="{{ route('document.letter.create') }}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Create New Letter</a>
        </span>
          {{ csrf_field() }}
            <!-- table-->
          <div class="table-responsive">
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Letter Number</th>
                          <th>File Name</th>
                          <th>Upload Date Time</th>
                          <th><center>Attachment</center></th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($listLetter as $llt)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $llt->let_number }}</td>
                          <td>{{ $llt->let_name }}</td>
                          <td>{!! date('Y-m-d H:m:s', strtotime($llt->let_prepareddate)) !!}</td>
                          <td><center>
                            @if($llt->let_attachment == 1)
                              <span class="label label-success"><i class="fa fa-check"></i></span>
                            @else
                              <span class="label label-danger"><i class="fa fa-close"></i></span>
                            @endif
                            </center>
                          </td>
                          <td>
                            <a data-toggle="tooltip" title="{{ __('Letter Detail') }}" class="btn btn-icon btn-info btn-sm" href="{{ route('document.letter.showletter', $llt->let_id) }}"><i class="fa fa-info-circle"></i></a>

                          @if($llt->let_preparedby == auth::user()->id)
                            <a data-toggle="tooltip" title="{{ __('Edit Letter') }}" class="btn btn-icon btn-warning btn-sm" href="{{ route('document.letter.editletter', $llt->let_id) }}"><i class="fa fa-pencil-square-o"></i></a>

                            @if($llt->let_permissiondel == 0 && $llt->let_verifiedby == null)
                            <a data-toggle="tooltip" title="{{ __('Request Delete') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('document.letter.requestremoveletter', $llt->let_id) }}" onclick="return confirm('Request to delete?')"><i class="fa fa-bullseye"></i>
                            </a>
                            @endif
                          @endif

                            <!-- @if($llt->let_permissiondel ==1 && $llt->let_verifiedby != null && auth::user()->id == $llt->let_preparedby)
                            <a data-toggle="tooltip" title="{{ __('Delete Letter') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('document.letter.remove', $llt->let_id) }}" onclick="return confirm('Delete Letter?')"><i class="fa fa-trash-o"></i></a>
                            @endif -->
                            @if(Auth::user()->hasPermissionTo('letter.verifiedremove') && $llt->let_permissiondel ==1)
                              <a data-toggle="tooltip" title="{{ __('Approved Delete') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('document.letter.remove', $llt->let_id) }}" onclick="return confirm('Delete this letter?')"><i class="fa fa-check"></i>
                              </a>
                              <a data-toggle="tooltip" title="{{ __('Decline Delete') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('document.letter.cancelremoveletter', $llt->let_id) }}" onclick="return confirm('Decline Request?')"><i class="fa fa-minus"></i>
                              </a>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />
@endsection


