@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('document/letter') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br/>
          {!! Form::open(['action'=>'LetterController@updateLetter', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

          {{ csrf_field() }}
            <span class="section">Letter Form</span>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">Letter Number<span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" value="{{ $letter->let_number }}" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">File Name <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" name="filename" required="required" type="text" value="{{ $letter->let_name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
                    <thead>
                      <tr>
                        <th>File Name</th>
                        <th>
                          <center>
                            <a id="add_letterattach" data-original-title="add new row" data-toggle="tooltip" data-placement="top" class="btn btn-sm btn-addrow btn-danger"> 
                              <i class="fa fa-plus"></i>
                            </a>
                          </center>
                        </th>
                      </tr>
                    </thead>
                    <tbody id="tableLetterAttachment"></tbody>
                    <input type="hidden" name="letterid" value="{{ $letter->let_id }}">
                    <tbody>
                      @foreach($letterAttach as $la)
                      <tr>
                        <td><a data-toggle="tooltip" title="{{$la->lratt_file}}" href="{{ route('document.letter.getfilereply', [$la->lratt_id,$la->lratt_file]) }}">{{ str_limit($la->lratt_file, $limit = 15, $end = '...') }}</a>
                        </td>
                        <td><center><a data-toggle="tooltip" title="{{ __('Delete Attachment') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('document.letter.removeattachment', $la->lratt_id) }}" onclick="return confirm('Delete Attachment?')"><i class="fa fa-trash-o"></i></a></center></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align:justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:150px;" required="required">{{ $letter->let_desc }}</textarea>
              </div>
            </div>
          </div>
        </div> <!-- //close row -->
        <input type="hidden" name="countrow" id="idcountrow">
        <input type="hidden" name="totalsize" id="idtotalsize">


          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button id="updateletter" type="submit" class="btn btn-primary">Update</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection