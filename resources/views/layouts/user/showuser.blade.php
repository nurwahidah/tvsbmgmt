@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <a href="{{ url('/user') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

          {{ csrf_field() }}

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Personal Info <!-- <small>Activity report</small> --></h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          @if($userdetail->file == null)
                          <img src="{{ asset('assets/img/logos/icon_user.png') }}" alt="" class="img-responsive avatar-view" style="width:150px; height:140px;" />
                          @else
                          <img src="{{ route('user.showprofile.getfile', [$userdetail->id,$userdetail->file]) }}" alt="" class="img-responsive avatar-view" style="width:150px; height:140px;"/>
                          @endif
                        </div>
                      </div>
                      <h4>{{ $userdetail->name }}</h4>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-envelope-square"></i>  {{ $userdetail->email }}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i>  {{ $userdetail->designation }}
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone"></i>  {{ $userdetail->phone }}
                        </li>
                      </ul>
                      {!! ($userdetail->status=='A') ? '<span class="label label-success">'.__('Active').'</span>' : '<span class="label label-danger">'.__('Disabled').'</span>' !!}
                      <!-- <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a> -->
                      <br />

                      <!-- start skills -->
                      <h4>Leave</h4>
                      <ul class="list-unstyled user_data">
                        <li>
                          <p>Carry Forward Leave ~ {{ $userdetail->cf_leave + 0}} /{{ $userdetail->cf_leave + $userdetail->cf_leaveused }}</p>
                          <div class="progress progress_sm">
                            @if($userdetail->cf_leave == 0 || $userdetail->cf_leave == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ ($userdetail->cf_leave / ($userdetail->cf_leave + $userdetail->cf_leaveused)) * 100 }}">
                            </div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>Replacement Leave Balance ~ {{ $userdetail->rl_balance + 0 }}/{{ $userdetail->rl + 0 }}</p>
                          <div class="progress progress_sm">
                            @if($userdetail->rl_balance == 0 || $userdetail->rl_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ ($userdetail->rl_balance / $userdetail->rl) * 100 }}">
                            </div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>Leave Balance ~ {{ $userdetail->leave_balance + 0 }}/{{ $userdetail->leave + 0 }}</p>
                          <div class="progress progress_sm">
                            @if($userdetail->leave_balance == 0 || $userdetail->leave_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ ($userdetail->leave_balance / $userdetail->leave) * 100 }}">
                            </div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>MC Balance ~ {{ $userdetail->mc_balance + 0 }}/{{ $userdetail->mc + 0 }}</p>
                          <div class="progress progress_sm">
                            @if($userdetail->mc_balance == 0 || $userdetail->mc_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ ($userdetail->mc_balance / $userdetail->mc) * 100 }}">
                            </div>
                            @endif
                          </div>

                        </li>
                      </ul>
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Details</a>
                          </li>
                          <!-- <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Change Password</a>
                          </li> -->
                          <!-- <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
                          </li> -->
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" required="required" type="text" readonly="readonly" value="{{ $userdetail->name }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Designation
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="designation" required="required" type="text" readonly="readonly" value="{{ $userdetail->designation }}">
                              </div>
                            </div> -->
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Leave
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="leave" name="leave" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->leave }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Leave used
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="leave_used" name="leave_used" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->leave_used }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Leave balanced
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="leave_balance" name="leave_balance" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->leave_balance }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">MC
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc" name="mc" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->mc }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">MC used
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc_used" name="leave_used" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->mc_used }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">MC balanced
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc_balance" name="mc_balance" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->mc_balance }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">CF
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc" name="mc" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->cf_leave }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">CF used
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc_used" name="leave_used" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->cf_leaveused }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Month Limit
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc_balance" name="mc_balance" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->cf_monthlimit }}">
                              </div>
                            </div>

                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">RL
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="rl" name="rl" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->rl }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">RL used
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="rl_used" name="rl_used" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->rl_used }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">RL balanced
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="rl_balance" name="rl_balance" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" readonly="readonly" value="{{ $userdetail->rl_balance }}">
                              </div>
                            </div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            Tab 2
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p>Tab 3</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

@endsection