@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <!-- <div class="x_title">
        <a href="{{ url('/user') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
        
        <div class="clearfix"></div>
      </div> -->
      <div class="x_content">


          {{ csrf_field() }}

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Personal Information <!-- <small>Activity report</small> --></h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->

                          @if(Auth::user()->file == null)
                          <img src="{{ asset('assets/img/logos/icon_user.png') }}" alt="" class="img-responsive avatar-view" style="width:150px; height:140px;" />
                          @else
                          <img src="{{ route('user.showprofile.getfile', [Auth::user()->id,Auth::user()->file]) }}" alt="" class="img-responsive avatar-view" style="width:150px; height:140px;"/>
                          @endif

                        </div>
                      </div>
                      <h4>{{ Auth::user()->name }}</h4>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-envelope-square"></i>  {{ Auth::user()->email }}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i>  {{ Auth::user()->designation }}
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone"></i>  {{ Auth::user()->phone }}
                        </li>
                      </ul>
                      {!! (Auth::user()->status=='A') ? '<span class="label label-success">'.__('Active').'</span>' : '<span class="label label-danger">'.__('Disabled').'</span>' !!}
                      <!-- <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a> -->
                      <br />

                      <!-- start skills -->
                      <h4>Leave</h4>
                      <ul class="list-unstyled user_data">
                        <li>

                          <p>Carry Forward Leave ~ {{ Auth::user()->cf_leave + 0 }}/{{ Auth::user()->cf_leave + Auth::user()->cf_leaveused }} (before {{ (DateTime::createFromFormat('!m', Auth::user()->cf_monthlimit)->format('F')) }} )</p>
                          <div class="progress progress_sm">
                            @if(Auth::user()->cf_leave == 0 || Auth::user()->cf_leave == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ (Auth::user()->cf_leave / (Auth::user()->cf_leave + Auth::user()->cf_leaveused)) * 100 }}"></div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>Replacement Leave Balance ~ {{ Auth::user()->rl_balance + 0 }}/{{ Auth::user()->rl + 0 }}</p>
                          <div class="progress progress_sm">
                            @if(Auth::user()->rl_balance == 0 || Auth::user()->rl_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ (Auth::user()->rl_balance / Auth::user()->rl) * 100 }}">
                            </div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>Leave Balance ~ {{ Auth::user()->leave_balance + 0 }}/{{ Auth::user()->leave + 0 }}
                          </p>
                          <div class="progress progress_sm">
                            @if(Auth::user()->leave_balance == 0 || Auth::user()->leave_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ (Auth::user()->leave_balance / Auth::user()->leave) * 100 }}"></div>
                            @endif
                          </div>
                        </li>
                        <li>
                          <p>MC Balance ~ {{ Auth::user()->mc_balance + 0 }}/{{ Auth::user()->mc + 0 }}</p>
                          <div class="progress progress_sm">
                            @if(Auth::user()->mc_balance == 0 || Auth::user()->mc_balance == null)
                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="0"></div>
                            @else
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ (Auth::user()->mc_balance / Auth::user()->mc) * 100 }}"></div>
                            @endif
                          </div>
                        </li>
                      </ul>
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Profile</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Change Password</a>
                          </li>
                          <!-- <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
                          </li> -->
                        </ul>
                        <div id="myTabContent" class="tab-content">

                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            {!! Form::open(['action'=>'UserController@updateProfile', 'method'=>'post', 'class'=>'form-horizontal form-label-left','files'=>true]) !!}

                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" style="text-align: justify;">Upload Photo
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" class="form-control col-md-12 col-xs-12" name="fileattachment" style="height: 40px;">
                              </div>
                            </div>

                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" style="text-align: justify;">Name <!-- <span class="required">*</span> -->
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" required="required" type="text" value="{{ Auth::user()->name }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation" style="text-align: justify;">Designation <!-- <span class="required">*</span> -->
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="designation" required="required" type="text" value="{{ Auth::user()->designation }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email" style="text-align: justify;">Email <!-- <span class="required">*</span> -->
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ Auth::user()->email }}">
                              </div>
                            </div>
                            <div class="row item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number" style="text-align: justify;">Mobile No <!-- <span class="required">*</span> -->
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="phone" name="phone" class="form-control col-md-7 col-xs-12" value="{{ Auth::user()->phone }}">
                              </div>
                            </div>

                            <div class="modal-footer">
                              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
                            </div>
                            {!! Form::close() !!}
                          </div>
                          

                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            {!! Form::open(['action'=>'UserController@changePassword', 'method'=>'post', 'class'=>'form-horizontal form-label-left']) !!}
                              <div class="row item form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align:justify;">Current Password <span class="required" style="color:red">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input class="form-control" type="password" value="" name="currpass" id="currpass" placeholder="" required="required"/>
                                </div>          
                              </div>
                              <div class="row item form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align:justify;">New Password <span class="required" style="color:red">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input class="form-control" type="password" value="" name="password" placeholder="" required="required"/>
                                </div>          
                              </div>
                              <div class="row item form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align:justify;">Confirm New Password <span class="required" style="color:red">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input class="form-control" type="password" value="" name="password_confirmation" placeholder="" required="required"/>
                                </div>          
                              </div>
                            
                            <div class="modal-footer">
                              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
                            </div>
                            {!! Form::close() !!}

                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

@endsection