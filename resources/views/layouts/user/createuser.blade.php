@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <a href="{{ url('/user') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          {{ csrf_field() }}

            @if($user->id != '')
              {!! Form::open(['action'=>'UserController@updateUser', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}
            @else
              {!! Form::open(['action'=>'UserController@storeUser', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}
            @endif

            <form class="form-horizontal form-label-left" novalidate>

              @if($user->id != '')
              <input type="hidden" name="id" value="{{$user->id}}">
              <span class="section">Edit User</span>
              @else
              <span class="section">Create New User</span>
              @endif

              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name" style="text-align: justify">Name <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" required="required" type="text" value="{{ $user->name }}">
                      @else
                      <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" required="required" type="text" value="{{ request()->input('name', old('name')) }}">
                      @endif
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="item form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="designation" style="text-align: justify">Designation <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        @if($user->id != '')
                        <input id="designation" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="designation" required="required" type="text" value="{{ $user->designation }}">
                        @else
                        <input id="designation" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="designation" required="required" type="text" value="{{ request()->input('designation', old('designation')) }}">
                        @endif
                      </div>
                    </div>
                </div>
              </div>
              
              @if($user->id != '')
              @else
              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="password" style="text-align: justify">Password <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="password" type="password" class="form-control passwordinput" name="password" required placeholder="Ex: Atiqah@12345">
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="password" style="text-align: justify">Confirm Password <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="password-confirm" type="password" class="form-control passwordinput" name="password_confirmation" required placeholder="Ex: Atiqah@12345">
                      </div>
                  </div>
                </div>
              </div>
              @endif

              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email" style="text-align: justify">Email <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $user->email }}">
                      @else
                      <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ request()->input('email', old('email')) }}">
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Mobile No <!-- <span class="required" style="color:red">*</span> -->
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="phone" name="phone" class="form-control col-md-7 col-xs-12" value="{{ $user->phone }}">
                      @else
                      <input type="text" id="phone" name="phone" class="form-control col-md-7 col-xs-12" value="{{ request()->input('email', old('phone')) }}" placeholder="EX: 0123245656">
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Total Leave <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="leave" name="leave" class="form-control col-md-7 col-xs-12" value="{{ $user->leave }}">
                      @else
                      <input type="text" id="leave" name="leave" class="form-control col-md-7 col-xs-12" value="{{ request()->input('leave', old('leave')) }}" placeholder="">
                      @endif
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Total MC <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="mc" name="mc" class="form-control col-md-7 col-xs-12" value="{{ $user->mc }}">
                      @else
                      <input type="text" id="mc" name="mc" class="form-control col-md-7 col-xs-12" value="14" placeholder="">
                      @endif
                    </div>
                  </div>
                </div>
              </div>

            @if($user->id != '')
              @if($ml_ResetLeave->mlve_status == 1 )
              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Leave Used <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="leaveused" class="form-control col-md-7 col-xs-12" value="{{ $user->leave_used }}">
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">MC Used <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="mcused" class="form-control col-md-7 col-xs-12" value="{{ $user->mc_used }}">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Leave Balance <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="leavebalance" class="form-control col-md-7 col-xs-12" value="{{ $user->leave_balance }}">
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">MC Balance <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="mcbalance" class="form-control col-md-7 col-xs-12" value="{{ $user->mc_balance }}">
                    </div>
                  </div>
                </div>
              </div>
              @endif
            @endif

              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Total RL
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="rl" name="rl" class="form-control col-md-7 col-xs-12" value="{{ $user->rl }}">
                      @else
                      <input type="text" id="rl" name="rl" class="form-control col-md-7 col-xs-12" value="{{ request()->input('rl', old('rl')) }}" placeholder="">
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Carry Forward {{$year-1}} (AL) <span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="cfleave" name="cfleave" class="form-control col-md-7 col-xs-12" value="{{ $user->cf_leave }}">
                      @else
                      <input type="text" id="cfleave" name="cfleave" class="form-control col-md-7 col-xs-12"placeholder="" value="{{ request()->input('cfleave', old('cfleave')) }}">
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align: justify">Role <span class="required" style="color:red">*</span></label>
                    @if($user->id != '')
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select class="form-control" name="role_id" id="all-categories">
                          <option value="">-- Choose Role --</option>
                          @foreach($roles as $role)
                              <option value="{{ $role->id }}" @if($role->id==$roleid) selected='selected' @endif>{{ $role->name }}</option>

                          @endforeach
                      </select>
                    </div>
                    @else
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select class="form-control" name="role_id" id="all-categories">
                          <option>-- Choose Role --</option>
                          @foreach($roles as $role)
                              <option value="{{ $role->id }}" {{ old('role_id') == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                          @endforeach
                      </select>
                    </div>
                    @endif
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="number" style="text-align: justify">Carry Forward (Month limit)<span class="required" style="color:red">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($user->id != '')
                      <input type="text" id="cfmonthlimit" name="cfmonthlimit" class="form-control col-md-7 col-xs-12" value="{{ $user->cf_monthlimit }}">
                      @else
                      <input type="text" id="cfmonthlimit" name="cfmonthlimit" class="form-control col-md-7 col-xs-12" placeholder="" value="{{ request()->input('cfmonthlimit', old('cfmonthlimit')) }}">
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-5">
                  @if($user->id != '')
                  <button id="send" type="submit" name="send" class="btn btn-primary">Update</button>
                  @else
                  <button id="send" type="submit" name="send" class="btn btn-primary">Register</button>
                  @endif
                  <button type="reset" name="reset" class="btn btn-danger">Reset</button>
                </div>
              </div>

              
              <div class="ln_solid"></div>
            </form>

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

@endsection