@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>User <small>List</small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <!-- <form class="form-horizontal form-label-left" method="post" action="{{ url('system-email/send') }}"> -->


          {!! Form::open(['action'=>'UserController@processaction','id'=>'v_user', 'method'=>'post', 'class'=>'form-horizontal']) !!}
          <!-- <p>For alternative validation library <code>parsleyJS</code> check out in the <a href="form.html">form page</a>
          </p> -->
          @if(Auth::user()->hasPermissionTo('user.create'))
          <span class="section">
          <a href="{{ route('user.createuser') }}" class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i> Register User</a>
          </span>
          @endif

          @if(Auth::user()->hasPermissionTo('leave.carryforward'))
            @if($ml->mlve_curryear < $curryear)
            <span class="section">
            <a href="{{ route('user.updatecf') }}" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Update Leave Carry Forward</a>
            </span>
            @endif
          @endif

          @if(Auth::user()->hasPermissionTo('reset.leave'))
            @if($ml_ResetLeave->mlve_status == 0 && $ml->mlve_curryear >= $curryear)
            <span class="section">
            <a href="{{ route('user.updaterlo') }}" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Open Permission (Reset Leave)</a>
            </span>
            @endif
            @if($ml_ResetLeave->mlve_status == 1 && $ml->mlve_curryear >= $curryear)
            <span class="section">
            <a href="{{ route('user.updaterlc') }}" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Close Permission (Reset Leave)</a>
            </span>
            @endif
          @endif

          {{ csrf_field() }}
            <!-- table-->
            <!-- <table id="datatable-responsive" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%"> -->
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Designation</th>
                          <th>E-mail</th>
                          <th>Role</th>
                          <th>User Status</th>
                          <th>User Approve</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($userList as $ul)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $ul->username }}</td>
                          <td>{{ $ul->designation }}</td>
                          <td>{{ $ul->email }}</td>
                          <td>{{ $ul->nameroles }}</td>
                          <td>
                              {!! ($ul->status=='A') ? '<span class="label label-success">'.__('Active').'</span>' : '<span class="label label-danger">'.__('Disabled').'</span>' !!}
                          </td>
                          <td>
                            <center>{!! ($ul->status_approve==1) ? '<span class="label label-success"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>' !!}
                            </center>
                          </td>
                          <td>
                            @if($ul->userid == Auth::user()->id)
                              <!--span class="label label-info">{{ __('Admin::base.permanentdata') }}</span-->
                            @elseif($ul->nameroles == 'superadmin')
                              @if($ul->status_approve == 0)
                                  @if(Auth::user()->hasPermissionTo('user.approve'))
                                  <a data-toggle="tooltip" title="{{ __('Approve') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('user.approved', $ul->userid) }}" onclick="return confirm('Approve {{ $ul->username }} ?')"><i class="fa fa-user"></i></a>
                                  <a data-toggle="tooltip" title="{{ __('Disapprove') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('user.disapproved', $ul->userid) }}" onclick="return confirm('Remove {{ $ul->username }} ?')"><i class="fa fa-user-times"></i></a>
                                  @endif
                              @endif
                            @else
                              
                              <?php 
                                if (!$ul->roleid) {
                                  $ul->roleid = 0;
                                }
                              ?>

                              <a data-toggle="tooltip" title="{{ __('User Detail') }}" class="btn btn-icon btn-info btn-sm" href="{{ route('user.showuser', Crypt::encrypt($ul->userid)) }}"><i class="fa fa-info-circle"></i></a>

                              @if(Auth::user()->hasPermissionTo('user.edit'))
                              <a data-toggle="tooltip" title="{{ __('Edit Detail') }}" class="btn btn-icon btn-warning btn-sm" href="{{ route('user.edituser', [Crypt::encrypt($ul->userid),$ul->roleid]) }}"><i class="fa fa-pencil-square-o"></i></a>
                              @endif
                              
                                @if($ul->status_approve == 1)
                                    @if($ul->status == 'A')
                                      <!--{{-- @if(Auth::user()->can('user.disable')) --}} -->
                                      @if(Auth::user()->hasPermissionTo('user.disable'))
                                      <a data-toggle="tooltip" title="{{ __('Disable') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('user.disable', $ul->userid) }}" onclick="return confirm('Are you sure?')"><i class="fa fa-minus-circle"></i></a>
                                      @endif
                                    @else
                                      <!-- {{-- @if(Auth::user()->can('user.enable')) --}} -->
                                      @if(Auth::user()->hasPermissionTo('user.enable'))
                                      <a data-toggle="tooltip" title="{{ __('Enable') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('user.enable', $ul->userid) }}"><i class="fa fa-check-circle"></i></a>
                                      @endif
                                    @endif
                                @else
                                  @if(Auth::user()->hasPermissionTo('user.approve'))
                                  <a data-toggle="tooltip" title="{{ __('Approve') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('user.approved', $ul->userid) }}" onclick="return confirm('Approve {{ $ul->username }} ?')"><i class="fa fa-user"></i></a>
                                  <a data-toggle="tooltip" title="{{ __('Disapprove') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('user.disapproved', $ul->userid) }}" onclick="return confirm('Remove {{ $ul->username }} ?')"><i class="fa fa-user-times"></i></a>
                                  @endif
                                @endif
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{-- $userList->paginate() --}}
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />



<script type="text/javascript">

  function activeUser(id)
  {
    var theForm = document.forms["v_user"];
    var enableUser = theForm.elements["enableUser"];
    if(enableUser.checked==true)
    {
        active = 'A';
    } 
    else
    {
      active = 'I';
    }
    $.get("{{ URL::action('UserController@getSetActivation') }}", { id: id, active: active },function(data){});

    $.get( "{{ URL::action('UserController@getSetActivation') }}/"+$(this).val(), function( data ) {

      $('.storepemohon').val('');
      console.log(data);
      if(data) {
        
        $('.storepemohon').val(data.stsl_description);
        $('.storepemohonid').val(data.stsl_storeladangid);

      } else {

        $('.storepemohon').val('Tiada Stor');
        
      }
    });
  }

  $(document).ready(function(){

    $(document).on("keyup",'#id_active', function(){
      alert(a);
    });

    $(document).on("switchChange.bootstrapSwitch", "#id_sactive", function (event, state) {
      alert('a');
        var $this = $(this);
        var id = $this.data('id');
        
        var active = '';
        if(state){
            active = 'A';
        }else{
            active = 'I';
        }
        
        $.get("{{ URL::action('UserController@getSetActivation') }}", { id: id, active: active },function(data){});
    });

  });
</script>
@endsection