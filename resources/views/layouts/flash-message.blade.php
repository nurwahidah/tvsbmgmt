@if(Session::has('success'))
    <div class="alert alert-success">
    	<button type="button" class="close" data-dismiss="alert">×</button>
       {{ Session::get('success') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
    	<button type="button" class="close" data-dismiss="alert">×</button>
       {{ Session::get('error') }}
    </div>
@endif

@if(Session::has('warning'))
    <div class="alert alert-warning">
    	<button type="button" class="close" data-dismiss="alert">×</button>
       {{ Session::get('warning') }}
    </div>
@endif

@if(Session::has('info'))
    <div class="alert alert-info">
    	<button type="button" class="close" data-dismiss="alert">×</button>
       {{ Session::get('info') }}
    </div>
@endif

@if($errors->all())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-exclamation-circle append-icon"></i><strong></strong> {{ $error }}</p>
            @endforeach
    </div>
@endif