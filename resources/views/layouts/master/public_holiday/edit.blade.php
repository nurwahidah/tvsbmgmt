@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('/master/public_holiday') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}

            {{ Form::open(['action'=>'MasterController@updatePublicHoliday', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'updatedataform','files'=>true]) }}
            <span class="section">Public Holiday ( {{ $phcity }} )</span>

        <div class="row">
          <table class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
            <input type="hidden" name="ph_city" value="{{ $phcity }}">
            <input type="hidden" name="ph_code" value="{{ $phcode }}">
            <thead>
              <tr>
                <td><input type="checkbox" name="select-all" id="select-all" /></td>
                <td>Date</td>
                <td>Description</td>
                <td>
                  <center>
                    <a id="add_leave" data-original-title="add new row" data-toggle="tooltip" data-placement="top" class="btn btn-sm btn-addrow btn-danger"> 
                      <i class="fa fa-plus"></i>
                    </a>
                  </center>
                </td>
              </tr>
            </thead>
            <tbody id="tablePublicHoliday"></tbody>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}

          {{ Form::open(['action'=>'MasterController@deleteAllPublicHoliday', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'updatedataform','files'=>true]) }}
            <tbody>
              @foreach($publicholiday as $ph)
                <tr>
                  <td><input type="checkbox" name="holiday[]" class="checkboxes" value="{{ $ph->ph_id }}" /></td>
                  <td>{!! date('Y-m-d', strtotime($ph->ph_date)) !!}</td>
                  <td>{{ $ph->ph_description }}</td>
                  <td>
                    <center>
                    <a data-toggle="tooltip" title="{{ __('Delete') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('master.pb.remove', $ph->ph_id) }}" onclick="return confirm('Delete Record?')"><i class="fa fa-trash-o"></i></a>
                    </center>
                  </td>
                </tr>
              @endforeach
              <tr>
                <td colspan="4">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                      <button style="margin-bottom: 10px" class="btn btn-primary" onclick="return confirm('Delete All Selected?')">Delete All Selected</button>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
            
            {!! Form::close() !!}

          </table>
        </div> <!-- //close row -->

        </div>
      </div>
    </div>
  </div>
@endsection
