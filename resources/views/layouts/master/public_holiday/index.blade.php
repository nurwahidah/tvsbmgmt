@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <!-- <span class="section">
          <a href="{{-- route('leave.createleave') --}}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add State</a>
        </span> -->
          {{ Form::open(['action'=>'LeaveController@index', 'method'=>'get']) }}

          {{ csrf_field() }}
          <legend>Public Holiday</legend>
            <!-- table-->
            <div class="table-responsive">
            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>State</th>
                          <th>Code</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <!-- <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          
                        </tr>
                      </tfoot> -->
                      <tbody>
                        @foreach($liststate as $ls)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $ls->st_name }}</td>
                          <td>{{ $ls->st_code }}</td>
                          <td>{!! ($ls->st_status=='A') ? '<span class="label label-success">'.__('Active').'</span>' : '<span class="label label-danger">'.__('Inactive').'</span>' !!}
                          </td>
                          <td>
                            @if(Auth::user()->hasPermissionTo('master.pb.edit'))
                                <a data-toggle="tooltip" title="{{ __('Edit') }}" class="btn btn-icon btn-warning btn-sm" href="{{ route('master.pb.editpb', $ls->st_code) }}"><i class="fa fa-pencil-square-o"></i></a>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->

@endsection


