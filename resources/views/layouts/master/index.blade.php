@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">

          {{ csrf_field() }}
            <!-- table-->
              <div class="row col-md-6 col-sm-6 col-xs-12 form-group">
{!! Form::open(['action'=>array('MasterController@update',$configid), 'method'=>'put', 'class'=>'form-horizontal']) !!}

              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <label>System Name</label>
                <input name="name" type="text" class="form-control" value="{{ $sysconfig->cs_name }}">
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <label>System Description</label>
                <input name="description" type="text" class="form-control" value="{{ $sysconfig->cs_description }}">
                <!-- /.input group -->
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <label>System State</label>
                <select class="form-control" name="statecode" id="id_state">
                    <option value="">-- Choose Category --</option>
                    @foreach($state as $ste)
                        <option value="{{ $ste->st_code }}" @if($ste->st_code == $sysconfig->cs_statecode) selected='selected' @endif>{{ $ste->st_name }}</option>
                    @endforeach
                </select>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-5">
                  <button type="submit" name="send" class="btn btn-primary">Update</button>
                  <button type="reset" name="reset" class="btn btn-danger">Reset</button>
                </div>
              </div>

            {!! Form::close() !!}
          </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

@endsection


