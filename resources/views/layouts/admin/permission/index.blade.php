@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2 title="Use for developer only">Permission <!-- <small>List</small> --></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

          @if(Auth::user()->hasPermissionTo('permission.create'))
          <span class="section">
          <a href="javascript:;" data-toggle="modal" data-target="#modal-add-permission" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Permission</a>
          </span>
          @endif
          
          {{ csrf_field() }}
            <!-- table-->
            <div class="table-responsive">
            <!-- <table id="" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%"> -->
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Permission Name</th>
                          <th>Description</th>
                          <th>Last Update</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                         
                        @foreach($perm as $perm)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $perm->name }}</td>
                          <td>{{ $perm->description }}</td>
                          <td>{!! date( 'd F Y, h:i:s', strtotime($perm->updated_at)) !!}</td>
                          <td>
                            @if(Auth::user()->hasPermissionTo('permission.edit'))
                            <a class="btn btn-icon btn-info editbtnPerm" data-toggle="tooltip" title="Edit Permission" data-id="{!! $perm->id !!}" data-desc="{!! $perm->name !!}"><i class="fa fa-edit"></i></a>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      {{-- $perm->paginate() --}}
                    </table>
                  </div>
                    
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

<!--modal-edit-perm -->
<div class="modal fade" id="modal-edit-perm">
  <div class="modal-dialog" id="modaldialogPerm">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Edit Permission</h4>
      </div>

      {{ Form::open(['action'=>'PermissionController@updatePerm', 'method'=>'post', 'class'=>'form-horizontal form-label-left', 'id'=>'updatepermform']) }}
      <div class="modal-body" id="displaydivPerm">
      </div>
      
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div> 
<!-- /modal-edit-perm -->

@endsection


