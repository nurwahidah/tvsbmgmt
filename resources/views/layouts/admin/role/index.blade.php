@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

{{-- $task->name --}}
<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Roles <!-- <small>List</small> --></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <!-- <form class="form-horizontal form-label-left" method="post" action="{{ url('system-email/send') }}"> -->


          {{-- Form::open(['action'=>'RoleController@processaction','id'=>'v_user', 'method'=>'post', 'class'=>'form-horizontal']) --}}
          <!-- <p>For alternative validation library <code>parsleyJS</code> check out in the <a href="form.html">form page</a>
          </p> -->
          @if(Auth::user()->hasPermissionTo('role.create'))
          <span class="section">
          <a href="javascript:;" data-toggle="modal" data-target="#modal-add-role" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Role</a>
          </span>
          @endif
          
          {{ csrf_field() }}
            <!-- table-->
            <!-- <table id="datatable-responsive" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%"> -->
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Role Name</th>
                          <th>Last Update</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tfoot>
                      <tbody>
                         
                        @foreach($roles as $rl)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $rl->name }}</td>
                          <td>{!! date( 'd F Y, h:i:s', strtotime($rl->updated_at)) !!}</td>
                          <td>
                            @if(Auth::user()->hasPermissionTo('role.edit'))
                            <a class="btn btn-icon btn-info editbtn" data-toggle="tooltip" title="Edit Role" data-id="{!! $rl->id !!}" data-desc="{!! $rl->name !!}"><i class="fa fa-edit"></i></a>
                            @endif

                            @if(Auth::user()->hasPermissionTo('role.permission'))
                            <a class="btn btn-icon btn-info listbtn" data-toggle="tooltip" title="Role Permission" data-id="{!! $rl->id !!}" data-desc="{!! $rl->name !!}"><i class="fa fa-list"></i></a>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

<!-- modal1 -->
<div class="modal fade" id="modal-1">
  <div class="modal-dialog" id="modaldialog1">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Edit Role</h4>
      </div>

      @if (session('flash_error'))
          <div class="alert alert-block alert-error fade in">
              <strong>Error !</strong>
              {!! session('flash_error') !!}
              <!--span class="close" data-dismiss="alert">×</span-->
          </div>
      @endif

      {{ Form::open(['action'=>'RoleController@updateRole', 'method'=>'post', 'class'=>'form-horizontal form-label-left', 'id'=>'updateroleform']) }}
      <div class="modal-body" id="displaydiv1">
      </div>
      
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div> 
<!-- /modal1 -->

<!-- modal2 -->
<div class="modal fade" id="modal-2">
  <div class="modal-dialog" id="modaldialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Permission</h4>
      </div>

      @if (session('flash_error'))
          <div class="alert alert-block alert-error fade in">
              <strong>Error !</strong>
              {!! session('flash_error') !!}
              <!--span class="close" data-dismiss="alert">×</span-->
          </div>
      @endif

      {{ Form::open(['action'=>'RoleController@storepermission', 'method'=>'post', 'class'=>'form-horizontal form-label-left', 'id'=>'rolepermissionform']) }}
      <div class="modal-body" id="displaydiv">
      </div>
      
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Save</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div> 
<!-- /modal2 -->

@endsection


