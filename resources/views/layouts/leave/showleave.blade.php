@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('leave') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}
          {!! Form::open(['action'=>'LeaveController@storeLeave', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}

            <span class="section">Leave Application Form</span>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Category <span class="required" style="color:red">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat" readonly>
                        <option value="">-- Choose Category --</option>
                        @foreach($leaveCat as $lc)
                            <option value="{{ $lc->lvct_id }}" @if($lc->lvct_id == $leavedetail->lve_category) selected='selected' @endif>{{ $lc->lvct_description }}</option>
                        @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Type <span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <select class="form-control" name="type" id="id_type" readonly>
                      <option value="">-- Choose Type --</option>
                      @foreach($leaveType as $lt)
                      <option value="{{ $lt->lvty_id }}" @if($lt->lvty_id == $leavedetail->lve_type) selected='selected' @endif>{{ $lt->lvty_name }} {{ $lt->lvty_description }}</option>
                      @endforeach
                </select>
              </div>
            </div>

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">From <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerstartLeave'>
                    <input type='text' class="form-control" name="datestart" value="{{ $leavedetail->lve_startdate }}" readonly/>
                    <span class="input-group-addon">
                       <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
            </div>
            <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" title="End" style="text-align: justify">To <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerendLeave'>
                    <input type='text' class="form-control" name="dateend" value="{{ $leavedetail->lve_enddate }}" readonly/>
                    <span class="input-group-addon">
                       <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
            </div>

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="End" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12 gap-items-2 gap-y" data-provide="photoswipe">
                @if($attachment->isEmpty())
                <label> _ </label>
                @endif
                @foreach($attachment as $attachment)
                    <a data-toggle="tooltip" title="{{$attachment->lvat_file}}" href="{{ route('leave.showleave.getfile', [$attachment->lvat_id,$attachment->lvat_file]) }}">{{ str_limit($attachment->lvat_file, $limit = 15, $end = '...') }}</a>
                @endforeach
              </div>
            </div>

          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:250px;" readonly>{{ $leavedetail->lve_description }}</textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Prepared By
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($prepared != null)
                <input type="text" class="form-control" name="preparedby" value="{{ $prepared->name }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="preparedby" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">
              @if($leavedetail->lve_status == 'D')
                Disapproved By
              @else
                Approved By
              @endif
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($approved != null)
                <input type="text" class="form-control" name="approvedby" value="{{ $approved->name }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="approvedby" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Prepared Date
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($prepared != null)
                <input type="text" class="form-control" name="preparedby" value="{{ $prepared->lve_prepareddate }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="preparedby" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">
              @if($leavedetail->lve_status == 'D')
                Disapproved Date
              @else
                Approved Date
              @endif
              </label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($approved != null)
                <input type="text" class="form-control" name="approveddate" value="{{ $approved->lve_approveddate }}" readonly="readonly">
                @else
                <input type="text" class="form-control" name="approveddate" value="-" readonly="readonly">
                @endif
              </div>
            </div>
          </div>
        </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection