@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <a href="{{ url('leave') }}" class="btn btn-default btn-sm"><i class="fa fa-mail-reply"></i> Back</a>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          {{ csrf_field() }}

          @if($leave->lve_id != '')
            {!! Form::open(['action'=>'LeaveController@updateLeave', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'updatedataform','files'=>true]) !!}
          @else
            {!! Form::open(['action'=>'LeaveController@storeLeave', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'newdataform','files'=>true]) !!}
          @endif

            <span class="section">Leave Application Form</span>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Category <span class="required" style="color:red">*</span>
                </label>
                @if($leave->lve_id != '')
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat">
                        <option value="">-- Choose Category --</option>
                        @foreach($leaveCat as $lc)
                            <option value="{{ $lc->lvct_id }}" @if($lc->lvct_id == $leavedetail->lve_category) selected='selected' @endif>{{ $lc->lvct_description }} ({{ $lc->lvct_name }})</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" value="{{$leavedetail->lve_id}}" name="leaveid">
                @else
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="category" id="id_cat">
                        <option value="">-- Choose Category --</option>
                        @foreach($leaveCat as $lc)
                            <option value="{{ $lc->lvct_id }}" {{ old('category') == $lc->lvct_id ? 'selected' : '' }}>{{ $lc->lvct_description }} ({{ $lc->lvct_name }})</option>
                        @endforeach
                    </select>
                </div>
                @endif
              </div>
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" style="text-align: justify">Type <span class="required" style="color:red">*</span>
                </label>

                @if($leave->lve_id != '')
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="type" id="id_type">
                        <option value="">-- Choose Type --</option>
                        @foreach($leaveType as $lt)
                        <option value="{{ $lt->lvty_id }}" @if($lt->lvty_id == $leavedetail->lve_type) selected='selected' @endif>{{ $lt->lvty_name }} {{ $lt->lvty_description }}</option>
                        @endforeach
                  </select>
                </div>
                @else
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <select class="form-control" name="type" id="id_type">
                        <option value="">-- Choose Type --</option>
                        @foreach($leaveType as $lt)
                        <option value="{{ $lt->lvty_id }}" {{ old('type') == $lt->lvty_id ? 'selected' : '' }}>{{ $lt->lvty_name }} {{ $lt->lvty_description }}</option>
                        @endforeach
                  </select>
                </div>
                @endif
              </div>


              <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" title="Start" style="text-align: justify">From <span class="required" style="color:red">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerstartLeave'>
                    @if($leave->lve_id != '')
                      <input type='text' class="form-control" name="datestart" value="{{ $leavedetail->lve_startdate }}" />
                    @else
                      <input type='text' class="form-control" name="datestart" value="{{ request()->input('datestart', old('datestart')) }}" />
                    @endif
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
            </div>

            


            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" title="End" style="text-align: justify">To <span class="required" style="color:red">*</span></label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerendLeave'>
                    @if($leave->lve_id != '')
                      <input type='text' class="form-control" name="dateend" value="{{ $leavedetail->lve_enddate }}"/>
                    @else
                      <input type='text' class="form-control" name="dateend" value="{{ request()->input('dateend', old('dateend')) }}"/>
                    @endif
                      <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align: justify">Attachment</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                @if($leave->lve_id != '')
                  <input type="file" class="form-control col-md-12 col-xs-12" name="fileattachment[]" multiple="multiple" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">
                  @foreach($attachment as $attachment)
                    <a data-toggle="tooltip" title="{{$attachment->lvat_file}}" href="{{ route('leave.showleave.getfile', [$attachment->lvat_id,$attachment->lvat_file]) }}">{{ str_limit($attachment->lvat_file, $limit = 15, $end = '...') }}</a>
                  @endforeach
                @else
                  <input type="file" class="form-control col-md-12 col-xs-12" name="fileattachment[]" multiple="multiple" value="{{ request()->input('fileattachment[]', old('fileattachment[]')) }}" style="height: 40px;">
                @endif
              </div>
            </div>

          </div>


          <div class="col-md-6 col-sm-6 col-xs-12 item form-group">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description" style="text-align: justify">Description<span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                @if($leave->lve_id != '')
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:250px;">{{ $leavedetail->lve_description }}</textarea>
                @else
                <textarea id="id_description" name="description" class="form-control col-md-7 col-xs-12" style="height:250px;">{{ Input::old('description') }}</textarea>
                @endif
              </div>
            </div>
          </div>

        </div> <!-- //close row -->



            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn btn-danger" type="reset">Reset</button>
              </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

@endsection