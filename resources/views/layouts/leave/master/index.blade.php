@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <!-- <span class="section">
          <a href="{{-- route('leave.createleave') --}}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Create Leave</a>
        </span> -->

          {{ csrf_field() }}
            <!-- table-->
            

            <div class="table-responsive">
            <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Description</th>
                          <th>CF</th>
                          <th>Month Limit</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if(!$mastleave->isEmpty())
                        @foreach($mastleave as $ml)
                        <tr>
                          <td>{{$ml->mlve_description}}</td>
                          <td>{{$ml->mlve_cf}}</td>
                          <td>{{$ml->mlve_month}}</td>
                          <td>
                            <a class="btn btn-icon btn-info editbtnmasterLeave" data-toggle="tooltip" title="Edit Carry Forward" data-id="{!! $ml->mlve_id !!}" data-desc="{!! $ml->mlve_cf !!}"><i class="fa fa-edit"></i></a>
                          </td>
                        </tr>
                        @endforeach
                      @else
                        <tr><td colspan="7">No record found</td></tr>
                      @endif
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

<!--modal-edit-perm -->
<div class="modal fade" id="modal-edit-ml">
  <div class="modal-dialog" id="modaldialogML">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Edit Carry Forward</h4>
      </div>

      {{ Form::open(['action'=>'LeaveController@updateML', 'method'=>'post', 'class'=>'form-horizontal form-label-left', 'id'=>'updateML']) }}
      <div class="modal-body" id="displaydivML">
      </div>
      
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
      </div>
      {{-- Form::close() --}}
    </div>
  </div>
</div> 
<!-- /modal-edit-perm -->

@endsection


