<!DOCTYPE html>

<html>
<head>
	<title>Leave Application Form</title>

	<link href="{{ asset('../assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ asset('../assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../assets/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset('../assets/vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('../assets/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('../assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('../assets/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('../assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset('../assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset('../assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('../assets/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('../assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset('../assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset('../assets/vendors/starrr/dist/starrr.css') }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('../assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('../assets/build/css/custom.min.css') }}" rel="stylesheet">


    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

     <style>

        .page-break{
            page-break-after: always;
        }
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 5cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
                font-family: Arial, Helvetica, sans-serif;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 1cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                text-align: center;
                /*background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;*/
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                text-align: center;
                /*background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;*/
            }

        .page-break {
            page-break-after: always;
        }
        .bg-grey {
            background: #F3F3F3;
        }
        .text-right {
            text-align: right;
        }

        .w-full {
            width: 100%;
        }

        .small-width {
            width: 15%;
        }
        .invoice {
            background: white;
            border: 1px solid #CCC;
            font-size: 14px;
            padding: 48px;
            margin: 20px 0;
        }

        </style>


</head>



<body>
    <header>
	   <img src="{{ asset('assets/img/logos/TVSB_logo.jpg') }}" alt="logo" class="main-logo" id="main_logo" style="width:600px; height:100px;">
    </header>
    <center><p style="font-family: Arial; font-size:20px;">{{ $title }}</p></center><br>

	<table border="0" cellpadding="10" cellspacing="5" style="width:100%">
		<tr>
			<td>Employee's Name</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="4">
			{{ $employeename }}
			</td>
		</tr>
		<tr>
			<td>Designation</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="4">{{ $designation }}</td>
		</tr>
		<tr>
			<td>Reason</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="4">{{ $desc }}</td>
		</tr>
		<tr>
			<td>Category</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="4">{{ $category }}</td>
		</tr>
		<tr>
			<td>Type of Leave</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="4">{{ $typeleave }} {{ $typedetail }}</td>
		</tr>
		<tr>
			<td>From</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;">{{ date('d/m/Y', strtotime($startdate)) }}
			</td>
			<td>To</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="2">{{ date('d/m/Y', strtotime($enddate)) }}</td>
			
		</tr>
		<tr>
			<td>No.of Leave(s)</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;">{{ $totalleave }}</td>
			<td>Prepared Date</td>H:i:s
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="2">{{ date('d/m/Y H:i:s', strtotime($prepareddate)) }}</td>
		</tr>
		<tr>
			<td>Approved By</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;">{{ $approvedby }}</td>
			<td>Approved Date</td>
			<td style="padding-bottom: 10px; border-bottom: 2px solid grey; line-height: 45px;" colspan="2">{{ date('d/m/Y H:i:s', strtotime($approveddate)) }}
			</td>
		</tr>

	</table>
    <div class="page-break"></div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <table class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Date Apply</th>
                          <th>Name</th>
                          <th>Designation</th>
                          <th>Category</th>
                          <th>Total Leave</th>
                          <th>From(Date)</th>
                          <th>To(Date)</th>
                          <!-- <th>Status</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      @if(!$leaveall->isEmpty())
                        @foreach($leaveall as $ll)

                        <tr>
                          <td>{{-- $count++ --}}</td>
                          <td>{{$ll->leavecreated}}</td>
                          <td>{{$ll->name}}</td>
                          <td>{{$ll->designation}}</td>
                          <td>{{$ll->lvct_description}}</td>
                          <td>{{$ll->lve_totalleave}}</td>
                          <td>{!! date( 'Y-m-d', strtotime($ll->lve_startdate)) !!}
                            {{-- $ll->lve_startdate --}}</td>
                          <td>
                            {!! date( 'Y-m-d', strtotime($ll->lve_enddate)) !!}
                            {{-- $ll->lve_enddate --}}</td>
                          <!-- <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{!! Globe::statusLabel($ll->lve_status) !!}</a></td> -->
                          
                        </tr>
                        @endforeach
                      @else
                        <tr><td colspan="10">No record found</td></tr>
                      @endif
                      </tbody>
                </table>
            </div>
        </div>
        <!-- ************************ -->
	<br>

     <footer>
        <h6><i>This is a computer-generated document. No signature is required.</i></h6>
    </footer>
<!-- 
	<p>Employee's Name : {{ $employeename }}</p>
	<p>Designation : {{ $designation }}</p>
	<p>Category : {{ $category }}</p>
	<p>Type of Leave : {{ $typeleave }}</p>
	<p>From : {{ $startdate }}</p>
	<p>To : {{ $enddate }}</p>
	<p>No of leave(s) : {{ $totalleave }}</p>
	<p>Date : {{ $prepareddate }}</p>
	<p>Approved By : {{ $approvedby }}</p>
	<p>Approved date : {{ $approveddate }}</p> -->

    <!-- Scripts -->
    <script src="{{ asset('../assets2/js/core.min.js') }}"></script>
    <script src="{{ asset('../assets2/js/app.min.js') }}"></script>
    <script src="{{ asset('../assets2/js/script.min.js') }}"></script>

    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('../js/jQuery/jquery-2.2.3.min.js') }}"></script>
    
    <!-- jQuery -->
    <script src="{{ asset('../assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('../assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('../assets/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('../assets/vendors/nprogress/nprogress.js') }}"></script>
    <!-- validator -->
    <!-- <script src="{{ asset('assets/vendors/validator/validator.js') }}"></script> -->
    <!-- Dropzone.js -->
    <script src="{{ asset('../assets/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{ asset('../assets/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- gauge.js -->
    <script src="{{ asset('../assets/vendors/gauge.js/dist/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('../assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('../assets/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ asset('../assets/vendors/skycons/skycons.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('../assets/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('../assets/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('../assets/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('../assets/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('../assets/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('../assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('../assets/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('../assets/vendors/DateJS/build/date.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('../assets/vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
    <script src="{{ asset('../assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('../assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('../assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('../assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('../assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('../assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ asset('../assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('../assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('../assets/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('../assets/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('../assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('../assets/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('../assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('../assets/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('../assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('../assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('../assets/vendors/starrr/dist/starrr.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('../assets/build/js/custom.min.js') }}"></script>


<script>
    $('#myDatepickerstart').datetimepicker({
      format: 'YYYY-MM-DD hh:mm:ss'
    });
    $('#myDatepickerend').datetimepicker({
      format: 'YYYY-MM-DD hh:mm:ss'
    });

    $('#myDatepickerstartLeave').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerendLeave').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerOT').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#myDatepickerStartTime').datetimepicker({
        format: 'hh:mm A'
    });
    $('#myDatepickerEndTime').datetimepicker({
        format: 'hh:mm A'
    });
</script>
	

</body>
</html>