@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            </div>
        </div>
    </div>
</div>

<!-- Form1 -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      
      <div class="x_content">
        <span class="section">
          <a href="{{ route('leave.createleave') }}" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Create Leave</a>
          <!-- <a href="{{ route('leave.htmltopdfview',['download'=>'pdf']) }}">Download PDF</a> -->
        </span>
          {{ Form::open(['action'=>'LeaveController@index', 'method'=>'get']) }}
          <!-- <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
              <select class="form-control" name="ticketstatus" id="ticketstatus" onchange="this.form.submit()">
                      <option value="">-- Filter Status --</option>
                      <option value="all">All</option>
                      <option value="1" >Open</option>
                      <option value="0">Close</option>
              </select>
            </div>
          </div> -->
        <div class="row">
          <div class="col-md-1 col-sm-1 col-xs-12 pull-right">
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
              <button type="submit" class="btn btn-primary">Filter</button>
              <a href="{{ url('/leave')}}" class="btn btn-success">Cancel</a>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
            <div class='input-group date col-md-12 col-sm-12 col-xs-12' id='myDatepickerstartLeave'>
                <input type='text' class="form-control" name="leavedate" value="" placeholder="Leave Date" value="{{ request()->input('leavedate', old('leavedate')) }}" />
                <span class="input-group-addon">
                   <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
          </div>
        </div>
        {{ Form::close() }}



          {{ csrf_field() }}
            <!-- table-->
            <div class="table-responsive">
            <table id="example-1" class="table table-striped table-bordered" cellspacing="0" data-provide="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Date Apply</th>
                          <th>Name</th>
                          <th>Designation</th>
                          <th>Category</th>
                          <th>Total Leave</th>
                          <th>From(Date)</th>
                          <th>To(Date)</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot style="display: none;">
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                      @if(!$leavelist->isEmpty())
                        @foreach($leavelist as $ll)

                        @if(Auth::user()->id == $ll->lve_preparedby && $ll->lve_readstatus == '0' && $ll->lve_status !== 'N')
                        <tr style="background-color:#ece3fc">
                        @else
                        <tr>
                        @endif
                          <td>{{ $count++ }}</td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{{$ll->leavecreated}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{{$ll->name}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{{$ll->designation}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{{$ll->lvct_description}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{{$ll->lve_totalleave}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">
                            {!! date( 'Y-m-d', strtotime($ll->lve_startdate)) !!}
                            {{-- $ll->lve_startdate --}}</td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">
                            {!! date( 'Y-m-d', strtotime($ll->lve_enddate)) !!}
                            {{-- $ll->lve_enddate --}}</a></td>
                          <td><a href="{{ route('leave.showleave', $ll->lve_id) }}">{!! Globe::statusLabel($ll->lve_status) !!}</a></td>
                          <td>
                            <!-- pdf file -->
                            @if($ll->lve_status == 'A')
                             <a data-toggle="tooltip" title="{{ __('Prints') }}" href="{{ route('leave.htmltopdfview',$ll->lve_id) }}" class="btn btn-icon btn-primary btn-sm"><i class="fa fa-file-pdf-o"></i></a> 
                             
                            {{-- @if(Auth::user()->id == 1)
                              <a data-toggle="tooltip" title="{{ __(' Print Leave') }}" href="{{ route('leave.printleave',$ll->lve_id) }}" class="btn btn-icon btn-info btn-sm"><i class="fa fa-file-pdf-o"></i></a> 
                             @endif
                             --}}

                           @endif
                            
                            <a data-toggle="tooltip" title="{{ __('Leave Detail') }}" class="btn btn-icon btn-info btn-sm" href="{{ route('leave.showleave', $ll->lve_id) }}"><i class="fa fa-info-circle"></i></a>
                            @if($ll->lve_status == 'N')
                              @if(Auth::user()->hasPermissionTo('leave.edit') && Auth::user()->id == $ll->id)
                                <a data-toggle="tooltip" title="{{ __('Edit Leave') }}" class="btn btn-icon btn-warning btn-sm" href="{{ route('leave.editleave', $ll->lve_id) }}"><i class="fa fa-pencil-square-o"></i></a>
                              @endif
                              @if(Auth::user()->hasPermissionTo('leave.approve') && Auth::user()->id != $ll->id || Auth::user()->hasPermissionTo('leave.approve') && Auth()->user()->hasRole('superadmin'))
                                <a data-toggle="tooltip" title="{{ __('Approve') }}" class="btn btn-icon btn-success btn-sm" href="{{ route('leave.approved', $ll->lve_id) }}" onclick="return confirm('Approve {{ $ll->name }} request ?')"><i class="fa fa-check"></i></a>
                              @endif
                              @if(Auth::user()->hasPermissionTo('leave.disapproved') && Auth::user()->id != $ll->id || Auth::user()->hasPermissionTo('leave.disapproved') && Auth()->user()->hasRole('superadmin'))
                                <a data-toggle="tooltip" title="{{ __('Disapproved') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('leave.disapproved', $ll->lve_id) }}" onclick="return confirm('Disapproved {{ $ll->name }} request ?')"><i class="fa fa-minus-circle"></i>
                                </a>
                              @endif
                            @endif

                            @if($ll->lve_status == 'D')
                              @if(Auth::user()->hasPermissionTo('leave.delete') && Auth::user()->id == $ll->id || Auth::user()->hasPermissionTo('leave.delete') && Auth()->user()->hasRole('superadmin')) <!-- for role has permission -->
                                  <a data-toggle="tooltip" title="{{ __('Delete Leave') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('leave.remove', $ll->lve_id) }}" onclick="return confirm('Delete Leave?')"><i class="fa fa-trash-o"></i></a>
                              @endif
                            @endif

                            @if($ll->lve_status == 'N')
                              @if(Auth::user()->hasPermissionTo('leave.delete') && Auth::user()->id == $ll->id) <!-- for role has permission -->
                                  <a data-toggle="tooltip" title="{{ __('Delete Leave') }}" class="btn btn-icon btn-danger btn-sm" href="{{ route('leave.remove', $ll->lve_id) }}" onclick="return confirm('Delete Leave?')"><i class="fa fa-trash-o"></i></a>
                              @endif
                            @endif

                          </td>
                        </tr>
                        @endforeach
                      @else
                        <tr><td colspan="10">No record found</td></tr>
                      @endif
                      </tbody>
                </table>
              </div>
            <!-- table -->
        {!! Form::close() !!}

      </div>
      <!-- /div class = x_content -->
    </div>
     <!-- footer content -->
      <!-- /footer content -->

  </div>
</div>
 <!--  /Form 1 -->
  <br />

<!--modal-edit-perm -->
<div class="modal fade" id="modal-edit-perm">
  <div class="modal-dialog" id="modaldialogPerm">
    <div class="modal-content">

      <div class="modal-header" style="background-color:#2f4661;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;">Edit Permission</h4>
      </div>

      {{-- Form::open(['action'=>'PermissionController@updatePerm', 'method'=>'post', 'class'=>'form-horizontal form-label-left', 'id'=>'updatepermform']) --}}
      <div class="modal-body" id="displaydivPerm">
      </div>
      
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Update</button>
      </div>
      {{-- Form::close() --}}
    </div>
  </div>
</div> 
<!-- /modal-edit-perm -->

@endsection


